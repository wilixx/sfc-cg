import csv
import collections
import SFC

def loadNetwork(filename):
	with open(filename, 'r') as nwFile:
		dataReader = csv.reader(nwFile, delimiter='\t')
		row = dataReader.next()
		g = DiGraph(int(row[0]))
		for u, v, w in dataReader:
			g.add_edge(int(u), int(v), float(w))
			g.add_edge(int(v), int(u), float(w))
	return g

def loadDemandsAndFunctionReq(filename):
	with open(filename, 'r') as dnfFile:
		dataReader = csv.reader(dnfFile, delimiter='\t')
		row = dataReader.next()
		funcReq = [float(v) for v in row]
		demands = []
		for demand in dataReader:
			demands.append((int(demand[0]), int(demand[1]), float(demand[2]), [int(f) for f in demand[3:]]))
	return demands, funcReq

def getLowerBound(g, demands):
	distances = g.distance_all_pairs()
	LB = 0
	for s, t, d, chain in demands:
		LB += distances[s][t] * d
	return LB
 
def getMultplicativeStrech(instanceName, model, percent):
	g = loadNetwork("./instances/{instanceName}_topo.txt".format(instanceName=instanceName))
	distances = g.distance_all_pairs()
	instanceFilename = "./results/{instanceName}_{model}_{percent:.6f}.res".format(instanceName=instanceName, model=model, percent=percent)
	_, _, _, _, _, _, paths, _, _, _ = getCGMetrics(instanceFilename)
	return [ (len(path) - 1) / float(distances[path[0]][path[-1]]) for path in paths ]

def getAdditiveStrech(instanceName, model, percent):
	g = loadNetwork("./instances/{instanceName}_topo.txt".format(instanceName=instanceName))
	distances = g.distance_all_pairs()
	instanceFilename = "./results/{instanceName}_{model}_{percent:.6f}.res".format(instanceName=instanceName, model=model, percent=percent)
	_, _, _, _, _, _, paths, _, _, _ = getCGMetrics(instanceFilename)
	return [ len(path) - 1 - float(distances[path[0]][path[-1]]) for path in paths ]


def getCGMetrics(filename):
	#print "Getting metrics from ", filename	
	try:
		with open(filename, 'r') as csvfile:
			dataReader = csv.reader(csvfile, delimiter='\t')
			row = dataReader.next()
			intObj = float(row[0])
			fracObj = float(row[1])

			row = dataReader.next()
			nbColumn = int(row[0])

			row = dataReader.next()
			CPUTime = float(row[0])
			WallTime = float(row[1])

			nbNFV = 0
			paths = []
			chains = collections.defaultdict(lambda: collections.defaultdict(int))
			nodeUsage = {}
			linkUsage = {} 
			try:
				n = int(dataReader.next()[0])
				for _ in range(n):
					row = dataReader.next()
					if int(row[2]) != -1:
						nbNFV += 1
					nodeUsage[int(row[0])] = ((int(row[1]), int(row[2])))
	 
				m = int(dataReader.next()[0])
				for _ in range(m):
					row = dataReader.next()
					linkUsage[(int(row[0]), int(row[1]))] = (float(row[2]), float(row[3]))

				nbPaths = int(dataReader.next()[0])
				
				for _ in range(nbPaths):
					row = dataReader.next()

					pathLength = int(row[0])
					paths.append([int(i) for i in row[1:1+pathLength]])
					nbLocal = int(row[pathLength])
					chainLocal = ()
					chain = ()
					for i in range(pathLength+1, len(row)-1, 2):
						chainLocal += (int(row[i]), )
						chain += (int(row[i+1]), )
					chains[chain][chainLocal] += 1
			except Exception as e:
				print e
				pass
			return intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage
	except Exception as e:
		print e
		return -1, -1, -1, -1, -1, -1, [], {}, {}, {}

def getMinimumBandwidth(instanceName, method="heur_pf", percent=float(1.0)):
	g = loadNetwork("./instances/occlim/" + instanceName + "_topo.txt")
	if SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=g.order()):
		minBandwidth, _, _, _, _, _, _, _, _, _ = getCGMetrics("./results/occlim/{instanceName}_{method}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, method=method, percent=percent, nbReplic=g.order()))	
		return minBandwidth
	else:
		return -1
def getNbDemands(instanceName, method="heur_pf", percent=float(1.0)):
	dem, func = loadDemandsAndFunctionReq("./instances/occlim/" + instanceName + "_demand.txt")
	return len(dem)
	
def getCriticalNbLicense(instanceName, method="heur_pf", ratio=1.01, percent=float(1.0)):
	prefix = "./instances/occlim/"
	nwFile = prefix + instanceName + "_topo.txt"
	demandFile = prefix + instanceName + "_demand.txt"
	g = loadNetwork(nwFile)
	dem, func = loadDemandsAndFunctionReq(demandFile)
	LB = getLowerBound(g, dem)
	# Get min bandwidth
	if SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=g.order()):
		minBandwidth, _, _, _, _, _, _, _, _, _ = getCGMetrics("./results/occlim/{instanceName}_{method}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, method=method, percent=percent, nbReplic=g.order()))
	else:
		return -1
	# Find first available nbReplicas
	firstAvailable = -1
	for nbReplic in xrange(1, g.order()+1, 1):
		if SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
			intObj, fracObj, _, _, _, _, _, _, _, _ = getCGMetrics("./results/occlim/{instanceName}_{method}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic))
			# If first available has minimum value, it might not be the critical number of licenses
			if nbReplic != 1 and intObj <= minBandwidth * ratio:
				print "First visited is not solved"
				return -1
			firstAvailable = nbReplic
			break
	for nbReplic in xrange(firstAvailable+1, g.order()+1, 1):
		if SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
			intObj, fracObj, _, _, _, _, _, _, _, _ = getCGMetrics("./results/occlim/{instanceName}_{method}_{percent:.6f}_{nbReplic}.res".format(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic))
			# print intObj, minBandwidth, minBandwidth * ratio
			if intObj <= minBandwidth * ratio:
				return nbReplic
		else:
			print nbReplic, " is not solved"
			return -1
	return -1
		

def getILPMetrics(filename):
	print "Getting metrics for ", filename
	try:
		with open(filename, 'r') as csvfile:
			dataReader = csv.reader(csvfile, delimiter='\t')
			row = dataReader.next()
			intObj = float(row[0])
			fracObj = float(row[1])

			row = dataReader.next()
			nbColumn = int(row[0])

			row = dataReader.next()
			CPUTime = float(row[0])
			WallTime = float(row[1])

			nbNFV = 0
			paths = []
			chains = collections.defaultdict(lambda: collections.defaultdict(int))
			nodeUsage = {}
			linkUsage = {}
			try:
				n = int(dataReader.next()[0])
				for _ in range(n):
					row = dataReader.next()
					if int(row[2]) != -1:
						nbNFV += 1
					nodeUsage[int(row[0])] = ((int(row[1]), int(row[2])))
	 
				m = int(dataReader.next()[0])
				for _ in range(m):
					row = dataReader.next()
					linkUsage[(int(row[0]), int(row[1]))] = (float(row[2]), float(row[3]))

				nbPaths = int(dataReader.next()[0])
				
				for _ in range(nbPaths):
					row = dataReader.next()

					pathLength = int(row[0])
					paths.append([int(i) for i in row[1:1+pathLength]])
					nbLocal = int(row[pathLength])
					chainLocal = ()
					chain = ()
					for i in range(pathLength+1, len(row)-1, 2):
						chainLocal += (int(row[i]), )
						chain += (int(row[i+1]), )
					chains[chain][chainLocal] += 1
			except Exception as e:
				print e
				pass
			return intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage
	except Exception as e:
		return -1, -1, -1, -1, -1, -1, [], {}, {}, {}

def getEnergyCGMetrics(filename):
	# print "Getting metrics for ", filename
	with open(filename, 'r') as csvfile:
		dataReader = csv.reader(csvfile, delimiter='\t')
		row = dataReader.next()
		intObj = float(row[0])
		fracObj = float(row[1])

		row = dataReader.next()
		nbColumn = int(row[0])

		row = dataReader.next()
		CPUTime = float(row[0])
		WallTime = float(row[1])

		nbNFV = 0
		paths = []
		chains = collections.defaultdict(lambda: collections.defaultdict(int))
		activeLinks = {}
		try:
		
			nodeUsage = {}
			n = int(dataReader.next()[0])
			for _ in range(n):
				row = dataReader.next()
				if int(row[2]) != -1:
					nbNFV += 1
				nodeUsage[int(row[0])] = ((int(row[1]), int(row[2])))

			linkUsage = {}
			m = int(dataReader.next()[0])
			for _ in range(m):
				edge = (int(row[0]), int(row[1]))
				row = dataReader.next()
				linkUsage[edge] = (float(row[2]), float(row[3]))
				activeLinks[edge] = float(row[4]) == 1.0000

			nbPaths = int(dataReader.next()[0])
			
			
			for _ in range(nbPaths):
				row = dataReader.next()

				pathLength = int(row[0])
				paths.append([int(i) for i in row[1:1+pathLength]])
				nbLocal = int(row[pathLength])
				chainLocal = ()
				chain = ()
				for i in range(pathLength+1, len(row)-1, 2):
					chainLocal += (int(row[i]), )
					chain += (int(row[i+1]), )
				chains[chain][chainLocal] += 1
		except Exception as e:
			print e
			pass

	return intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks
