PROBABILITY = 0.1
PROBABILITY_RANGE = [0.1, .2, .5, .75, 1]
NB_NODES = 15
NB_NODES_RANGE = [10, 12, 14, 16, 18, 20]
HETERO_RATIO = 0
HETERO_RATIO_RANGE = [0.25, 0.5, 1, 2, 4, 8]
CHAIN_SIZE_RANGE = xrange(5, 11, 1)
NB_CHAINS_RANGE = xrange(1, 11, 1)
NB_FUNCTIONS_RANGE = list(set(range(1, 7, 1) + range(1, 31, 5)))
LOAD_RANGE = [1e6, 1e7, 1e8, 1e9]
SEEDS = range(30)

def getUniformName(nbFunctions=6, nbChains=4, chainSize=5):
	return "uniform_{nbFunctions}_{nbChains}_{chainSize}".format(
		nbFunctions=nbFunctions, nbChains=nbChains, chainSize=chainSize)

def getPopulationName(ratio):
	if ratio == 0:
		return "uniform"
	return "population_{ratio:.6f}".format(ratio=ratio)