params = {'backend': 'pdf',
          # 'text.latex.preamble': ['\usepackage{gensymb}'],
          'axes.labelsize': 8, # fontsize for x and y labels (was 10)
          'axes.titlesize': 8,
          'font.size': 30, # was 10
          'legend.fontsize': 8, # was 10
          'xtick.labelsize': 8,
          'ytick.labelsize': 8,
          'text.usetex': True,
          'figure.figsize': [fig_width, fig_height],
          'font.family': 'serif', 
}

matplotlib.rcParams.update(params)

periods = ["D1", "D2", "D3", "D4", "D5"]
"""
You just have to 
"""
delays = ["""list of all path delays for period p""" for p in periods]

plt.clf()
plt.boxplot(delays, positions=xrange(0, 5), widths=.8, sym='+', whis=[10, 90])
plt.xlabel("Time periods")
plt.xticks(xrange(0, 5), periods)
plt.ylabel("Delay (ms)")
plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                      wspace=0.2, hspace=0.2)