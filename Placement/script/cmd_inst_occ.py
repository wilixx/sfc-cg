"""
Print all commands for the replication experiments for a given network or all networks
Arguments:
	1st: Network name. If equal to "all", experiments will be run on all networks 
"""
import sys


orders = {
	"internet2": 10,
	"atlanta": 15,
	"germany50": 50,
	"internet2_emb_10": 10,
	"atlanta_emb_15": 15,
	"germany50_emb_50": 50
}

names = [sys.argv[1]] if sys.argv[1] != "all" else orders.keys()
models = [sys.argv[2]] if sys.argv[2] != "all" else ["heur", "heur_pf", "BB"]
percent = sys.argv[3]

for name in names:
  for model in models:
  	for i in reversed(xrange(1, orders[name]+1)):
  		print "{} -r {} -m {} -d {}".format(name, i, model, percent)