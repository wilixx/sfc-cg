import glob
import os
import SFC
import SFC_generators

def getOccCall(instanceName, nbReplic, method):
	return "./LP_OCC {instanceName} -m {method} -r {nbReplic}".format(instanceName=instanceName, nbReplic=nbReplic, method=method)

def genAllRandom(percent=1.0, methods=["heur_pf"]):
	genAll_nbNodes(percent=percent, methods=methods)
	genAll_edgeProba(percent=percent, methods=methods)
	genAll_nbFunctionssizeChains(percent=percent, methods=methods)
	genAll_chainSize(percent=percent, methods=methods)
	genAll_nbChains(percent=percent, methods=methods)
	genAll_ratio(percent=percent, methods=methods)
	genAll_load(percent=percent, methods=methods)

def genAll_nbNodes(percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for nbNodes in SFC_generators.NB_NODES_RANGE:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(nbNodes=nbNodes, seed=seed)
					if not SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)

def genAll_load(nbNodes=10, percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for load in SFC_generators.LOAD_RANGE:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(load=load, seed=seed)
					if not SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)


def genAll_edgeProba(nbNodes=10, percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for edgeProba in [0.1, .2, .5, .75, 1]:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(edgeProba=edgeProba, seed=seed)
					if not SFC.isSolved(instanceName, method, percent, nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)

def genAll_nbFunctionssizeChains(nbNodes=10, percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for nbFunctions in SFC_generators.NB_FUNCTIONS_RANGE:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(nbFunctions=nbFunctions), seed=seed)
					if not SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)

def genAll_chainSize(nbNodes=10, percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for chainSize in SFC_generators.CHAIN_SIZE_RANGE:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(chainSize=chainSize), seed=seed)
					if not SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)

def genAll_nbChains(nbNodes=10, percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for nbChains in SFC_generators.NB_CHAINS_RANGE:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(chainType=SFC_generators.getUniformName(nbChains=nbChains), seed=seed)
					if not SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)

def genAll_ratio(nbNodes=10, percent=1.0, methods=["heur_pf"]):
	for method in methods:
		for seed in SFC_generators.SEEDS:
			for ratio in SFC_generators.HETERO_RATIO_RANGE:
				for nbReplic in xrange(nbNodes, 0, -1):
					instanceName = SFC.printInstanceGeneration(pairType=SFC_generators.getPopulationName(ratio=ratio), seed=seed)
					if not SFC.isSolved(instanceName=instanceName, method=method, percent=percent, nbReplic=nbReplic):
						print getOccCall(instanceName=instanceName, nbReplic=nbReplic, method=method)