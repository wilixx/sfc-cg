import csv
import sys

orders = {
	"atlanta": 15,
	"internet2": 10,
	"germany50": 5,
	"ta2": 65
}

name = sys.argv[1]
factor = float(sys.argv[2])

edges = []
firstLine = None
filename = "./instances/{}_topo.txt".format(name)
print filename
with open(filename, "r") as f:
	datareader = csv.reader(f, delimiter='\t')
	firstLine = datareader.next()
	for row in datareader:
		edges.append((int(row[0]), int(row[1]), float(row[2])))

print edges
with open(filename, "w") as f:
	dataWriter = csv.writer(f, delimiter='\t')
	dataWriter.writerow(firstLine)
	for u, v, w in edges:
		dataWriter.writerow([u, v, w*factor])