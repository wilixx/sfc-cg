import csv
import sys
import matplotlib
from math import ceil
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import sys
sys.path.append("../../../MyPython")
from math import sqrt

load("metrics.sage")

orders = {
	"atlanta": 15, 
	"internet2": 10, 
	"germany50": 50
}

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=20):
	assert(columns in [1,2])
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	    fig_width = fig_widths[columns-1] # width in inches

	if fig_height is None:
	    fig_height = fig_width*((sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
	    print("WARNING: fig_height too large:" + fig_height + 
	          "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
	    fig_height = MAX_HEIGHT_INCHES

	params = {'backend': 'pdf',
	          # 'text.latex.preamble': ['\usepackage{gensymb}'],
	          'axes.labelsize': 10, # fontsize for x and y labels (was 10)
	          'axes.titlesize': 10,
	          'font.size': fontsize, # was 10
	          'legend.fontsize': 10, # was 10
	          'xtick.labelsize': 10,
	          'ytick.labelsize': 10,
	          # 'text.usetex': True,
	          'figure.figsize': [fig_width, fig_height],
	          'font.family': 'serif', 
	}
	matplotlib.rcParams.update(params)

def plotBandwidth(name, model, percent=1, funcCoef=4):
	latexify(columns=1)
	listIntCG = []
	listXCG = []

	listIntILP = []
	listXILP = []

	for p in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
		nbDemands = int(ceil(p / 100.0 * (4 * (orders[name] - 1) * orders[name])))
		instName = "{}_emb_{}_{}".format(name, orders[name], nbDemands)
		try:
			intObjCG, fracObjCG, nbColumnCG, CPUTimeCG, WallTimeCG, nbNFV, pathsCG, chainsCG, _, _ = metrics.getCGMetrics("./results/{}_{}_{:.6f}_{}.res".format(instName, model, percent, funcCoef))
			if intObjCG != -1:
				print name, " -> ", intObjCG, nbNFV
				listIntCG.append(intObjCG)
				listXCG.append(p)
		except Exception as e:
			print e
			pass
		try:
			intObjILP, fracObjILP, nbColumnsILP, CPUTimeILP, WallTimeILP, nbNFV = metrics.getILPMetrics("./results/ilp/{}_{:.6f}_{}.res".format(instName, percent, funcCoef), orders[name])
			if intObjILP != -1:
				print name, " -> ", intObjILP, nbNFV
				listIntILP.append(intObjILP)
				listXILP.append(p)
		except Exception as e:
			print e
			pass
	# plt.scatter(nbNFV, listFrac)
	plt.clf()

	plt.scatter(listXCG, listIntCG, label="NFV_CG", color="blue", marker="+")
	plt.scatter(listXILP, listIntILP, label="NFV_ILP", color="red", marker="o")
	# plt.legend(loc="lower right")
	plt.xlabel("% of demands")
	plt.xlim(0, 100)
	plt.ylabel("Bandwidth")
	plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
	plt.ylim(0, plt.ylim()[1])
	plt.subplots_adjust(left=0.15, bottom=0.2, right=0.96, top=0.93, wspace=0.2, hspace=0.2)
	filename = "./figs/emb/{}_{}_bandwidthComp.pdf".format(name, model)
	print "Saved to", filename
	plt.savefig(filename, bbox_inches='tight')

def plotWallTime(name, percent=1, funcCoef=4):
	latexify(columns=1)
	listXCG_CPLEX = []
	listIntCG_CPLEX = []

	listXCG_BF = []
	listIntCG_BF = []

	listXILP = []
	listIntILP = []


	for p in [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]:
		nbDemands = int(ceil(p / 100.0 * (4 * (orders[name] - 1) * orders[name])))
		instName = "{}_emb_{}_{}".format(name, orders[name], nbDemands)
		
		intObjCG, fracObjCG, nbColumnCG, CPUTimeCG, WallTimeCG, nbNFV, pathsCG, chainsCG, _, _ = metrics.getCGMetrics("./results/{}_{}_{:.6f}_{}.res".format(instName, "pathThread", percent, funcCoef))
		print name, " -> ", intObjCG, WallTimeCG, nbNFV
		if intObjCG != -1:
			listIntCG_CPLEX.append(WallTimeCG / 1000.0)
			listXCG_CPLEX.append(p)
		
		intObjCG, fracObjCG, nbColumnCG, CPUTimeCG, WallTimeCG, nbNFV, pathsCG, chainsCG, _, _ = metrics.getCGMetrics("./results/{}_{}_{:.6f}_{}.res".format(instName, "pathThreadBF", percent, funcCoef))
		print name, " -> ", intObjCG, WallTimeCG, nbNFV
		if intObjCG != -1:
			listIntCG_BF.append(WallTimeCG / 1000.0)
			listXCG_BF.append(p)

		intObjILP, fracObjILP, nbColumnILP, CPUTimeILP, WallTimeILP, nbNFVILP, pathsILP, chainsILP, nodeUsageILP, linkUsageILP = metrics.getILPMetrics("./results/ilp/{}_{:.6f}_{}.res".format(instName, percent, funcCoef))
		print name, " -> ", intObjILP, WallTimeILP, nbNFV
		if intObjILP != -1:
			listIntILP.append(WallTimeILP / 1000.0)
			listXILP.append(p)
	# plt.scatter(nbNFV, listFrac)
	plt.clf()
	plt.scatter(listXCG_CPLEX, listIntCG_CPLEX, label="NFV_CG (CPLEX)", color="green", marker="x")
	print "NFV_CG (CPLEX)", listXCG_CPLEX, listIntCG_CPLEX
	plt.scatter(listXCG_BF, listIntCG_BF, label="NFV_CG (BF)", color="blue", marker="+")
	print "NFV_CG (BF)", listXCG_BF, listIntCG_BF
	plt.scatter(listXILP, listIntILP, label="NFV_ILP", color="red", marker="o")
	print "NFV_ILP", listXILP, listIntILP
	
	plt.legend(bbox_to_anchor=(0, -.35), loc="upper left", borderaxespad=0.)
	plt.xlabel("% of demands")
	plt.xlim(0, 100)
	plt.ylabel("Time (s)")	
	plt.ylim(min(listIntCG_CPLEX + listIntCG_BF + listIntILP), plt.ylim()[1])
	plt.yscale('log')
	plt.subplots_adjust(left=0.18, bottom=0.2, right=0.96, top=0.93, wspace=0.2, hspace=0.2)
	filename = "./figs/emb/{}_ILP_WallTimeComp.pdf".format(name)
	print "Saved to", filename
	plt.savefig(filename, bbox_inches='tight')

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print "Not enough arguments..."
	else:
		name = sys.argv[1]
		model = sys.argv[2]

		plotBandwidth(name, model)
		plotWallTime(name, model)