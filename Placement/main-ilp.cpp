
#include <MyTimer.hpp>
#include <tclap/CmdLine.h>

#include "SFCIlp.hpp"

std::string getDemandFile(const std::string& name, int _i);
std::vector<Demand> loadDemands(const std::string& _filename, double _percent);
std::vector<int> loadNodeCapa(const std::string& _filename,
    const double _percent = 1.0);

DiGraph loadNetwork(const std::string& _filename);
std::vector<double> loadFunctions(const std::string& _filename);
void saveNodeCapas(const std::vector<int>& _nodeCapas,
    const std::string& _filename);
void saveTopo(const DiGraph& _graph, const std::string& _filename);

struct Param {
    std::string name;
    double factor;
    int funcCoef;
    double percentDemand;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with ILP", ' ', "1.0");

    TCLAP::UnlabeledValueArg<std::string> networkName(
        "network", "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<double> demandFactor(
        "d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> functionChages(
        "c", "functionChages", "Specify the index for the function charge file",
        false, 4, "int");
    cmd.add(&functionChages);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand",
        "Specify the percentage of demand used",
        false, 100, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {networkName.getValue(), demandFactor.getValue(),
        functionChages.getValue(), percentDemand.getValue()};
}

int main(int argc, char** argv) {
    std::cout << std::fixed;
    try {
        const Param params = getParams(argc, argv);

        const DiGraph network =
            loadNetwork("./instances/" + params.name + "_topo.txt");
        const std::vector<Demand> allDemands = loadDemands(
            "./instances/" + params.name + "_demand.txt", params.factor);
        const std::vector<double> funcCharge = loadFunctions(
            "./instances/func" + std::to_string(params.funcCoef) + ".txt");
        std::vector<int> nodeCapas =
            loadNodeCapa("./instances/" + params.name + "_nodeCapa.txt");
        std::cout << "Files loaded...\n";

        const int nbDemands =
            ceil(allDemands.size() * params.percentDemand / 100.0);
        std::vector<Demand> demands(allDemands.begin(),
            allDemands.begin() + nbDemands);

        int totalNbCores = 0;
        for (int i = 0; i < int(demands.size()); ++i) {
            const auto& funcs = std::get<3>(demands[i]);
            for (int j = 0; j < int(funcs.size()); ++j) {
                totalNbCores += ceil(std::get<2>(demands[i]) / funcCharge[funcs[j]]);
            }
        }
        std::cout << totalNbCores << " needed cores!" << '\n';

        std::vector<int> NFVNodes;
        for (Graph::Node u(0); u < network.getOrder(); ++u) {
            if (nodeCapas[u] != -1) {
                NFVNodes.push_back(u);
            }
        }
        int availableCores = 0;
        for (const auto& u : NFVNodes) {
            availableCores += nodeCapas[u];
        }
        std::cout << availableCores << " cores available !\n";

        Time timer;
        timer.start();
        std::cout << "Getting initial configuration...\n";

        SFCIlp ilp(network, nodeCapas, demands, funcCharge, NFVNodes);
        if (ilp.solve()) {
            ilp.save("./results/ilp/" + params.name + "_" + std::to_string(nbDemands) + "_" + std::to_string(params.factor) + "_" + std::to_string(params.funcCoef) + ".res",
                timer.show());
        } else {
            return -1;
        }

    } catch (const IloException& e) {
        std::cout << e.getMessage() << '\n';
    } catch (const TCLAP::ArgException& e) {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << '\n';
    }
    return 1;
}

std::vector<int> loadNodeCapa(const std::string& _filename,
    const double _percent) {
    std::vector<int> nodeCapas;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    } else {
        std::cout << "Opening :" << _filename << '\n';
    }

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int u, capa;
            std::stringstream lineStream(line);
            lineStream >> u >> capa;
            nodeCapas.emplace_back(capa * _percent);
        }
    }
    return nodeCapas;
}

std::vector<Demand> loadDemands(const std::string& _filename,
    const double _percent) {
    std::vector<Demand> demands;
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    } else {
        std::cout << "Opening :" << _filename << '\n';
    }

    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int s, t;
            double d;
            std::stringstream lineStream(line);
            lineStream >> s >> t >> d;
            FunctionDescriptor f;
            std::vector<FunctionDescriptor> functions;
            while (lineStream.good()) {
                lineStream >> f;
                functions.push_back(f);
            }
            functions.pop_back(); // Dirty hack
            demands.emplace_back(s, t, d * _percent, functions);
        }
    }
    return demands;
}

std::string getDemandFile(const std::string& _name, const int _n) {
    std::ifstream ifs("./instances/" + _name + "_demands.txt");
    if (!ifs) {
        std::cerr << ("./instances/" + _name + "_demands.txt")
                  << " does not exists!\n";
        exit(-1);
    }
    std::string filename;
    int i = 0;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (i >= _n) {
            filename = line;
            break;
        }
        ++i;
    }
    return "./instances/" + filename;
}

DiGraph loadNetwork(const std::string& _filename) {
    const auto network = DiGraph::loadFromFile(_filename);
    return std::get<0>(network);
}

std::vector<double> loadFunctions(const std::string& _filename) {
    std::ifstream ifs(_filename, std::ifstream::in);
    if (!ifs) {
        std::cerr << _filename << " does not exists!\n";
        exit(-1);
    }
    std::vector<double> functions;
    while (ifs.good()) {
        std::string line;
        std::getline(ifs, line);
        if (line != "") {
            int f;
            double cpu;
            std::stringstream lineStream(line);
            lineStream >> f >> cpu;
            functions.push_back(cpu);
        }
    }
    return functions;
}