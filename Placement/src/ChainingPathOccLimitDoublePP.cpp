#include "ChainingPathOccLimitDoublePP.hpp"

namespace SFC::OccLim {
ChainingPathOccFuncDoublePP::ReducedMainProblem::ReducedMainProblem(const Instance& _inst)
    : m_inst(&_inst)
    , m_dummyPaths([&]() {
        IloNumVarArray y(m_env, m_inst->demands.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            y[i] = IloAdd(m_model, IloNumVar(m_env, 0.0, IloInfinity));
            IloAdd(m_integerConversions, IloRange(m_env, 0.0, IloNumExprArg(y[i]), 0.0,
                                             std::string("dummyPath_" + std::to_string(i)).c_str()));
        }
        return y;
    }())
    , m_dummyLocations([&]() {
        IloNumVarArray z(m_env, m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            z[f] = IloAdd(m_model, IloNumVar(m_env, 0.0, IloInfinity));
            IloAdd(m_integerConversions, IloRange(m_env, 0.0, IloNumExprArg(z[f]), 0.0,
                                             std::string("dummyLocation" + toString(f)).c_str()));
        }
        return z;
    }())
    , m_obj([&]() {
        IloExpr expr(m_env);
        // Add dummy variables to objective
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            expr += (m_inst->demands[i].d * 2) * m_dummyPaths[i];
        }
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            expr += (100000) * m_dummyLocations[f];
        }
        auto obj = IloAdd(m_model, IloMinimize(m_env, expr));
        expr.end();
        return obj;
    }())
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_inst->demands.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            onePathCons[i] = IloRange(m_env, 1.0, IloNumExprArg(m_dummyPaths[i]), 1.0);
            setIloName(onePathCons[i], "onePathCons" + toString(m_inst->demands[i]));
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_inst->network.size(), 0.0, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            linkCapaCons[e].setBounds(0.0, m_inst->network.getEdgeWeight(edge));
            setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons([&]() {
        IloRangeArray nodeCapasCons(m_env, m_inst->NFVNodes.size());
        for (const auto& u : m_inst->NFVNodes) {
            const int i = m_inst->NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, 0.0, m_inst->nodeCapa[u]));
            setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_oneLocationCons([&]() {
        IloRangeArray oneLocationCons(m_env, m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            oneLocationCons[f] = IloRange(m_env, 1.0, IloNumExprArg(m_dummyLocations[f]), 1.0);
            setIloName(oneLocationCons[f], "oneLocationCons" + toString(f));
        }
        return IloAdd(m_model, oneLocationCons);
    }())
    , m_pathFuncCons([&]() {
        IloRangeArray pathFuncCons(m_env,
            m_inst->demands.size() * m_inst->funcCharge.size() * m_inst->NFVNodes.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                for (const auto& u : m_inst->NFVNodes) {
                    pathFuncCons[getIndexPathFunc(i, f, u)] = IloRange(m_env, 0.0, IloInfinity);
                }
            }
        }
        return IloAdd(m_model, pathFuncCons);
    }())
    , m_linkCharge(m_env, m_inst->network.size())
    , m_nodeCharge(m_env, m_inst->NFVNodes.size())
    , m_funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size())
    , m_pathFuncCons_Utility(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size()) {}

double ChainingPathOccFuncDoublePP::ReducedMainProblem::getObjValue() const {
    return m_solver.getObjValue();
}

int ChainingPathOccFuncDoublePP::ReducedMainProblem::getIndexB(const Graph::Node _u,
    const function_descriptor _f) const {
    assert(_f < m_inst->funcCharge.size());
    assert(_u < m_inst->network.getOrder());
    return m_inst->NFVNodes.size() * _f + m_inst->NFVIndices[_u];
}

int ChainingPathOccFuncDoublePP::ReducedMainProblem::getIndexPathFunc(const int _i,
    const function_descriptor _f, const Graph::Node _u) const {
    assert(_i < m_inst->demands.size());
    assert(_f < m_inst->funcCharge.size());
    assert(_u < m_inst->network.getOrder());

    return m_inst->NFVNodes.size() * m_inst->funcCharge.size() * _i
           + m_inst->NFVNodes.size() * _f
           + m_inst->NFVIndices[_u];
}

ChainingPathOccFuncDoublePP::ReducedMainProblem::~ReducedMainProblem() {
    m_env.end();
}

void ChainingPathOccFuncDoublePP::ReducedMainProblem::addPath(const ServicePath& _sPath) {
    assert([&]() {
        const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
        if (ite != m_paths.end()) {
            std::cout << _sPath << " is already present!" << '\n';
            return false;
        }
        return true;
    }());

    const auto & [ nPath, locations, demandID ] = _sPath;

    for (int e = 0; e < m_inst->network.size(); ++e) {
        m_linkCharge[e] = 0;
    }
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end(); ++iteU, ++iteV) {
        m_linkCharge[m_inst->edgeToId(*iteU, *iteV)] += m_inst->demands[demandID].d;
    }

    for (IloInt i = 0; i < m_nodeCharge.getSize(); ++i) {
        m_nodeCharge[i] = IloNum(0.0);
    }

    for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        for (const auto u : m_inst->NFVNodes) {
            const int index = m_inst->NFVIndices[u];
            m_funcPath[m_inst->NFVNodes.size() * f + index] = IloNum(0.0);
            m_pathFuncCons_Utility[m_inst->NFVNodes.size() * f + index] = m_pathFuncCons[getIndexPathFunc(demandID, f, u)];
        }
    }

    for (int j = 0; j < locations.size(); ++j) {
        const int index = m_inst->NFVIndices[locations[j]];
        m_nodeCharge[locations[j]] += m_inst->nbCores(demandID, j);
        m_funcPath[m_inst->NFVNodes.size() * m_inst->demands[demandID].functions[j] + index] = -1.0;
    }

    auto yVar = IloNumVar(m_onePathCons[demandID](1.0)
                          + m_obj(m_inst->demands[demandID].d * (nPath.size() - 1))
                          + m_linkCapasCons(m_linkCharge)
                          + m_nodeCapasCons(m_nodeCharge)
                          + m_pathFuncCons_Utility(m_funcPath));
    setIloName(yVar, "y" + toString(_sPath));
    m_y.add(yVar);
    IloAdd(m_integerConversions, IloConversion(m_env, yVar, ILOBOOL));
    m_paths.emplace_back(_sPath);
}

void ChainingPathOccFuncDoublePP::ReducedMainProblem::addLocation(const FunctionLocation& _loc) {
    IloNumColumn inc(m_oneLocationCons[_loc.f](1.0));
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (const auto& u : m_inst->NFVNodes) {
            if (_loc.locations[u]) {
                inc += m_pathFuncCons[getIndexPathFunc(i, _loc.f, u)](1.0);
            }
        }
    }
    auto zVar = IloNumVar(inc);
    setIloName(zVar, "z");
    m_z.add(zVar);
    IloAdd(m_integerConversions, IloConversion(m_env, zVar, ILOBOOL));
    m_localisations.push_back(_loc);
}

bool ChainingPathOccFuncDoublePP::ReducedMainProblem::checkReducedCosts() const {
    IloNumArray funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());

    for (int k = 0; k < m_paths.size(); ++k) {
        const auto & [ nPath, locations, demandID ] = m_paths[k];
        double myRC = -m_onePathDuals[demandID];
        for (auto iteU = nPath.begin(), iteV = std::next(iteU);
             iteV != nPath.end();
             ++iteU, ++iteV) {
            myRC += m_inst->demands[demandID].d
                    * (1 - m_linkCapasDuals[m_inst->edgeToId(*iteU, *iteV)]);
        }

        for (int j = 0; j < locations.size(); ++j) {
            myRC += -m_inst->nbCores(demandID, j) * m_nodeCapasDuals[locations[j]];
        }

        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (const auto& u : m_inst->NFVNodes) {
                const int index = m_inst->NFVIndices[u];
                funcPath[m_inst->NFVNodes.size() * f + index] = IloNum(0.0);
            }
        }

        for (int j = 0; j < locations.size(); ++j) {
            const int index = m_inst->NFVIndices[locations[j]];
            funcPath[m_inst->NFVNodes.size() * m_inst->demands[demandID].functions[j] + index] = 1.0;
        }

        for (const auto& u : m_inst->NFVNodes) {
            const int index = m_inst->NFVIndices[u];
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                myRC += funcPath[m_inst->NFVNodes.size() * f + index]
                        * m_pathFuncDuals[getIndexPathFunc(demandID, f, u)];
            }
        }

        const double solverRC = m_solver.getReducedCost(m_y[k]);
        if (std::abs(myRC - solverRC) > RC_EPS) {
            std::cout << "myRC: " << myRC << ", solverRC: " << solverRC << '\n';
            funcPath.end();
            return false;
        }
    }

    funcPath.end();
    return true;
}

double ChainingPathOccFuncDoublePP::ReducedMainProblem::solveInteger() {
    IloModel integerModel(m_env);
    integerModel.add(m_model);
    integerModel.add(m_integerConversions);
    m_solver.extract(integerModel);
    m_solver.setOut(std::cout);
    double obj = -1;
    if (epsilon_equal<double>()(m_solver.solve(), IloTrue)) {
        obj = m_solver.getObjValue();
        std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size() << '\n';
        getNetworkUsage();
    } else {
        std::cout << "No solution found!\n";
        m_solver.exportModel("IMP.lp");
    }
    return obj;
}

bool ChainingPathOccFuncDoublePP::ReducedMainProblem::solve() {
    if (epsilon_equal<double>()(m_solver.solve(), IloTrue)) {
        updateDuals();
        return true;
    }
    return false;
}

void ChainingPathOccFuncDoublePP::ReducedMainProblem::updateDuals() const {
    m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons);
    m_solver.getDuals(m_pathFuncDuals, m_pathFuncCons);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
    m_solver.getDuals(m_oneLocationDuals, m_oneLocationCons);
}

void ChainingPathOccFuncDoublePP::ReducedMainProblem::getNetworkUsage() const {
}

std::vector<ServicePath> ChainingPathOccFuncDoublePP::ReducedMainProblem::getServicePaths() const {
    std::vector<ServicePath> sPaths(m_inst->demands.size());
    for (int i = 0; i < m_paths.size(); ++i) {
        auto y = m_y[i];
        if (m_solver.getValue(y) > RC_EPS) {
            sPaths[m_paths[i].demand] = m_paths[i];
        }
    }
    return sPaths;
}

// ################################################################################################
// ################################################################################################
// ################################################################################################

ChainingPathOccFuncDoublePP::PathPricingProblem::PathPricingProblem(const Instance& _inst)
    : m_inst(&_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env, m_inst->NFVNodes.size() * (nbLayers - 1));
        for (const auto& u : m_inst->NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                a[getIndexA(u, j)] = IloAdd(m_model,
                    IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_b([&]() {
        IloNumVarArray b(m_env, m_inst->NFVNodes.size() * m_inst->funcCharge.size());
        for (const auto& u : m_inst->NFVNodes) {
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(b[getIndexB(u, f)], "b" + toString(std::make_tuple(f, u)));
            }
        }
        return b;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (int e = 0; e < m; ++e) {
            for (int j = 0; j < nbLayers; ++j) {
                setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_inst->edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
            for (int j = 0; j < nbLayers; ++j) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& v : m_inst->network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_inst->edgeToId(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_inst->edgeToId(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_inst->isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(m_model,
                    IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
                vars.end();
                vals.end();
            }
        }
        return flowConsCons;
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e = 0; e < m_inst->edges.size(); ++e) {
            linkCapaCons[e].setUB(m_inst->network.getEdgeWeight(m_inst->edges[e]));
            setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto& u : m_inst->NFVNodes) {
            nodeCapaCons[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_inst->nodeCapa[u]));
            setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_funcNodeUsageCons([&]() {
        IloRangeArray funcNodeUsageCons(m_env, m_inst->NFVNodes.size() * m_inst->funcCharge.size());
        for (const auto& u : m_inst->NFVNodes) {
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                funcNodeUsageCons[getIndexB(u, f)] = IloAdd(m_model,
                    IloRange(m_env, 0.0, IloNum(m_inst->maxChainSize) * m_b[getIndexB(u, f)], IloInfinity));
                setIloName(funcNodeUsageCons[getIndexB(u, f)], "funcNodeUsageCons" + toString(std::make_tuple(f, u)));
            }
        }
        return funcNodeUsageCons;
    }())
    , m_nbLicensesCons([&]() {
        IloRangeArray nbLicensesCons(m_env, m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            for (const auto& u : m_inst->NFVNodes) {
                vars.add(m_b[getIndexB(u, f)]);
            }
            nbLicensesCons[f] = IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars), m_inst->nbLicenses));
            vars.end();
        }
        return nbLicensesCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {
    std::cout << "Rows: " << m_solver.getNrows() << ", cols:" << m_solver.getNcols() << '\n';
}

ChainingPathOccFuncDoublePP::PathPricingProblem::~PathPricingProblem() {
    m_env.end();
}

int ChainingPathOccFuncDoublePP::PathPricingProblem::getIndexF(const int _e,
    const int _j) const {
    assert(_e < m_inst->network.size());
    assert(_j < nbLayers);
    return m * _j + _e;
}

int ChainingPathOccFuncDoublePP::PathPricingProblem::getIndexA(const Graph::Node _u,
    const int _j) const {
    assert(_u < m_inst->network.getOrder());
    assert(_j < nbLayers);
    return m_inst->NFVNodes.size() * _j + m_inst->NFVIndices[_u];
}

int ChainingPathOccFuncDoublePP::PathPricingProblem::getIndexB(const Graph::Node _u,
    const function_descriptor _f) const {
    assert(_u < m_inst->network.getOrder());
    assert(_f < m_inst->funcCharge.size());
    return m_inst->NFVNodes.size() * _f + m_inst->NFVIndices[_u];
}

void ChainingPathOccFuncDoublePP::PathPricingProblem::updateNbLicenses() {
    for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        m_nbLicensesCons[f].setUB(m_inst->nbLicenses);
    }
}

void ChainingPathOccFuncDoublePP::PathPricingProblem::setDemandID(const int _demandID) {
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(0.0, 0.0);
    m_flowConsCons[n * m_inst->demands[m_demandID].functions.size()
                   + m_inst->demands[m_demandID].t]
        .setBounds(0.0, 0.0);

    m_demandID = _demandID;
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(1.0, 1.0);
    m_flowConsCons[n * m_inst->demands[m_demandID].functions.size()
                   + m_inst->demands[m_demandID].t]
        .setBounds(-1.0, -1.0);
    updateModelsCoefficients();
}

void ChainingPathOccFuncDoublePP::PathPricingProblem::updateModelsCoefficients() {
    for (int e = 0; e < m_inst->edges.size(); ++e) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j = 0; j <= m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->demands[m_demandID].d);
        }
        for (int j(m_inst->demands[m_demandID].functions.size() + 1); j < nbLayers; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(0);
        }
        m_linkCapaCons[e].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
    }

    for (const auto& u : m_inst->NFVNodes) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j = 0; j < m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(m_inst->nbCores(m_demandID, j));
        }
        for (int j = m_inst->demands[m_demandID].functions.size(); j < nbLayers - 1; ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(0);
        }
        m_nodeCapaCons[u].setLinearCoefs(vars, vals);
        setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
        vars.end();
        vals.end();
    }

    //Function node usage
    for (const auto& u : m_inst->NFVNodes) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int j = 0; j < m_inst->demands[m_demandID].functions.size(); ++j) {
                vars.add(m_a[getIndexA(u, j)]);
                if (m_inst->demands[m_demandID].functions[j] == f) {
                    vals.add(-1.0);
                } else {
                    vals.add(0.0);
                }
            }
            for (int j = m_inst->demands[m_demandID].functions.size(); j < nbLayers - 1; ++j) {
                vars.add(m_a[getIndexA(u, j)]);
                vals.add(0.0);
            }
            m_funcNodeUsageCons[getIndexB(u, f)].setLinearCoefs(vars, vals);
            vars.end();
            vals.end();
        }
    }
}

void ChainingPathOccFuncDoublePP::PathPricingProblem::updateDual(const ReducedMainProblem& _rmp) {
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);

    for (int e = 0; e < m; ++e) {
        for (int j = 0; j <= m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->demands[m_demandID].d * (1 - _rmp.m_linkCapasDuals[e]));
        }
    }

    for (const auto& u : m_inst->NFVNodes) {
        const int index = m_inst->NFVIndices[u];
        for (int j = 0; j < m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(-_rmp.m_nodeCapasDuals[index] * m_inst->nbCores(m_demandID, j));
        }
    }
    for (const auto& u : m_inst->NFVNodes) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            vars.add(m_b[getIndexB(u, f)]);
            vals.add(_rmp.m_pathFuncDuals[_rmp.getIndexPathFunc(m_demandID, f, u)]);
        }
    }

    m_obj.setConstant(-_rmp.m_onePathDuals[m_demandID]);
    m_obj.setLinearCoefs(vars, vals);
    m_solver.extract(m_model);
    vars.end();
    vals.end();
}

double ChainingPathOccFuncDoublePP::PathPricingProblem::getObjValue() const {
    return m_solver.getObjValue();
}

bool ChainingPathOccFuncDoublePP::PathPricingProblem::solve() {
    return m_solver.solve();
}

ServicePath ChainingPathOccFuncDoublePP::PathPricingProblem::getServicePath() const {
    const auto & [ s, t, d, functions ] = m_inst->demands[m_demandID];
    ServicePath sPath{
        {s},
        std::vector<function_descriptor>(functions.size()),
        m_demandID};

    IloNumArray funcPlacementVal(m_env);
    m_solver.getValues(funcPlacementVal, m_a);
    IloNumArray flowVal(m_env);
    m_solver.getValues(flowVal, m_f);

    assert(epsilon_equal<double>()(IloSum(funcPlacementVal), m_inst->demands[m_demandID].functions.size()));

    int j = 0;
    [[maybe_unused]] int nbLoop = 0;
    while (sPath.nPath.back() != t || j < functions.size()) {
        // std::cout << sPath << '\n';
        if (j < functions.size()
            && m_inst->isNFV[sPath.nPath.back()]
            && epsilon_equal<double>()(funcPlacementVal[getIndexA(sPath.nPath.back(), j)], IloTrue)) {
            sPath.locations[j] = sPath.nPath.back();
            ++j;
        } else {
            auto eID = 0u;
            for (const auto& edge : m_inst->network.getEdges()) {
                if (epsilon_equal<double>()(flowVal[getIndexF(eID, j)], IloTrue)) {
                    sPath.nPath.push_back(edge.second);
                    flowVal[getIndexF(eID, j)] = IloFalse;
                    break;
                }
                ++eID;
            }
        }
        assert([&]() {
            if (nbLoop++ < n + m_inst->demands[m_demandID].functions.size() + 1) {
                return true;
            }
            std::cout << "Demands: " << m_inst->demands[m_demandID]
                      << ", sPath: " << sPath << '\n';
            return false;
        }());
    }
    return sPath;
}

// ##################################################################################################
// ##################################################################################################
// ##################################################################################################

ChainingPathOccFuncDoublePP::LocationPricingProblem::LocationPricingProblem(const Instance& _inst)
    : m_inst(&_inst)
    // , m_funcLocation([&](){
    // 	FunctionLocation funcLoc = {
    // 		0,
    // 		std::vector<char>(_inst.funcCharge.size())
    // 	}
    // 	return funcLoc;
    // }())
    , m_funcLocation({0, std::vector<char>(_inst.funcCharge.size())}) {}

FunctionLocation ChainingPathOccFuncDoublePP::LocationPricingProblem::getLocations() const {
    return m_funcLocation;
}

bool ChainingPathOccFuncDoublePP::LocationPricingProblem::solve(const ReducedMainProblem& _rmp) {
    std::vector<double> duals(m_inst->NFVNodes.size(), 0.0);
    for (const auto& u : m_inst->NFVNodes) {
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            duals[m_inst->NFVIndices[u]] -= _rmp.m_pathFuncDuals[_rmp.getIndexPathFunc(i, m_f, u)];
        }
    }
    // Sort nodes by ascending sum of dual values
    std::vector<Graph::Node> nodes = m_inst->NFVNodes;
    std::sort(nodes.begin(), nodes.end(), [&](const Graph::Node __u1, const Graph::Node __u2) {
        return duals[__u1] < duals[__u2];
    });

    // Get L_f first function
    std::fill(m_funcLocation.locations.begin(), m_funcLocation.locations.end(), false);
    for (int i = 0; i < m_inst->nbLicenses; ++i) {
        m_funcLocation.locations[nodes[i]] = true;
        m_objValue -= duals[nodes[i]];
    }

    m_funcLocation.f = m_f;
    return true;
}

void ChainingPathOccFuncDoublePP::LocationPricingProblem::setFunction(const function_descriptor _f) {
    m_f = _f;
}

double ChainingPathOccFuncDoublePP::LocationPricingProblem::getObjValue() const {
    return m_objValue;
}

// ##########################################################################################################
// ##########################################################################################################
// ##########################################################################################################

ChainingPathOccFuncDoublePP::ChainingPathOccFuncDoublePP(const Instance& _inst)
    : m_inst(&_inst)
    , m_mainProblem(_inst)
    , m_pool(std::max(1u, std::thread::hardware_concurrency()))
    , m_threadMap([&]() {
        std::map<std::thread::id, int> pps;
        int i = 0;
        for (const auto& id : m_pool.getIds()) {
            pps.emplace(id, i);
            ++i;
        }
        return pps;
    }())
    , m_ppps([&]() {
        std::vector<PathPricingProblem> pps;
        int concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
        pps.reserve(concurentThreadsSupported);
        for (int i = 0; i < concurentThreadsSupported; ++i) {
            pps.emplace_back(_inst);
        }
        return pps;
    }())
    , m_lpps([&]() {
        std::vector<LocationPricingProblem> pps;

        int concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
        pps.reserve(concurentThreadsSupported);
        for (int i = 0; i < concurentThreadsSupported; ++i) {
            pps.emplace_back(_inst);
        }
        return pps;
    }()) {
}

double ChainingPathOccFuncDoublePP::getObjValue() const {
    return m_mainProblem.getObjValue();
}

// std::vector<ChainingPathOccFuncDoublePP::BBNode> ChainingPathOccFuncDoublePP::getNodes() const {
// 	return m_mainProblem.getNodes();
// }

// bool ChainingPathOccFuncDoublePP::isInteger() const {
// 	return m_mainProblem.isInteger();
// }

// void ChainingPathOccFuncDoublePP::applyNode(const BBNode& _node) {
// 	// resetBBounds();
// 	for(const auto& bound : _node.m_bounds) {
// 		setBBounds(std::get<0>(bound), std::get<1>(bound), std::get<2>(bound));
// 	}
// }

void ChainingPathOccFuncDoublePP::addInitConf(const std::vector<ServicePath>& _paths) {
    for (const auto& path : _paths) {
        m_mainProblem.addPath(path);
    }
}

std::vector<ServicePath> ChainingPathOccFuncDoublePP::getServicePaths() const {
    return m_mainProblem.getServicePaths();
}

void ChainingPathOccFuncDoublePP::save(const std::string& _filename,
    const std::pair<double, double>& _time) {
    std::ofstream ofs(_filename);
    ofs << m_intObj << '\t' << m_fractObj << '\n';
    ofs << m_mainProblem.m_paths.size() << std::endl;
    ofs << _time.first << '\t' << _time.second << std::endl;

    std::vector<double> linkUsage(m_inst->network.size(), 0);
    std::vector<int> nodeUsage(m_inst->network.getOrder(), 0);
    std::vector<int> pathsUsed(m_inst->demands.size());

    IloNumArray yVal(m_mainProblem.m_env);
    m_mainProblem.m_solver.getValues(yVal, m_mainProblem.m_y);
    for (int i = 0; i < m_mainProblem.m_paths.size(); ++i) {
        if (yVal[i] > RC_EPS) {
            const auto & [ nPath, locations, demandID ] = m_mainProblem.m_paths[i];
            pathsUsed[demandID] = i;

            for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end(); ++iteU, ++iteV) {
                linkUsage[m_inst->edgeToId(*iteU, *iteV)] += m_inst->demands[demandID].d * yVal[i];
            }
            for (int j = 0; j < locations.size(); ++j) {
                nodeUsage[locations[j]] += yVal[i] * m_inst->nbCores(demandID, j);
            }
        }
    }

    ofs << m_inst->network.getOrder() << '\n';
    for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
        ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst->nodeCapa[u] << '\n';
    }

    ofs << m_inst->network.size() << '\n';
    int i = 0;
    for (const auto& edge : m_inst->network.getEdges()) {
        ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i]
            << '\t' << m_inst->network.getEdgeWeight(edge) << '\n';
        ++i;
    }

    ofs << pathsUsed.size() << '\n';
    for (const auto& index : pathsUsed) {
        const auto & [ nPath, locations, demandID ] = m_mainProblem.m_paths[index];

        ofs << nPath.size() << '\t';
        for (const auto& u : nPath) {
            ofs << u << '\t';
        }
        for (int j = 0; j < locations.size(); ++j) {
            ofs << locations[j] << '\t' << m_inst->demands[demandID].functions[j] << '\t';
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}

bool ChainingPathOccFuncDoublePP::solveInteger() {
    if (solve()) {
        m_intObj = m_mainProblem.solveInteger();
        return m_intObj > 0.0;
    }
    return false;
}

void ChainingPathOccFuncDoublePP::printSolution() const {
    std::cout << "Final obj value: " << m_intObj << " / " << m_fractObj << '\n';
}

bool ChainingPathOccFuncDoublePP::solve() {
    bool reducedCost;
    std::vector<std::future<std::optional<FunctionLocation>>> locFutures;
    locFutures.reserve(m_inst->funcCharge.size());
    std::vector<std::future<std::optional<ServicePath>>> pathFutures;
    pathFutures.reserve(m_inst->demands.size());

    // auto pricingProblems = getPricingProblems(m_inst);
    do {
        reducedCost = false;
        if (m_mainProblem.solve()) {
            m_fractObj = m_mainProblem.getObjValue();
            std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
            assert(m_mainProblem.checkReducedCosts());
            // Generate location columns
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                locFutures.emplace_back(
                    m_pool.enqueue([&, this, f]() -> std::optional<FunctionLocation> {
                        auto& lpp = m_lpps[m_threadMap.at(std::this_thread::get_id())];
                        lpp.setFunction(f);
                        if (lpp.solve(m_mainProblem) == IloFalse) {
                            throw std::runtime_error("Cannot solve function placement pricing problem");
                        }
                        if (lpp.getObjValue() > -RC_EPS) {
                            return std::nullopt;
                        }
                        return lpp.getLocations();
                    }));
            }

            // Generate path columns
            for (int i = 0; i < m_inst->demands.size(); ++i) {
                pathFutures.emplace_back(
                    m_pool.enqueue([&, this, i]() -> std::optional<ServicePath> {
                        auto& ppp = m_ppps[m_threadMap.at(std::this_thread::get_id())];
                        ppp.setDemandID(i);
                        ppp.updateDual(m_mainProblem);
                        if (ppp.solve() == IloFalse) {
                            throw std::runtime_error("Cannot solve path pricing problem");
                        }
                        if (ppp.getObjValue() > -RC_EPS) {
                            return std::nullopt;
                        }
                        return ppp.getServicePath();
                    }));
            }

            // Add locations
            for (auto& fut : locFutures) {
                auto loc = fut.get();
                if (loc.has_value()) {
                    reducedCost = true;
                    m_mainProblem.addLocation(loc.value());
                }
            }

            // Add paths
            for (auto& fut : pathFutures) {
                auto sPath = fut.get();
                if (sPath.has_value()) {
                    reducedCost = true;
                    m_mainProblem.addPath(sPath.value());
                }
            }
        } else {
            m_mainProblem.m_solver.exportModel("rm.lp");
            std::cout << "No solution found for RMP" << '\n';
        }
    } while (reducedCost);
    return m_fractObj > 0.0;
}
} // namespace SFC::OccLim
