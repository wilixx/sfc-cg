#include "ChainingPathOccLimit.hpp"

namespace SFC::OccLim {

// ##########################################################################################################
// ##########################################################################################################
// ##########################################################################################################

PricingProblemLP::PricingProblemLP(const Instance* _inst)
    : m_inst(_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env,
            m_inst->NFVNodes.size() * (nbLayers - 1));
        for (const auto& u : m_inst->NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                a[getIndexA(u, j)] =
                    IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
                // setIloName(a[getIndexA(u, j)],
                //     "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_b([&]() {
        IloNumVarArray b(m_env, m_inst->NFVNodes.size() * m_inst->funcCharge.size());
        for (const auto& u : m_inst->NFVNodes) {
            for (int f = 0; f < m_inst->funcCharge.size();
                 ++f) {
                b[getIndexB(u, f)] =
                    IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
                // setIloName(b[getIndexB(u, f)],
                //     "b" + toString(std::make_tuple(f, u)));
            }
        }
        return b;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (int e = 0; e < m; ++e) {
            for (int j = 0; j < nbLayers; ++j) {
                // setIloName(f[getIndexF(e, j)],
                //     "f" + toString(std::make_tuple(m_inst->edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u = 0; u < m_inst->network.getOrder();
             ++u) {
            for (int j = 0; j < nbLayers; ++j) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& v : m_inst->network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_inst->edgeToId(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_inst->edgeToId(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_inst->isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(
                    m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
                vars.end();
                vals.end();
            }
        }
        return flowConsCons;
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e = 0; e < m_inst->edges.size(); ++e) {
            linkCapaCons[e].setUB(m_inst->network.getEdgeWeight(
                m_inst->edges[e]));
            // setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto& u : m_inst->NFVNodes) {
            nodeCapaCons[u] = IloAdd(
                m_model, IloRange(m_env, 0.0, m_inst->nodeCapa[u]));
            // setIloName(nodeCapaCons[u],
            //     "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_funcNodeUsageCons([&]() {
        IloRangeArray funcNodeUsageCons(
            m_env, m_inst->NFVNodes.size() * m_inst->funcCharge.size());
        for (const auto& u : m_inst->NFVNodes) {
            for (int f = 0; f < m_inst->funcCharge.size();
                 ++f) {
                funcNodeUsageCons[getIndexB(u, f)] = IloAdd(
                    m_model, IloRange(m_env, 0.0,
                                 IloNum(m_inst->maxChainSize) * m_b[getIndexB(u, f)],
                                 IloInfinity));
                // setIloName(funcNodeUsageCons[getIndexB(u, f)],
                //     "funcNodeUsageCons" + toString(std::make_tuple(f, u)));
            }
        }
        return funcNodeUsageCons;
    }())
    , m_nbLicensesCons([&]() {
        IloRangeArray nbLicensesCons(m_env,
            m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            for (const auto& u : m_inst->NFVNodes) {
                vars.add(m_b[getIndexB(u, f)]);
            }
            nbLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, 0.0, IloSum(vars),
                                    m_inst->nbLicenses));
            vars.end();
        }
        return nbLicensesCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {
    std::cout << "Rows: " << m_solver.getNrows() << ", cols:" << m_solver.getNcols() << '\n';
}

int PricingProblemLP::getIndexF(
    const int _e, const int _j) const {
    assert(_e < m_inst->network.size());
    assert(_j < nbLayers);
    return m * _j + _e;
}

int PricingProblemLP::getIndexA(
    const Graph::Node _u, const int _j) const {
    assert(_u < m_inst->network.getOrder());
    assert(_j < nbLayers);
    return m_inst->NFVNodes.size() * _j + m_inst->NFVIndices[_u];
}

int PricingProblemLP::getIndexB(
    const Graph::Node _u, const function_descriptor _f) const {
    assert(_u < m_inst->network.getOrder());
    assert(_f < m_inst->funcCharge.size());
    return m_inst->NFVNodes.size() * _f + m_inst->NFVIndices[_u];
}

void PricingProblemLP::setPricingID(const int _i) {
    m_solver.clearModel();
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(
        0.0, 0.0);
    m_flowConsCons[n * m_inst->demands[m_demandID].functions.size() + m_inst->demands[m_demandID].t]
        .setBounds(0.0, 0.0);

    m_demandID = _i;
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(
        1.0, 1.0);
    m_flowConsCons[n * m_inst->demands[m_demandID].functions.size() + m_inst->demands[m_demandID].t]
        .setBounds(-1.0, -1.0);

    for (int e = 0; e < m_inst->edges.size(); ++e) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j = 0;
             j <= m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->demands[m_demandID].d);
        }
        for (int j(m_inst->demands[m_demandID].functions.size() + 1);
             j < nbLayers; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(0);
        }
        m_linkCapaCons[e].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
    }

    for (const auto& u : m_inst->NFVNodes) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j = 0;
             j < m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(m_inst->nbCores(m_demandID, j));
        }
        for (int j =
                 m_inst->demands[m_demandID].functions.size();
             j < nbLayers - 1; ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(0);
        }
        m_nodeCapaCons[u].setLinearCoefs(vars, vals);
        // setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
        vars.end();
        vals.end();
    }

    // Function node usage
    for (const auto& u : m_inst->NFVNodes) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int j = 0;
                 j < m_inst->demands[m_demandID].functions.size(); ++j) {
                vars.add(m_a[getIndexA(u, j)]);
                if (m_inst->demands[m_demandID].functions[j] == f) {
                    vals.add(-1.0);
                } else {
                    vals.add(0.0);
                }
            }
            for (int j =
                     m_inst->demands[m_demandID].functions.size();
                 j < nbLayers - 1; ++j) {
                vars.add(m_a[getIndexA(u, j)]);
                vals.add(0.0);
            }
            m_funcNodeUsageCons[getIndexB(u, f)].setLinearCoefs(vars, vals);
            vars.end();
            vals.end();
        }
    }
}

void PricingProblemLP::updateDual(const Placement_DWD& _rmp) {
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);

    for (int e = 0; e < m; ++e) {
        for (int j = 0;
             j <= m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->demands[m_demandID].d * (1 - _rmp.getLinkCapacityDual(e)));
        }
    }

    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(-_rmp.getNodeCapacityDual(u) * m_inst->nbCores(m_demandID, j));

            vars.add(m_b[getIndexB(u, m_inst->demands[m_demandID].functions[j])]);
            vals.add(-_rmp.getPathFunctionDual(m_demandID, j, u));
        }
    }

    m_obj.setConstant(-_rmp.getOnePathDual(m_demandID));
    m_obj.setLinearCoefs(vars, vals);
    m_solver.extract(m_model);
    vars.end();
    vals.end();
}

void PricingProblemLP::updateDual_stab(const Placement_DWD& _rmp) {
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);

    // Intra layer links
    for (int e = 0; e < m; ++e) {
        for (int j = 0;
             j <= m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_inst->demands[m_demandID].d
                     - m_inst->demands[m_demandID].d * _rmp.getLinkCapacityDual_stab(e));
        }
    }

    // Between layer links
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0;
             j < m_inst->demands[m_demandID].functions.size(); ++j) {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(-_rmp.getNodeCapacityDual_stab(u) * m_inst->nbCores(m_demandID, j));
        }
    }
    for (const auto& u : m_inst->NFVNodes) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            vars.add(m_b[getIndexB(u, f)]);
            vals.add(_rmp.getPathFunctionDual_stab(m_demandID, f, u));
        }
    }

    m_obj.setConstant(-_rmp.getOnePathDual_stab(m_demandID));
    m_obj.setLinearCoefs(vars, vals);
    m_solver.extract(m_model);
    vars.end();
    vals.end();
}

double PricingProblemLP::getObjValue() const {
    return m_solver.getObjValue();
}

bool PricingProblemLP::solve() {
    return m_solver.solve();
}

ServicePath PricingProblemLP::getColumn() const {
    Graph::Node u = m_inst->demands[m_demandID].s;
    ServicePath retval{{u}, {}, m_demandID};

    IloNumArray aVal(m_env);
    m_solver.getValues(aVal, m_a);

    int j = 0;
    while (u != m_inst->demands[m_demandID].t
           || j < m_inst->demands[m_demandID].functions.size()) {
        if (j < m_inst->demands[m_demandID].functions.size()
            && m_inst->isNFV[u]
            && epsilon_equal<double>()(aVal[getIndexA(u, j)], 1.0)) {
            retval.locations.push_back(u);
            ++j;
        } else {
            for (const auto& v : m_inst->network.getNeighbors(u)) {
                if (m_solver.getValue(
                        m_f[getIndexF(m_inst->edgeToId(u, v), j)])
                    > 0) {
                    retval.nPath.push_back(v);
                    u = v;
                    break;
                }
            }
        }
    }

    assert(retval.nPath.back() == m_inst->demands[m_demandID].t);
    aVal.end();
    return retval;
}

// ################################################################################################
// ################################################################################################
// ################################################################################################

PricingProblemBF::PricingProblemBF(const Instance* _inst)
    : m_inst(_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_layeredGraph([=]() {
        DiGraph layeredGraph(n * nbLayers);
        for (const auto& u : m_inst->NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                layeredGraph.addEdge(n * j + u, n * (j + 1) + u);
            }
        }
        for (int j = 0; j < nbLayers; ++j) {
            for (const auto& edge : m_inst->network.getEdges()) {
                layeredGraph.addEdge(n * j + edge.first, n * j + edge.second);
            }
        }
        return layeredGraph;
    }()) {}

void PricingProblemBF::setPricingID(const int _demandID) {
    m_demandID = _demandID;
}

void PricingProblemBF::updateDual(const Placement_DWD_Alpha& _rmp) {
    m_objValue = -_rmp.getOnePathDual(m_demandID);
    // Inter layer weight
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                -_rmp.getNodeCapacityDual(u) * m_inst->nbCores(m_demandID, j)
                - _rmp.getPathFunctionDual(m_demandID, j, u));
        }
    }

    // Same layer weight
    const auto edges = m_inst->network.getEdges();
    for (int j = 0; j < nbLayers; ++j) {
        for (int e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
                m_inst->demands[m_demandID].d
                    * (1 - _rmp.getLinkCapacityDual(e)));
        }
    }

    isNegative = std::any_of(m_layeredGraph.getEdges().begin(), m_layeredGraph.getEdges().end(), [&](auto&& _edge){
        return epsilon_less<double>()(m_layeredGraph.getEdgeWeight(_edge), 0.0);
    });
    // for (const auto& edge : ) {
    //     if () {
    //         isNegative = true;
    //     }
    // }
}

void PricingProblemBF::updateDual(const Placement_EM& _rmp) {
    m_objValue = -_rmp.getOnePathDual(m_demandID);
    
    // Inter layer weight
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                -_rmp.getNodeCapacityDual(u) * m_inst->nbCores(m_demandID, j)
                    - _rmp.getPathFunctionDual(m_demandID, j, u));
        }
    }

    // Same layer weight
    const auto edges = m_inst->network.getEdges();
    for (int j = 0; j < nbLayers; ++j) {
        for (int e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
                m_inst->demands[m_demandID].d
                    * (1 - _rmp.getLinkCapacityDual(e)));
        }
    }

    isNegative = std::any_of(m_layeredGraph.getEdges().begin(), m_layeredGraph.getEdges().end(), [&](auto&& _edge){
        return epsilon_less<double>()(m_layeredGraph.getEdgeWeight(_edge), 0.0);
    });
}

void PricingProblemBF::updateDual_stab(const Placement_EM& _rmp) {
    // Inter layer weight
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                -_rmp.getNodeCapacityDual_stab(u) * m_inst->nbCores(m_demandID, j)
                    - _rmp.getPathFunctionDual_stab(m_demandID, j, u));
        }
    }

    // Same layer weight
    const auto edges = m_inst->network.getEdges();
    for (int j = 0; j < nbLayers; ++j) {
        for (int e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
                m_inst->demands[m_demandID].d
                    * (1 - _rmp.getLinkCapacityDual_stab(e)));
        }
    }

    isNegative = false;
    for (const auto& edge : m_layeredGraph.getEdges()) {
        if (m_layeredGraph.getEdgeWeight(edge) < 0) {
            isNegative = true;
        }
    }
    m_objValue = -_rmp.getOnePathDual_stab(m_demandID);
}

void PricingProblemBF::updateDual(const Placement_DWD_Alpha::DualValues& _dualValues) {
    // Inter layer weight
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                _dualValues.getReducedCost(CrossLayerLink{m_demandID, u, j}));
        }
    }

    // Same layer weight
    const auto edges = m_inst->network.getEdges();
    for (int j = 0; j < nbLayers; ++j) {
        for (int e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
               _dualValues.getReducedCost(IntraLayerLink{m_demandID, edges[e], j}));
        }
    }

    isNegative = false;
    for (const auto& edge : m_layeredGraph.getEdges()) {
        if (m_layeredGraph.getEdgeWeight(edge) < 0) {
            // std::cout << "Negative weight: " << m_layeredGraph.getEdgeWeight(edge) << "\n";
            isNegative = true;
        }
    }
    m_objValue = -_dualValues.m_onePathDuals[m_demandID];
}

bool PricingProblemBF::solve() {
    const Graph::Node dest = n * (m_inst->demands[m_demandID].functions.size()) + m_inst->demands[m_demandID].t;
    if (isNegative) {
        m_path = m_bf.getShortestPath(m_inst->demands[m_demandID].s, dest);
        m_objValue += m_bf.getDistance(dest);
    } else {
        m_path = m_dijkstra.getShortestPath(m_inst->demands[m_demandID].s, dest);
        m_objValue += m_dijkstra.getDistance(dest);
    }
    return !m_path.empty();
}

double PricingProblemBF::getObjValue() const {
    return m_objValue;
}

ServicePath PricingProblemBF::getColumn() const {
    return SFC::getServicePath(m_path, n, m_demandID);
}
} // namespace SFC::OccLim
