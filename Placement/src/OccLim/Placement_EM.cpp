#include "ChainingPathOccLimit.hpp"
#include "cplex_utility.hpp"

namespace SFC::OccLim {
Placement_EM::Placement_EM(const Instance* _inst)
    : m_inst(_inst)
    , m_f([&]() {
        std::cout << "Adding m_f...";
        IloNumVarArray f(m_env, m_inst->network.size() * m_inst->demands.size(), 0.0, 1.0, ILOFLOAT);
        std::cout << "done\n";
        IloAdd(m_integerConversions, IloConversion(m_env, f, ILOINT));
        return IloAdd(m_model, f);
    }())
    , m_a([&]() {
        std::cout << "Adding m_a...";
        IloNumVarArray a(m_env, m_inst->maxChainSize * m_inst->network.getOrder() * m_inst->demands.size(), 0.0, 1.0, ILOFLOAT);
        std::cout << "done\n";
        IloAdd(m_integerConversions, IloConversion(m_env, a, ILOINT));
        return IloAdd(m_model, a);
    }())
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_b([&]() {
        IloNumVarArray b(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (const auto& u : m_inst->NFVNodes) {
                b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env));
                // setIloName(b[getIndexB(u, f)], "b[u=" + std::to_string(u) + ", f=" + std::to_string(f) + "]");
            }
        }
        IloAdd(m_integerConversions, IloConversion(m_env, b, ILOBOOL));
        return b;
    }())
    , m_obj([&]() {
            std::cout << "Adding m_obj...";
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int e = 0; e < m_inst->network.size(); ++e) {
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    vars.add(m_f[getIndexF(e, i)]);
                    vals.add(m_inst->demands[i].d);
                }
            }
            auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
            vars.end();
            vals.end();
            std::cout << "done\n";
            return obj;
        }())
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_inst->demands.size(), 1.0,
            1.0);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            // setIloName(onePathCons[i],
                // "onePathCons" + toString(m_inst->demands[i]));
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        std::cout << "Adding m_linkCapasCons...";
        IloRangeArray retval(m_env, m_inst->network.size(), 0.0, 0.0);
        for (int e = 0; e < m_inst->network.size(); ++e) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int i = 0; i < m_inst->demands.size(); ++i) {
                vars.add(m_f[getIndexF(e, i)]);
                vals.add(m_inst->demands[i].d);
            }
            retval[e] = (IloScalProd(vars, vals) <= m_inst->network.getEdgeWeight(m_inst->edges[e]));
            vars.end();
            vals.end();
        }
        std::cout << "done\n";
        return IloAdd(m_model, retval);
    }())
    , m_nodeCapasCons([&]() {
        std::cout << "Adding m_nodeCapacityConstraints...";
        IloRangeArray retval(m_env, m_inst->network.getOrder(), 0.0, 0.0);
        for (const auto& u : m_inst->NFVNodes) {
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int i = 0; i < m_inst->demands.size(); ++i) {
                for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                    vars.add(m_a[getIndexA(u, i, j)]);
                    vals.add(m_inst->nbCores(i, j));
                }
            }
            retval[u] = (IloScalProd(vars, vals) <= IloInt(m_inst->nodeCapa[u]));
            vars.end();
            vals.end();
        }
        std::cout << "done\n";
        return IloAdd(m_model, retval);
    }())
    , m_nbLicensesCons([&]() {
        std::cout << "Adding m_nbLicensesCons...";
        IloRangeArray nbLicensesCons(m_env, m_inst->funcCharge.size());
        IloNumVarArray vars(m_env, m_inst->NFVNodes.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            int i = 0;
            for (const auto& u : m_inst->NFVNodes) {
                vars[i] = m_b[getIndexB(u, f)];
                ++i;
            }
            nbLicensesCons[f] = IloAdd(m_model, IloRange(m_env, -IloInfinity, IloSum(vars), m_inst->nbLicenses));            
        }
        vars.end();
        std::cout << "done\n";
        return nbLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        std::cout << "Adding m_funcNodeUsageCons...";
        IloRangeArray funcNodeUsageCons(m_env, m_inst->NFVNodes.size() * m_inst->maxChainSize * m_inst->demands.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (const auto& u : m_inst->NFVNodes) {
                for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                    funcNodeUsageCons[getIndexA(u, i, j)] = IloAdd(m_model, IloRange(m_env, 0.0, m_b[getIndexB(u, m_inst->demands[i].functions[j])] - m_a[getIndexA(u, i, j)], IloInfinity));
                }
            }
        }
        std::cout << "done\n";
        return funcNodeUsageCons;
    }())
    , m_relation_f([&](){
        std::cout << "Adding m_relation_f...";
        IloRangeArray retval(m_env, m_f.getSize());
        for(IloInt i = 0; i < m_f.getSize(); ++i) {
            retval[i] = IloRange(m_env, 0.0 -m_f[i], 0.0);
        }
        std::cout << "done\n";
        return IloAdd(m_model, retval);
    }())
    , m_relation_a([&](){
        std::cout << "Adding m_relation_a...";
        IloRangeArray retval(m_env, m_a.getSize());
        for(IloInt i = 0; i < m_a.getSize(); ++i) {
            retval[i] = IloRange(m_env, 0.0 -m_a[i], 0.0);
        }
        std::cout << "done\n";
        return IloAdd(m_model, retval);
    }())
    , m_dummyPaths([&]() {
        IloNumVarArray retval(m_env);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            retval.add(m_onePathCons[i](1.0) + m_obj(2 * m_inst->demands[i].d * m_inst->network.getOrder()));
            // setIloName(retval[retval.getSize() - 1], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , relation_a(m_env, m_relation_a.getSize())
    , relation_f(m_env, m_relation_f.getSize())
    , m_solver([&]() {
        IloCplex retval(m_model);
        retval.setOut(retval.getEnv().getNullStream());
        retval.setWarning(retval.getEnv().getNullStream());
        retval.setParam(IloCplex::Threads, 1);
        retval.setParam(IloCplex::RootAlg, IloCplex::Concurrent);
        return retval;
    }()) {}

double Placement_EM::getObjValue() const {
    return m_fractObj;
}

Placement_EM::Solution Placement_EM::getSolution() const {
    return Placement_EM::Solution(*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), m_paths.size());
}

double Placement_EM::getReducedCost(const SFC::ServicePath& _sPath) const {
    return getReducedCost(_sPath, normalDuals);
}

void Placement_EM::addColumns(const std::vector<Column>& _sPaths) {
    // std::cout << "Adding: " << _sPath << '\n';
    IloNumColumnArray columns(m_env);
    columns.setSize(_sPaths.size());
    int idx = 0;
    for(const auto& _sPath : _sPaths) {
        assert([&]() {
            const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
            if (ite != m_paths.end()) {
                std::cout << _sPath << " is already present!" << '\n';
                return false;
            }
            return true;
        }());
        
        IloRangeArray vars(m_env, _sPath.locations.size() + _sPath.nPath.size() -1);
        IloNumArray vals(m_env, vars.getSize());
        int index = 0;
        int j = 0;
        for (auto iteU = _sPath.nPath.begin(), iteV = std::next(iteU); iteV != _sPath.nPath.end(); ) {
            if(j < _sPath.locations.size() && _sPath.locations[j] == *iteU) {
                vars[index] = (m_relation_a[getIndexA(*iteU, _sPath.demand, j)]);
                vals[index] = +(1.0);
                ++index;
                ++j;
            } else {
                vars[index] = (m_relation_f[getIndexF(m_inst->edgeToId(*iteU, *iteV), _sPath.demand)]);
                vals[index] = +(1.0);
                ++index;
                ++iteU;
                ++iteV;
            }
        }
        for(; j < _sPath.locations.size(); ++j) {
            vars[index] = (m_relation_a[getIndexA(_sPath.nPath.back(), _sPath.demand, j)]);
            vals[index] = +(1.0);
            ++index;
        }

        columns[idx] = IloNumColumn(m_env);
        columns[idx] += m_onePathCons[_sPath.demand](1.0);
        columns[idx] += vars(vals);
        ++idx;
        vals.end();
        vars.end();
    }
    m_y.add(IloNumVarArray(m_env, columns));
    m_paths.reserve(m_paths.size() + _sPaths.size());
    m_paths.insert(m_paths.end(), _sPaths.begin(), _sPaths.end());
}

// double Placement_EM::getLowerBound(const double _reducedCost, const DualValues& _dualValues) const {
//     IloNumArray dummyVals(m_env);
//     m_solver.getValues(dummyVals, m_dummyPaths);

//     double total = 0.0;
//     for (int i = 0; i < m_inst->demands.size(); ++i) {
//         total += ((2 * m_inst->demands[i].d * m_inst->network.getOrder()) - _dualValues.m_onePathDuals[i]) * dummyVals[i];
//     }
//     std::cout << "total: " << total << ", ";
//     return total + _reducedCost + getDualSumRHS(_dualValues) + getAdditionalVariable(_dualValues);
// }

// double Placement_EM::getBestLowerBound(const double _bestLowerBound, const std::vector<Column>& _columns) {
//     const double LB_st = getLowerBound(_reducedCost, stabDuals);
//     std::cout << "Stab. LB: " << LB_st << '\n';
//     if (LB_st > _bestLowerBound) {
//         bestDuals = stabDuals;
//         return LB_st;
//     }
//     return _bestLowerBound;
// }

double Placement_EM::getReducedCost(const ServicePath& _sPath, const DualValues& _dualValues) const {
    return -_dualValues.m_onePathDuals[_sPath.demand] + getBaseLagragian(_sPath, _dualValues);
}

double Placement_EM::getBaseLagragian(const ServicePath& _sPath, const DualValues& _dualValues) const {
    const auto & [ nPath, locations, demand ] = _sPath;
    double retval = 0.0;

    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        retval += m_inst->demands[demand].d * (1 - _dualValues.m_linkCapasDuals[m_inst->edgeToId(*iteU, *iteV)]);
    }
    for (int j = 0; j < locations.size(); ++j) {
        retval += - m_inst->nbCores(demand, j) * _dualValues.m_nodeCapasDuals[locations[j]];
    }    

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.m_pathFuncDuals[getIndexPathFunc(demand, j, locations[j])];
    }
    
    return retval;
}

bool Placement_EM::checkReducedCosts() const {
    IloNumArray funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
    for (int k = 0; k < m_paths.size(); ++k) {
        const double solverRC =
            m_solver.getReducedCost(m_y[k]);
        const double myRC = getReducedCost(m_paths[k], normalDuals);
        if (!epsilon_equal<double>()(solverRC, myRC)) {
            std::cerr << "myRC: " << myRC << ", solverRC: " << solverRC << '\n';
            return false;
        }
    }
    return true;
}

bool Placement_EM::solveInteger() {
    m_solver.clearModel();
    IloModel integerModel(m_env);
    integerModel.add(m_model);
    integerModel.add(m_integerConversions);
    for (int i = 0; i < m_inst->demands.size(); ++i) {
        integerModel.add(IloRange(m_env, 0.0, IloNumExprArg(m_dummyPaths[i]), 0.0));
    }
    m_solver.extract(integerModel);
    m_solver.setOut(std::cout);
    
    // Setting priorities for location variables
    for (IloInt i = 0; i < m_b.getSize(); ++i) {
        m_solver.setPriority(m_b[i], 1.0);
    }

    if (m_solver.solve() == IloFalse) {
        std::cout << "No solution found!\n";
        return false;
    }
    
    // Solution found
    m_intObj = m_solver.getObjValue();
    std::cout << "Final obj value: " << m_intObj << "\t# Paths: " << m_paths.size() << '\n';
    m_solver.exportModel("ILP.lp");
    return true;
}

double Placement_EM::getDualSumRHS(const DualValues& /*_dualValues*/) const {
    double retval = 0.0;
    
    // for (const auto u : m_inst->NFVNodes) {
    //     retval += _dualValues.m_nodeCapasDuals[m_inst->NFVIndices[u]] * m_inst->nodeCapa[u];
    // }

    // std::cout << "[nc:" << retval << ", ";     
    // const auto edges = m_inst->network.getEdges();
    // for (int e = 0; e < m_inst->network.size(); ++e) {
    //     retval += _dualValues.m_linkCapasDuals[e] * m_inst->network.getEdgeWeight(edges[e]);
    // }
    // std::cout << "lc:" << retval << ", ";

    // retval += IloSum(_dualValues.m_onePathDuals);
    // std::cout << "+ op: " << retval << ", ";

    // retval += IloSum(_dualValues.m_nbLicensesDuals) * m_inst->nbLicenses;
    // std::cout << " + nl: " << retval << "], ";
    return retval;
}

double Placement_EM::getAdditionalVariable(const DualValues& /*_dualValues*/) const {
    // IloNumArray bCoeff(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
    // for (int i = 0; i < m_inst->demands.size(); ++i) {
    //     for (const auto& u : m_inst->NFVNodes) {
    //         for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
    //             bCoeff[getIndexB(u, m_inst->demands[i].functions[j])] += _dualValues.m_pathFuncDuals[getIndexPathFunc(i, j, u)];
    //         }
    //     }
    // }

    // for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
    //     for (const auto& u : m_inst->NFVNodes) {
    //         bCoeff[getIndexB(u, f)] += -_dualValues.m_nbLicensesDuals[f];
    //     }
    // }
    double retval = 0.0;
    // for(IloInt i = 0; i < bCoeff.getSize(); ++i) {
    //     if (bCoeff[i] < 0) {
    //         retval += bCoeff[i];
    //     }
    // }
    // std::cout << "[av:" << retval << "]\t";
    return retval;
}

bool Placement_EM::solve() {
    m_solver.extract(m_model);
    if (m_solver.solve() == IloFalse) {
        return false;
    }
    m_fractObj = m_solver.getObjValue();
    getDuals();
    assert(checkReducedCosts());
    m_solver.clear();
    return true;
}

void Placement_EM::getDuals() {
    m_solver.getDuals(normalDuals.m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(normalDuals.m_nodeCapasDuals, m_nodeCapasCons);
    m_solver.getDuals(normalDuals.m_pathFuncDuals, m_pathFuncCons);
    m_solver.getDuals(normalDuals.m_onePathDuals, m_onePathCons);
    m_solver.getDuals(normalDuals.m_nbLicensesDuals, m_nbLicensesCons);

    for (auto i = 0; i < normalDuals.m_onePathDuals.getSize(); ++i) {
        stabDuals.m_onePathDuals[i] = m_alpha * normalDuals.m_onePathDuals[i]
                                      + ((1 - m_alpha) * bestDuals.m_onePathDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_linkCapasDuals.getSize(); ++i) {
        stabDuals.m_linkCapasDuals[i] = m_alpha * normalDuals.m_linkCapasDuals[i]
                                        + ((1 - m_alpha) * bestDuals.m_linkCapasDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_nodeCapasDuals.getSize(); ++i) {
        stabDuals.m_nodeCapasDuals[i] = m_alpha * normalDuals.m_nodeCapasDuals[i]
                                        + ((1 - m_alpha) * bestDuals.m_nodeCapasDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_pathFuncDuals.getSize(); ++i) {
        stabDuals.m_pathFuncDuals[i] = m_alpha * normalDuals.m_pathFuncDuals[i]
                                       + ((1 - m_alpha) * bestDuals.m_pathFuncDuals[i]);
    }
    for (auto i = 0; i < normalDuals.m_nbLicensesDuals.getSize(); ++i) {
        stabDuals.m_nbLicensesDuals[i] = m_alpha * normalDuals.m_nbLicensesDuals[i]
                                      + ((1 - m_alpha) * bestDuals.m_nbLicensesDuals[i]);
    }
}

std::vector<ServicePath>
Placement_EM::getServicePaths() const {
    std::vector<ServicePath> retval(m_inst->demands.size());
    IloNumArray yVals(m_env);
    m_solver.getValues(m_y, yVals);
    for (int k = 0; k < m_paths.size(); ++k) {
        if (IloRound(yVals[k]) > 0) {
            retval[m_paths[k].demand] = m_paths[k];
        }
    }
    return retval;
}
} // namespace SFC::OccLim
