#include "ChainingPathOccLimit.hpp"
#include "cplex_utility.hpp"

using ranges = boost::multi_array_types::index_range;

namespace SFC::OccLim {
Placement_DWD_Alpha::Placement_DWD_Alpha(const Instance* _inst)
    : m_inst(_inst)
    , m_env([&](){
        IloEnv env;
        env.setNormalizer(IloFalse);
        return env;
    }())
    , m_b([&]() {
        boost::multi_array<IloNumVar, 2> b(boost::extents[m_inst->network.getOrder()][m_inst->funcCharge.size()]);
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
                b[u][f] = IloNumVar(m_env);
                IloAdd(m_integerConversions, IloConversion(m_env, b[u][f], ILOBOOL));
                IloAdd(m_model, b[u][f]);
                setIloName(b[u][f], "b[u=" + std::to_string(u) + ", f=" + std::to_string(f) + "]");
            }
        }
        return b;
    }())
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons([&]() {
        std::vector<IloRange> onePathCons(m_inst->demands.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            onePathCons[i] = IloRange(m_env, 1.0, 1.0);
            m_model.add(onePathCons[i]);
            setIloName(onePathCons[i],
                "onePathCons" + toString(m_inst->demands[i]));
        }
        return onePathCons;
    }())
    , m_linkCapasCons([&]() {
        std::vector<IloRange> linkCapaCons(m_inst->network.size());
        std::generate(linkCapaCons.begin(), linkCapaCons.end(), [this, e=0]() mutable {
            const auto& edge = m_inst->edges[e++];
            return IloAdd(m_model,
                IloRange(m_env, -IloInfinity, m_inst->network.getEdgeWeight(edge),
                    std::string("linkCapas" + toString(edge)).c_str()));
        });
        return linkCapaCons;
    }())
    , m_nodeCapasCons([&]() {
        std::vector<IloRange> nodeCapasCons(m_inst->network.getOrder());
        std::generate(nodeCapasCons.begin(), nodeCapasCons.end(), [this, u=0]() mutable {
            return IloAdd(m_model,
                IloRange(m_env, -IloInfinity, m_inst->nodeCapa[u],
                    std::string("nodeCapasCons" + std::to_string(u)).c_str()));
        });
        return nodeCapasCons;
    }())
    , m_nbLicensesCons([&]() {
        std::vector<IloRange> funcLicensesCons(m_inst->funcCharge.size());
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            const auto allNode_f = m_b[boost::indices[ranges(0, m_inst->network.getOrder())][f]];
            funcLicensesCons[f] =
                IloAdd(m_model, IloRange(m_env, -IloInfinity, 
                    std::accumulate(allNode_f.begin(), allNode_f.end(), IloExpr(m_env)),
                    m_inst->nbLicenses));
            setIloName(funcLicensesCons[f],
                "funcLicensesCons(" + toString(f) + ")");
            // vars.end();
        }
        return funcLicensesCons;
    }())
    , m_pathFuncCons([&]() {
        boost::multi_array<IloRange, 3> pathFuncCons(
            boost::extents[m_inst->demands.size()][m_inst->maxChainSize][m_inst->network.getOrder()]);
        
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            for (int j = 0; j < m_inst->demands[i].functions.size();
                 ++j) {
                for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
                    pathFuncCons[i][j][u] =
                        IloAdd(m_model, 
                            IloRange(m_env, -IloInfinity, -m_b[u][m_inst->demands[i].functions[j]], 0.0));
                    setIloName(pathFuncCons[i][j][u],
                        "pathFuncCons" + toString(std::make_tuple(i, j, u)));
                }
            }
        }
        return pathFuncCons;
    }())
    , m_dummyPaths([&]() {
        std::vector<IloNumVar> retval(m_inst->demands.size());
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            retval[i] = m_onePathCons[i](1.0) + m_obj(2 * m_inst->demands[i].d * m_inst->network.getOrder());
            m_model.add(retval[i]);
            setIloName(retval[i], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(m_inst->network.size())
    , m_nodeCharge(m_inst->NFVNodes.size())
    , m_funcPath(boost::extents[m_inst->maxChainSize][m_inst->NFVNodes.size()])
    , m_solver([&]() {
        IloCplex retval(m_model);
        retval.setOut(retval.getEnv().getNullStream());
        retval.setWarning(retval.getEnv().getNullStream());
        retval.setParam(IloCplex::NodeAlg, IloCplex::Primal);
        retval.setParam(IloCplex::Param::Simplex::Perturbation::Indicator, 1);
        retval.setParam(IloCplex::Param::Simplex::PGradient, 4);
        return retval;
    }())
    {
        m_solver.exportModel("RMP.lp");
    }

double Placement_DWD_Alpha::getObjValue() const {
    return m_fractObj;
}

Placement_DWD_Alpha::Solution Placement_DWD_Alpha::getSolution() const {
    Placement_DWD_Alpha::Solution sol(*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), m_paths.size());
    m_solver.extract(m_model);
    return sol;
}

double Placement_DWD_Alpha::getReducedCost(const SFC::ServicePath& _sPath) const {
    return getReducedCost(_sPath, normalDuals);
}

double Placement_DWD_Alpha::getReducedCost(const ServicePath& _sPath, const DualValues& _dualValues) const {
    const auto & [ nPath, locations, demandID ] = _sPath;
    double retval = -_dualValues.m_onePathDuals[demandID];

    for (auto iteU = nPath.begin(), iteV = std::next(iteU);
         iteV != nPath.end(); ++iteU, ++iteV) {
        retval += _dualValues.getReducedCost(IntraLayerLink{demandID, {*iteU, *iteV}, -1}); /// layer # is not use for the reduced cost
    }

    for (int j = 0; j < locations.size(); ++j) {
        retval += _dualValues.getReducedCost(CrossLayerLink{demandID, locations[j], j});
    }
    return retval;
}

IloNumColumn Placement_DWD_Alpha::getColumn(const Column& _col) {
    const auto & [nPath, locations, demandID] = _col;
    const auto & [s, t, d, chain] = m_inst->demands[demandID];
    assert([&]() {
        const auto ite = std::find(m_paths.begin(), m_paths.end(), _col);
        if (ite != m_paths.end()) {
            std::cout << _col << " is already present!" << '\n';
            return false;
        }
        return true;
    }());
    std::fill(m_linkCharge.begin(), m_linkCharge.end(), 0.0);
    std::fill(m_nodeCharge.begin(), m_nodeCharge.end(), 0.0);
    IloNumColumn col = m_onePathCons[demandID](1.0) + m_obj(d * (nPath.size() - 1));
    for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end();
         ++iteU, ++iteV) {
        m_linkCharge[m_inst->edgeToId(*iteU, *iteV)] += d;
    }
    const auto mult = [](const IloRange& _const, double _val) {
        return _const(_val);
    };
    const auto add = [](auto&& _acc, auto&& _newVal) {
        return _acc += _newVal;
    };
    col += std::inner_product(m_linkCapasCons.begin(), m_linkCapasCons.end(), m_linkCharge.begin(),
        IloNumColumn(m_env), add, mult);
    for (int j = 0; j < locations.size(); ++j) {
        m_nodeCharge[locations[j]] += m_inst->nbCores(demandID, j);
        col += m_pathFuncCons[demandID][j][locations[j]](1.0);
    }
    col += std::inner_product(m_nodeCapasCons.begin(), m_nodeCapasCons.end(), m_nodeCharge.begin(),
        IloNumColumn(m_env), add, mult);
    return col;
}

void Placement_DWD_Alpha::addColumns(const std::vector<Column>& _sPaths) {
    for(const auto& _sPath : _sPaths) {
        IloAdd(m_integerConversions, m_y.emplace_back(getColumn(_sPath)));

    }
    m_paths.reserve(m_paths.size() + _sPaths.size());
    m_paths.insert(m_paths.end(), _sPaths.begin(), _sPaths.end());
}

double Placement_DWD_Alpha::getLowerBound(const double _rc, const std::vector<Column>& _col, const DualValues& _dualValues) {
    return _rc 
        + getDualSumRHS(_dualValues)
        + getAdditionalVariable(_dualValues, _col);
}

/*double Placement_DWD_Alpha::getStabilizedLowerBound(const std::vector<Column>& _columns) {
    // std::cout << "\ngetStabilizedLowerBound: ";
    return getLowerBound(_columns, stabDuals);
}*/

double Placement_DWD_Alpha::getBestLowerBound(const double _bestLowerBound, const double _rc, const std::vector<Column>& _col) {
    if(m_dummyActive) {
        return _bestLowerBound;
    }
    // const double N_st = getLowerBound(_columns, stabDuals);
    // const double best_st = getLowerBound(_columns, stabDuals);
    const double LB_stab = getLowerBound(_rc, _col, stabDuals);
    const double LB_best = _bestLowerBound;//getLowerBound(_rc, _col, bestDuals);
    // std::cout << "Normal LB: " << N_st << '\t';
    std::cout << "Best LB: " << LB_best << '\t';
    std::cout << "Stab. LB: " << LB_stab << '\n';
    if (LB_stab > LB_best) {
        bestDuals = stabDuals;
        return LB_stab;
    }
    return LB_best;
}

// double Placement_DWD_Alpha::getBaseLagragian(const ServicePath& _sPath, const DualValues& _dualValues) const {
//     const auto & [ nPath, locations, demand ] = _sPath;
//     double retval = -_dualValues.m_onePathDuals[_sPath.demand];

//     for (auto iteU = nPath.begin(), iteV = std::next(iteU);
//          iteV != nPath.end(); ++iteU, ++iteV) {
//         retval += m_inst->demands[demand].d * (1 - _dualValues.m_linkCapasDuals[m_inst->edgeToId(*iteU, *iteV)]);
//     }    
//     for (int j = 0; j < locations.size(); ++j) {
//         retval += - m_inst->nbCores(demand, j) * _dualValues.m_nodeCapasDuals[locations[j]]
//                   - _dualValues.m_pathFuncDuals[getIndexPathFunc(demand, j, locations[j])];
//     }
//     return retval;
// }


bool Placement_DWD_Alpha::checkReducedCosts() const {
    IloNumArray funcPath(m_env, m_inst->funcCharge.size() * m_inst->NFVNodes.size());
    for (int k = 0; k < m_paths.size(); ++k) {
        const double solverRC =
            m_solver.getReducedCost(m_y[k]);
        const double myRC = getReducedCost(m_paths[k], normalDuals);
        if (!epsilon_equal<double>()(solverRC, myRC)) {
            std::cerr << "myRC: " << myRC << ", solverRC: " << solverRC << '\n';
            return false;
        }
    }
    return true;
}

void Placement_DWD_Alpha::relax() {
    m_solver.extract(m_model);

}
bool Placement_DWD_Alpha::solveInteger() {
    // m_solver.clearModel();
    IloModel integerModel(m_env);
    integerModel.add(m_model);
    integerModel.add(m_integerConversions);
    m_solver.extract(integerModel);
    m_solver.setOut(std::cout);

    
    // Setting priorities for location variables
    for(auto arr : m_b) {
        for(IloNumVar b : arr) {
            m_solver.setPriority(b, 1.0);
        }
    }

    if (m_solver.solve() == IloFalse) {
        std::cout << "No solution found: best LB: " << m_fractObj << "\t# Paths: " << m_paths.size() << '\n';
        m_solver.exportModel("IMP.lp");
        return false;
    }
    // Solution found
    m_intObj = m_solver.getObjValue();
    std::cout << "Final obj value: " << m_intObj << ", best LB: " << m_fractObj << "\t# Paths: " << m_paths.size() << '\n';
    return true;
}

double Placement_DWD_Alpha::getDualSumRHS(const DualValues& _dualValues) const {
    double retval = 0.0;
    
    for (const auto u : m_inst->NFVNodes) {
        //retval += _dualValues.m_nodeCapasDuals[u] * m_inst->nodeCapa[u];
        retval += _dualValues.m_nodeCapasDuals[u] * m_nodeCapasCons[u].getUB();
    }

    // std::cout << "[nc:" << retval << ", ";
    const auto& edges = m_inst->network.getEdges();
    for (int e = 0; e < m_inst->network.size(); ++e) {
        retval += _dualValues.m_linkCapasDuals[e] * m_inst->network.getEdgeWeight(edges[e]);
    }
    // std::cout << "lc:" << retval << ", ";
    retval += std::accumulate(_dualValues.m_onePathDuals.begin(), _dualValues.m_onePathDuals.end(), 0.0);

    for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
        retval += _dualValues.m_nbLicensesDuals[f] * m_inst->nbLicenses;    
    }
    // std::cout << "m_nbLicensesDuals: " << _dualValues.m_nbLicensesDuals << '\n';
    // std::cout << "nl: " << retval << "], ";
    return retval;
}

double Placement_DWD_Alpha::getAdditionalVariable(const DualValues& _dualValues, const std::vector<Column>& /*_col*/) const {
    boost::multi_array<IloNum, 2> bCoeff(boost::extents[m_b.shape()[0]][m_b.shape()[1]]);
    std::fill(bCoeff.data(), bCoeff.data()+bCoeff.num_elements(), 0.0);

    for (int i = 0; i < m_inst->demands.size(); ++i) {
        for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
            for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                bCoeff[u][m_inst->demands[i].functions[j]] += _dualValues.m_pathFuncDuals[i][j][u];
            }
        }
    }

    for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            bCoeff[u][f] += -_dualValues.m_nbLicensesDuals[f];
        }
    }
    
    // std::cout << "[# Neg: ";
    // for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
    //     int nbNeg = 0;
    //     for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
    //         if(epsilon_less<double>()(bCoeff[getIndexB(u, f)], 0.0)) {
    //             ++nbNeg;
    //         }
    //     }
    //     std::cout << nbNeg << ", ";
    // }
    // std::cout << "]\n";
    
    // return bCoeff;
    double retval = 0.0;
    // for(const auto& _sPath : _col) {
    //     for(int j = 0; j < _sPath.locations.size(); ++j) {
    //         retval += bCoeff[getIndexB(_sPath.locations[j], m_inst->demands[_sPath.demand].functions[j])];
    //     }
    // }
    for (Graph::Node u = 0; u < m_inst->network.getOrder(); ++u) {
        for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
            if (epsilon_less<double>()(bCoeff[u][f], 0.0)) {
                retval += bCoeff[u][f];
            }
        }
    }
    std::cout << "[av:" << retval << "]\t";
    return retval;
}

void Placement_DWD_Alpha::removeDummyIfPossible() {
    if(m_dummyActive) {
        const double dummySum = std::accumulate(m_dummyPaths.begin(), m_dummyPaths.end(), 0.0, 
            [&](auto&& _acc, auto&& _var){
                return _acc + m_solver.getValue(_var);
        });
        if(epsilon_equal<double>()(dummySum, 0.0)) {
            m_dummyActive = false;
            std::cout << "\n######################################################\nRemoved dummy...\n######################################################\n";
            // m_model.remove(m_dummyPaths);
            for(auto& dummy : m_dummyPaths) {
                dummy.removeFromAll();
            }

        }
    }
}

void Placement_DWD_Alpha::getDuals() {
    const auto getDual = [&](const IloRange& _cons){
        return m_solver.getDual(_cons);
    };
    std::transform(m_linkCapasCons.begin(), m_linkCapasCons.end(), normalDuals.m_linkCapasDuals.begin(), getDual);
    std::transform(m_nodeCapasCons.begin(), m_nodeCapasCons.end(), normalDuals.m_nodeCapasDuals.begin(), getDual);
    std::transform(m_pathFuncCons.data(), m_pathFuncCons.data()+m_pathFuncCons.num_elements(), 
        normalDuals.m_pathFuncDuals.data(), getDual);
    std::transform(m_onePathCons.begin(), m_onePathCons.end(), normalDuals.m_onePathDuals.begin(), getDual);
    std::transform(m_nbLicensesCons.begin(), m_nbLicensesCons.end(), normalDuals.m_nbLicensesDuals.begin(), getDual);

    std::cout << "\nPrimal : " << m_fractObj 
        << " -> Dual optimal values with normal: " << getDualSumRHS(normalDuals);
    if(!m_dummyActive) {
        std::generate(stabDuals.m_linkCapasDuals.begin(), stabDuals.m_linkCapasDuals.end(), 
            [i = 0, this]() mutable {
            int j = i;
            ++i;
            return m_alpha * normalDuals.m_linkCapasDuals[j]
                + ((1 - m_alpha) * bestDuals.m_linkCapasDuals[j]);
        });
        std::generate(stabDuals.m_nodeCapasDuals.begin(), stabDuals.m_nodeCapasDuals.end(),
            [i = 0, this]() mutable {
            int j = i;
            ++i;
            return m_alpha * normalDuals.m_nodeCapasDuals[j]
                + ((1 - m_alpha) * bestDuals.m_nodeCapasDuals[j]);
        });
        std::generate(stabDuals.m_pathFuncDuals.data(), stabDuals.m_pathFuncDuals.data()+stabDuals.m_pathFuncDuals.num_elements(),
            [i = 0, this]() mutable {
                int j = i;
                ++i;
                return m_alpha * *(normalDuals.m_pathFuncDuals.data()+j) 
                    + ((1 - m_alpha) * *(bestDuals.m_pathFuncDuals.data()+j));
        });
        std::generate(stabDuals.m_onePathDuals.begin(), stabDuals.m_onePathDuals.end(),
            [i = 0, this]() mutable {
            int j = i;
            ++i;
            return m_alpha * normalDuals.m_onePathDuals[j]
                + ((1 - m_alpha) * bestDuals.m_onePathDuals[j]);
        });
        std::generate(stabDuals.m_nbLicensesDuals.begin(), stabDuals.m_nbLicensesDuals.end(),
            [i = 0, this]() mutable {
            int j = i;
            ++i;
            return m_alpha * normalDuals.m_nbLicensesDuals[j]
                + ((1 - m_alpha) * bestDuals.m_nbLicensesDuals[j]);
        });
        
        std::cout << ", stab.: " << getDualSumRHS(stabDuals)
            << ", best: " << getDualSumRHS(bestDuals)
            << '\n';
    } else {
        stabDuals = normalDuals;
    }
    std::cout << '\n';
    std::cout << "Shape of duals: " << normalDuals << '\n';
    assert(std::fabs(getDualSumRHS(normalDuals) - m_fractObj) < 1e-6);
}

const Placement_DWD_Alpha::DualValues& Placement_DWD_Alpha::getDualValues() const {
    if(m_dummyActive) {
        return normalDuals;    
    } else {
        return stabDuals;
    }
    
}

std::vector<ServicePath>
Placement_DWD_Alpha::getServicePaths() const {
    std::vector<ServicePath> retval(m_inst->demands.size());
    for (int k = 0; k < m_paths.size(); ++k) {
        if (epsilon_equal<double>()(m_solver.getValue(m_y[k]), IloTrue)) {
            retval[m_paths[k].demand] = m_paths[k];
        }
    }
    return retval;
}

std::ostream& operator<<(std::ostream& _out, const Placement_DWD_Alpha::DualValues& _dualValues) {
    return _out << "lc: " << std::accumulate(_dualValues.m_linkCapasDuals.begin(), _dualValues.m_linkCapasDuals.end(), 0.0) 
    << ", nc: " << std::accumulate(_dualValues.m_nodeCapasDuals.begin(), _dualValues.m_nodeCapasDuals.end(), 0.0)
    << ", pf: " << std::accumulate(_dualValues.m_pathFuncDuals.data(), _dualValues.m_pathFuncDuals.data()+_dualValues.m_pathFuncDuals.num_elements(), 0.0)
    << ", op: " << std::accumulate(_dualValues.m_onePathDuals.begin(), _dualValues.m_onePathDuals.end(), 0.0)
    << ", nl: " << std::accumulate(_dualValues.m_nbLicensesDuals.begin(), _dualValues.m_nbLicensesDuals.end(), 0.0) << '\n';
}
} // namespace SFC::OccLim
