#include "ChainingPathOccLimit.hpp"

Lagrangian_Path::Lagrangian_Path(const Instance* _inst)
    : m_inst(_inst)
    , n(_inst.network.getOrder())
    , m(_inst.network.size())
    , nbLayers(_inst.maxChainSize)
    , m_a([&]() {
        IloNumVarArray a(m_env, m_inst.network.getOrder() * m_inst.maxChainSize * m_inst.demands.size());
        for (const auto& u : m_inst.NFVNodes) {
            for (int i = 0; i < m_inst.demands.size(); ++i) {
                for (int j = 0; j < m_inst.demands[i].functions.size(); ++j) {
                    const int aIndex = getIndexA(u, i, j);
                    a[aIndex] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                    setIloName(a[aIndex], "a" + toString(std::make_tuple(u, i, j)));
                }
            }
        }
        return a;
    }())
    , m_b([&]() {
        IloNumVarArray b(m_env, m_inst.network.getOrder() * m_inst.funcCharge.size());
        for (const auto& u : m_inst.NFVNodes) {
            for (int f = 0; f < m_inst.funcCharge.size(); ++f) {
                b[getIndexB(u, f)] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0, ILOBOOL));
                setIloName(b[getIndexB(u, f)], "b" + toString(std::make_tuple(u, f)));
            }
        }
        return b;
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);

        for (int e = 0; e < m_inst.network.size(); ++e) {
            for (int i = 0; i < m_inst.demands.size(); ++i) {
                for (int j = 0; j <= m_inst.demands[i].functions.size(); ++j) {
                    vars.add(m_f[getIndexF(e, i, j)]);
                    vals.add(m_inst.demands[i].d);
                }
            }
        }
        // Link capacity
        for () {
        }
        return IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowCons = IloRangeArray(m_env, m_inst.network.getOrder() * (m_inst.maxChainSize + 1) * m_inst.demands.size());
        for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
            for (int demandID = 0; demandID < m_inst.demands.size(); ++demandID) {
                for (int j = 0; j <= m_inst.demands[demandID].functions.size(); ++j) {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for (const auto& v : m_inst.network.getNeighbors(u)) {
                        // Outgoing flows
                        vars.add(m_f[getIndexF(m_inst.edgeToId(u, v), demandID, j)]);
                        vals.add(1.0);
                        // Incoming flows
                        vars.add(m_f[getIndexF(m_inst.edgeToId(v, u), demandID, j)]);
                        vals.add(-1.0);
                    }
                    if (m_inst.isNFV[u]) {
                        if (j < m_inst.demands[demandID].functions.size()) { // Going next layer
                            vars.add(m_a[getIndexA(u, demandID, j)]);
                            vals.add(1.0);
                        }
                        if (j > 0) { // Coming from last layer
                            vars.add(m_a[getIndexA(u, demandID, j - 1)]);
                            vals.add(-1.0);
                        }
                    }
                    const int fIndex = getIndexFC(u, demandID, j);
                    flowCons[getIndexFC(u, demandID, j)] = IloAdd(m_model, IloScalProd(vars, vals) == 0);
                    vars.end();
                    vals.end();
                    setIloName(flowCons[fIndex], "flowCons" + toString(std::make_tuple(u, demandID, j)));
                }
            }
        }

        for (int i = 0; i < m_inst.demands.size(); ++i) {
            flowCons[getIndexFC(m_inst.demands[i].s, i, 0)].setBounds(1.0, 1.0);
            flowCons[getIndexFC(m_inst.demands[i].t, i, m_inst.demands[i].functions.size())].setBounds(-1.0, -1.0);
        }
        return flowCons;
    }())