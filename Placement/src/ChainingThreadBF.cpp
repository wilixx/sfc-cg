#include "ChainingThreadBF.hpp"

namespace SFC {

// bool solveInteger() {
//     if (solve() && m_mainProblem.solveInteger()) {
//         m_intObj = m_mainProblem.getObjValue();
//         return true;
//     }
//     return false;
// }

PlacementDWD_Path::PlacementDWD_Path(const Instance* _inst)
    : m_inst(_inst)
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_inst->demands.size(), 1.0, 1.0);
        // for (int i = 0; i < m_inst->demands.size(); ++i) {
        //     // setIloName(onePathCons[i], "onePathCons" + toString(m_inst->demands[i]));
        // }
        m_model.add(onePathCons);
        return onePathCons;
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_inst->network.size(), -IloInfinity, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_inst->edges) {
            linkCapaCons[e].setUB(m_inst->network.getEdgeWeight(edge));
            // setIloName(linkCapaCons[e], "linkCapas" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons([&]() {
        IloRangeArray nodeCapasCons(m_env, m_inst->NFVNodes.size(), -IloInfinity, 0.0);
        for (const auto& u : m_inst->NFVNodes) {
            nodeCapasCons[m_inst->NFVIndices[u]].setUB(m_inst->nodeCapa[u]);
            // setIloName(nodeCapasCons[i], "nodeCapasCons" + std::to_string(u));
        }
        return IloAdd(m_model, nodeCapasCons);
    }())
    , m_dummyPaths([&]() {
        IloNumVarArray retval(m_env);
        for (int i = 0; i < m_inst->demands.size(); ++i) {
            retval.add(m_onePathCons[i](1.0) + m_obj(2 * m_inst->demands[i].d * m_inst->network.getOrder()));
            // setIloName(retval[retval.getSize() - 1], "dummyPaths" + toString(m_inst->demands[i]));
        }
        return retval;
    }())
    , m_linkCharge(m_env, m_inst->network.size())
    , m_nodeCharge(m_env, m_inst->NFVNodes.size())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setOut(m_env.getNullStream());
        solver.setParam(IloCplex::Threads, 1);
        solver.setParam(IloCplex::RootAlg, IloCplex::Dual);
        return solver;
    }()) {}

PlacementDWD_Path::Solution PlacementDWD_Path::getSolution() const {
    return PlacementDWD_Path::Solution(*m_inst, getIntegerObjValue(), getRelaxationObjValue(),
        getServicePaths(), m_paths.size());
}

std::vector<ServicePath> PlacementDWD_Path::getServicePaths() const {
    std::vector<ServicePath> retval(m_inst->demands.size());
    IloNumArray yVals(m_env);
    m_solver.getValues(m_y, yVals);
    for (int k = 0; k < m_paths.size(); ++k) {
        if (IloRound(yVals[k]) > 0) {
            retval[m_paths[k].demand] = m_paths[k];
        }
    }
    return retval;
}

double PlacementDWD_Path::getDualSumRHS() const {
    double retval = 0.0;
    for (const auto u : m_inst->NFVNodes) {
        retval += getNodeCapacityDual(u) * m_inst->nodeCapa[u];
    }
    std::cout << retval << '\t';

    const auto& edges = m_inst->network.getEdges();
    for (int e = 0; e < m_inst->network.size(); ++e) {
        retval += getLinkCapacityDual(e) * m_inst->network.getEdgeWeight(edges[e]);
    }
    std::cout << retval << '\t';

    for (int i = 0; i < m_inst->demands.size(); ++i) {
        retval += getOnePathDual(i);
    }
    std::cout << retval << '\t';
    return retval;
}

void PlacementDWD_Path::addColumns(const std::vector<Column>& _sPaths) {
    // const auto pair = std::make_pair(_sPath, _i);
    IloNumColumnArray columns(m_env);
    columns.setSize(_sPaths.size());
    int idx = 0;
    for (const auto& _sPath : _sPaths) {
        assert([&]() {
            const auto ite = std::find(m_paths.begin(), m_paths.end(), _sPath);
            if (ite != m_paths.end()) {
                std::cout << _sPath << " is already present!" << '\n';
                return false;
            }
            return true;
        }());

        const auto & [ nPath, locations, demandID ] = _sPath;

        std::fill(begin(m_linkCharge), end(m_linkCharge), 0.0);
        for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end(); ++iteU, ++iteV) {
            m_linkCharge[m_inst->edgeToId(*iteU, *iteV)] += m_inst->demands[demandID].d;
        }

        std::fill(begin(m_nodeCharge), end(m_nodeCharge), 0.0);
        for (int j = 0; j < locations.size(); ++j) {
            m_nodeCharge[m_inst->NFVIndices[locations[j]]] += m_inst->nbCores(demandID, j);
        }

        columns[idx] = IloNumColumn(m_env);
        columns[idx] += m_onePathCons[demandID](1.0);
        columns[idx] += m_obj(m_inst->demands[demandID].d * (nPath.size() - 1));
        columns[idx] += m_linkCapasCons(m_linkCharge);
        columns[idx] += m_nodeCapasCons(m_nodeCharge);
        ++idx;
    }
    m_y.add(IloNumVarArray(m_env, columns));
    // setIloName(m_y[m_y.getSize() - 1], "y" + toString(std::make_tuple(m_inst->demands[demandID], _sPath)));
    m_paths.reserve(m_paths.size() + _sPaths.size());
    m_paths.insert(m_paths.end(), _sPaths.begin(), _sPaths.end());
}

bool PlacementDWD_Path::checkReducedCosts() const {
    for (int k = 0; k < m_paths.size(); ++k) {
        const auto & [ nPath, locations, demandID ] = m_paths[k];
        double rc = -m_onePathDuals[demandID];
        for (auto iteU = nPath.begin(), iteV = std::next(iteU); iteV != nPath.end(); ++iteU, ++iteV) {
            rc += m_inst->demands[demandID].d
                  * (1 - m_linkCapasDuals[m_inst->edgeToId(*iteU, *iteV)]);
        }
        for (int j = 0; j < locations.size(); ++j) {
            rc += -m_nodeCapasDuals[m_inst->NFVIndices[locations[j]]] * m_inst->nbCores(demandID, j);
        }
        const auto solverRC = m_solver.getReducedCost(m_y[k]);
        if (!epsilon_equal<double>()(solverRC, rc)) {
            std::cout << solverRC << " but " << rc;
            return false;
        }
    }
    return true;
}

bool PlacementDWD_Path::solveInteger() {
    m_solver.extract(m_model);
    m_model.add(IloConversion(m_env, m_y, ILOBOOL));
    m_solver.setParam(IloCplex::Threads, 1);
    m_solver.setOut(std::cout);
    if (m_solver.solve() == IloTrue) {
        m_intObj = m_solver.getObjValue();
        std::cout << "Final obj value: " << m_intObj 
            << "\t# Paths: " << m_paths.size() << '\n';
        getNetworkUsage(m_solver);
        return true;
    }
    return false;
}

void PlacementDWD_Path::getDuals() {
    m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
}

double PlacementDWD_Path::getRelaxationObjValue() const {
    return m_fractObj;
}

double PlacementDWD_Path::getIntegerObjValue() const {
    return m_intObj;
}

void PlacementDWD_Path::removeDummyIfPossible() {
    if (m_dummyActive) {
        IloNumArray dumVal(m_env);
        m_solver.getValues(dumVal, m_dummyPaths);
        if (IloSum(dumVal) < 1e-6) {
            m_dummyActive = false;
            std::clog << "\n######################################################\nRemoved dummy...\n######################################################\n";
            //m_model.remove(m_dummyPaths);
            m_dummyPaths.removeFromAll();
        }
    }
}

void PlacementDWD_Path::getNetworkUsage(const IloCplex& _sol) const {
    std::vector<ServicePath> sPaths;
    sPaths.reserve(m_inst->demands.size());
    IloNumArray yVals(m_env);
    _sol.getValues(m_y, yVals);

    for (int i = 0; i < m_paths.size(); ++i) {
        if (epsilon_equal<double>()(yVals[i], IloTrue)) {
            sPaths.push_back(m_paths[i]);
        }
    }
    showNetworkUsage(*m_inst, sPaths);
}

double PlacementDWD_Path::getObjValue() const {
    return m_fractObj;
}

// #####################################################################
// #####################################################################
// #####################################################################

PricingProblemBF::PricingProblemBF(const Instance* _inst)
    : m_inst(_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_demandID(0)
    , m_layeredGraph([=]() {
        DiGraph layeredGraph(n * nbLayers);
        for (const auto& u : m_inst->NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                layeredGraph.addEdge(n * j + u, n * (j + 1) + u);
            }
        }
        for (int j = 0; j < nbLayers; ++j) {
            for (const auto& edge : m_inst->network.getEdges()) {
                layeredGraph.addEdge(n * j + edge.first, n * j + edge.second);
            }
        }
        return layeredGraph;
    }()) {}

void PricingProblemBF::setPricingID(const int _demandID) {
    m_demandID = _demandID;
}

void PricingProblemBF::updateDual(const PlacementDWD_Path& _rmp) {
    // Inter layer weight
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                -_rmp.getNodeCapacityDual(u) * m_inst->nbCores(m_demandID, j));
        }
    }

    // Same layer weight
    const auto& edges = m_inst->network.getEdges();
    for (int j = 0; j < nbLayers; ++j) {
        for (int e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
                m_inst->demands[m_demandID].d
                    * (1 - _rmp.getLinkCapacityDual(e)));
        }
    }

    isNegative = false;
    for (const auto& edge : m_layeredGraph.getEdges()) {
        if (epsilon_less<double>()(m_layeredGraph.getEdgeWeight(edge), 0)) {
            // std::cout << "Negative weight: " << m_layeredGraph.getEdgeWeight(edge) << "\n";
            isNegative = true;
        }
    }
    m_objValue = -_rmp.getOnePathDual(m_demandID);
}

bool PricingProblemBF::solve() {
    const Graph::Node dest = n * (m_inst->demands[m_demandID].functions.size()) + m_inst->demands[m_demandID].t;
    if (isNegative) {
        m_path = m_bf.getShortestPath(m_inst->demands[m_demandID].s, dest);
        m_objValue += m_bf.getDistance(dest);
    } else {
        m_path = m_dijkstra.getShortestPath(m_inst->demands[m_demandID].s, dest);
        m_objValue += m_dijkstra.getDistance(dest);
    }
    return !m_path.empty();
}

double PricingProblemBF::getObjValue() const {
    return m_objValue;
}

ServicePath PricingProblemBF::getColumn() const {
    return SFC::getServicePath(m_path, n, m_demandID);
}

// #####################################################################
// #####################################################################
// #####################################################################

PricingProblemLP::PricingProblemLP(const Instance* _inst)
    : m_inst(_inst)
    , n(m_inst->network.getOrder())
    , m(m_inst->network.size())
    , nbLayers(m_inst->maxChainSize + 1)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env,
            m_inst->NFVNodes.size() * (nbLayers - 1));
        for (const auto& u : m_inst->NFVNodes) {
            for (int j = 0; j < nbLayers - 1; ++j) {
                a[getIndexA(u, j)] =
                    IloAdd(m_model, IloNumVar(m_env, 0, 1, ILOBOOL));
                // setIloName(a[getIndexA(u, j)],
                //     "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (int e = 0; e < m; ++e) {
            for (int j = 0; j < nbLayers; ++j) {
                // setIloName(f[getIndexF(e, j)],
                //     "f" + toString(std::make_tuple(m_inst->edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u(0); u < m_inst->network.getOrder(); ++u) {
            for (int j = 0; j < nbLayers; ++j) {
                // IloNumExpr expr(m_env);
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& v : m_inst->network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_inst->edgeToId(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_inst->edgeToId(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_inst->isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(
                    m_model, IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
                vars.end();
                vals.end();
            }
        }

        return flowConsCons;
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e = 0; e < m_inst->edges.size(); ++e) {
            linkCapaCons[e].setUB(m_inst->network.getEdgeWeight(
                m_inst->edges[e]));
            // setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto& u : m_inst->NFVNodes) {
            nodeCapaCons[u] = IloAdd(
                m_model, IloRange(m_env, 0.0, m_inst->nodeCapa[u]));
            // setIloName(nodeCapaCons[u],
            // "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_env);
        solver.setParam(IloCplex::Threads, 1);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {}

int PricingProblemLP::getIndexF(
    const int _e, const int _j) const {
    assert(_e < m_inst->network.size());
    assert(_j < nbLayers);
    return m * _j + _e;
}

int PricingProblemLP::getIndexA(
    const Graph::Node _u, const int _j) const {
    assert(_u < m_inst->network.getOrder());
    assert(_j < nbLayers);
    return m_inst->NFVNodes.size() * _j + m_inst->NFVIndices[_u];
}

bool PricingProblemLP::solve() {
    return m_solver.solve();
}

double PricingProblemLP::getObjValue() const {
    return m_solver.getObjValue();
}

void PricingProblemLP::setPricingID(const int _i) {
    m_solver.clearModel();
    int nbFunctions =
        m_inst->demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(
        0.0, 0.0);
    m_flowConsCons[n * nbFunctions + m_inst->demands[m_demandID].t]
        .setBounds(0.0, 0.0);

    m_demandID = _i;
    nbFunctions = m_inst->demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_inst->demands[m_demandID].s].setBounds(
        1.0, 1.0);
    m_flowConsCons[n * nbFunctions + m_inst->demands[m_demandID].t]
        .setBounds(-1.0, -1.0);

    for (int e = 0; e < m_inst->edges.size(); ++e) {
        IloExpr expr(m_env);
        for (int j = 0; j <= nbFunctions; ++j) {
            expr += m_f[getIndexF(e, j)] * m_inst->demands[m_demandID].d;
        }
        m_linkCapaCons[e].setExpr(expr);
        expr.end();
    }
    for (const auto& u : m_inst->NFVNodes) {
        IloExpr expr(m_env);
        for (int j = 0; j < nbFunctions; ++j) {
            expr += m_a[getIndexA(u, j)] * m_inst->nbCores(m_demandID, j);
        }
        m_nodeCapaCons[u].setExpr(expr);
        expr.end();
    }
}

void PricingProblemLP::updateDual(const SFC::PlacementDWD_Path& _rmp) {
    const auto& d = m_inst->demands[m_demandID].d;
    const auto& functions = m_inst->demands[m_demandID].functions;
    const auto nbFunctions = functions.size();

    IloNumExpr expr(m_env, -_rmp.getOnePathDual(m_demandID));
    for (int e = 0; e < m; ++e) {
        for (int j = 0; j <= nbFunctions; ++j) {
            expr += m_f[getIndexF(e, j)] * (d - d * _rmp.getLinkCapacityDual(e));
        }
    }
    for (const auto& u : m_inst->NFVNodes) {
        for (int j = 0; j < functions.size(); ++j) {
            expr += m_a[getIndexA(u, j)] * (-_rmp.getNodeCapacityDual(u) * m_inst->nbCores(m_demandID, j));
        }
    }
    m_obj.setExpr(expr);
    expr.end();
    m_solver.extract(m_model);
}

ServicePath PricingProblemLP::getColumn() const {
    const auto& funcs = m_inst->demands[m_demandID].functions;
    const auto& t = m_inst->demands[m_demandID].t;

    Graph::Node u = m_inst->demands[m_demandID].s;
    ServicePath retval{{u}, {}, m_demandID};
    int j = 0;

    IloNumArray aVal(m_env);
    m_solver.getValues(aVal, m_a);

    while (u != t || j < funcs.size()) {
        if (j < funcs.size() && m_inst->isNFV[u] && epsilon_equal<double>()(aVal[getIndexA(u, j)], IloTrue)) {
            retval.locations.push_back(u);
            ++j;
        } else {
            for (const auto& v : m_inst->network.getNeighbors(u)) {
                if (epsilon_equal<double>()(m_solver.getValue(
                        m_f[getIndexF(m_inst->edgeToId(u, v), j)]), IloTrue)) {
                    retval.nPath.push_back(v);
                    u = v;
                    break;
                }
            }
        }
    }

    assert(retval.nPath.back() == t);
    return retval;
}

} // namespace SFC
