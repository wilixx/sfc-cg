#include "ChainingPathOccLimit.hpp"
#include "ChainingPathOccLimitDoublePP.hpp"
#include "ChainingThreadBFFixed.hpp"
#include "ColumnGeneration.hpp"
#include "KMedoids.hpp"
#include "Localisation.hpp"
#include "LocalisationPerFunction.hpp"
#include "MyTimer.hpp"
#include "SFC.hpp"
#include "SFCOccIlp.hpp"
#include <DiGraph.hpp>
#include <algorithm>
#include <array>
#include <ilconcert/iloalg.h>
#include <ilconcert/ilosys.h>
#include <ilcplex/ilocplexi.h>
#include <iosfwd>
#include <stddef.h>
#include <tclap/ArgException.hpp>
#include <tclap/CmdLine.hpp>
#include <tclap/UnlabeledValueArg.hpp>
#include <tclap/ValueArg.hpp>
#include <tclap/ValuesConstraint.hpp>

struct Param {
    std::string model;
    std::string name;
    int nbThreads;
    double factor;
    double alpha;
    double epsilon;
    double percentDemand;
    int replicationLimit;
};

Param getParams(int argc, char** argv);

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with limits on the function replicas with column generation", ' ', "1.0");

    TCLAP::ValuesConstraint<std::string> vCons(
        {"pathThread", "lowerBound", "path_loc", "ILP", "benders", "pathPred",
            "path_heur_pf", "heur", "heur_pf", "ILP", "LP", "heur_pf_asc",
            "heur_pf_desc", "BB", "heur_pf_nr", "heur_pf_med_asc",
            "heur_pf_med_desc", "pathThread_stab", 
            "path_alpha_clear", "path_alpha_no_clear", "path_alpha_stab",
            "path_em"});
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used",
        false, "pathThread", &vCons);
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName(
        "network", "Specify the network name", true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<int> replicationLimit(
        "r", "replicationLimit",
        "Specify the maximum amount of replication in functions", true, 3,
        "int >= 0");
    cmd.add(&replicationLimit);

    TCLAP::ValueArg<double> demandFactor(
        "d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "double");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<double> alpha(
        "a", "alpha", "Specify the alpha",
        false, -1, "double");
    cmd.add(&alpha);

    TCLAP::ValueArg<double> epsilon(
        "e", "epsilon", "Specify the epsilon",
        false, 1e-6, "double");
    cmd.add(&epsilon);

    TCLAP::ValueArg<int> nbThreads(
        "j", "nbThreads", "Specify the number of threads",
        false, 1, "int");
    cmd.add(&nbThreads);

    // TCLAP::ValueArg<int> functionChages(
    //     "c", "functionChages", "Specify the index for the function charge file",
    //     false, 4, "int >= 0");
    // cmd.add(&functionChages);

    TCLAP::ValueArg<double> percentDemand("p", "percentDemand",
        "Specify the percentage of demand used",
        false, 100.0, "double");
    cmd.add(&percentDemand);

    cmd.parse(argc, argv);

    return {modelName.getValue(), networkName.getValue(),
        nbThreads.getValue(), demandFactor.getValue(),
        alpha.getValue(), epsilon.getValue(),
        percentDemand.getValue(), replicationLimit.getValue()};
}

Matrix<char> getFunctionPlacement_perFunc(
    SFC::OccLim::Instance inst,
    const std::vector<SFC::function_descriptor>& _funcs);
std::vector<int>
getFunctionsAscendingCores(const SFC::OccLim::Instance& inst);
std::vector<int>
getFunctionsDescendingCores(const SFC::OccLim::Instance& inst);
Matrix<char> getFunctionPlacement_medoids(
    const SFC::OccLim::Instance& _inst,
    const std::vector<SFC::function_descriptor>& _funcs);

int main(const int argc, char** argv) {
    std::cout << std::fixed;
    auto params = getParams(argc, argv);
    epsilon_value = params.epsilon;
    
    std::string folderName = "./instances/occlim/";
    DiGraph<double> network = SFC::loadNetwork(folderName + params.name + "_topo.txt");
    const auto[allDemands, funcCharge] = SFC::loadDemands(
        folderName + params.name + "_demand.txt", params.factor);
    const auto& allDemands_ref = allDemands;
    std::cout << "Function requirements: " << funcCharge << '\n';
    auto nodeCapas =
        SFC::loadNodeCapa(folderName + params.name + "_nodeCapa.txt");
    std::cout << "Files loaded...\n";

    const int nbDemands =
        static_cast<int>(std::ceil(allDemands.size() * params.percentDemand / 100.0));
    std::vector<SFC::Demand> demands = std::vector<SFC::Demand>(allDemands.begin(),
        allDemands.begin() + nbDemands);

    std::cout << "# demands: " << nbDemands << '\n';
    int totalNbCores = 0;
    for (const auto& demand : demands) {
        for (int j = 0; j < demand.functions.size(); ++j) {
            totalNbCores += ceil(demand.d / funcCharge[demand.functions[j]]);
        }
    }
    std::cout << totalNbCores << " needed cores!" << '\n';

    std::vector<Graph::Node> NFVNodes;
    for (Graph::Node u = 0; u < network.getOrder(); ++u) {
        if (nodeCapas[u] != -1) {
            NFVNodes.push_back(u);
        }
    }
    int availableCores = 0;
    for (const auto& u : NFVNodes) {
        availableCores += nodeCapas[u];
    }
    std::cout << availableCores << " cores available !\n";

    SFC::OccLim::Instance inst(network, nodeCapas, demands, funcCharge,
        NFVNodes, params.replicationLimit);

    const std::string filename = [&]() {
        std::string retval = "./results/occlim/" + params.name;
        if (nbDemands != allDemands_ref.size()) {
            retval += "_" + std::to_string(nbDemands);
        }
        retval += "_" + params.model;
        if (params.alpha >= 0.0) {
            retval += "_" + std::to_string(params.alpha);
        }
        retval += "_" + std::to_string(params.factor) + "_" + std::to_string(params.replicationLimit) + ".res";
        return retval;
    }();

    Time timer;
    timer.start();
    if (params.model == "pathThread") {
        ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> cgModel(&inst);
        if (cgModel.solve<SFC::OccLim::PricingProblemLP>(params.nbThreads)) {
            cgModel.getSolution().save(filename, timer.get());
        }
    } else if (params.model == "path_alpha_clear") {
        ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD_Alpha> cgModel(&inst);
        if (cgModel.solve<SFC::OccLim::PricingProblemBF, true>(params.nbThreads)) {
            cgModel.getSolution().save(filename, timer.get());
        }
    } else if (params.model == "path_alpha_no_clear") {
        ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD_Alpha> cgModel(&inst);
        if (cgModel.solve<SFC::OccLim::PricingProblemBF, false>(params.nbThreads)) {
            cgModel.getSolution().save(filename, timer.get());
        }
    } else if (params.model == "path_alpha_stab") {
        try {
            ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD_Alpha> cgModel(&inst);
            if (cgModel.solveStabilized<SFC::OccLim::PricingProblemBF>(params.nbThreads, params.alpha)) {
                cgModel.getSolution().save(filename, timer.get());
            }
        } catch (const IloException& e) {
            std::cout << e.getMessage() << '\n';
        }
    } else if (params.model == "LP") {
        SFC::OccLim::SFCOccIlp ilp(inst);
        ilp.solveRelaxation();
    } else if (params.model == "ILP") {
        SFC::OccLim::SFCOccIlp ilp(inst);
        ilp.solve();
    } else if (params.model == "path_em") {
        ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_EM> cgModel(&inst);
        try {
            if (cgModel.solve<SFC::OccLim::PricingProblemBF>(params.nbThreads)) {
                cgModel.getSolution().save(filename, timer.get());
            }
        } catch (const IloException& e) {
            std::cout << e.getMessage() << '\n';
        }
    }
    //    else if (params.model == "lowerBound") {
    //       ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> css(inst);
    //       css.solve();
    //       css.getSolution().save(filename, timer.get());
    //   } else if (params.model == "path_heur_pf") {
    //       // First, get solution from heur
    //       // heur_pf
    //       std::vector<int> functions(funcCharge.size());
    //       std::iota(functions.begin(), functions.end(), 0);

    //       SFC::Placed::Instance inst1(
    //           inst, getFunctionPlacement_perFunc(inst, functions));
    //       SFC::Placed::Instance inst2(
    //           inst,
    //           getFunctionPlacement_perFunc(inst, getFunctionsAscendingCores(inst)));
    //       SFC::Placed::Instance inst3(inst,
    //           getFunctionPlacement_perFunc(
    //               inst, getFunctionsDescendingCores(inst)));

    //       SFC::Placed::ChainingPathThreadedBFFixed cssFixed1(inst1);
    //       SFC::Placed::ChainingPathThreadedBFFixed cssFixed2(inst2);
    //       SFC::Placed::ChainingPathThreadedBFFixed cssFixed3(inst3);

    //       std::array<bool, 3> foundSolution = {{cssFixed1.solveInteger(),
    //           cssFixed2.solveInteger(),
    //           cssFixed3.solveInteger()}};
    //       std::cout << "cssSolution: " << cssFixed1.getObjValue() << '\t' << cssFixed2.getObjValue()
    //                 << '\t' << cssFixed3.getObjValue() << '\n';
    //       if (std::any_of(foundSolution.begin(), foundSolution.end(),
    //               [](auto f) { return f; })) {
    //           ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> css(inst);
    //           auto* minSol =
    //               std::min({&cssFixed1, &cssFixed2, &cssFixed3},
    //                   [](const SFC::Placed::ChainingPathThreadedBFFixed* _c1,
    //                       const SFC::Placed::ChainingPathThreadedBFFixed* _c2) {
    //                       if (_c1->getObjValue() == -1) { // _c1 has no solution
    //                           return false;
    //                       }
    //                       if (_c2->getObjValue() != -1) {
    //                           return _c1->getObjValue() < _c2->getObjValue();
    //                       }
    //                       return true;
    //                   });
    //           std::cout << "Obj value of the smallest heur: " << minSol->getObjValue()
    //                     << '\n';
    //           if (minSol->getObjValue() != -1) {
    //               css.addInitConf(minSol->getServicePaths());
    //               css.solve();
    //               css.getSolution().save(filename, timer.get());
    //           }
    //       }
    //   } else if (params.model == "pathPred") {
    //       ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> css(inst);
    //       // Gets service path from higher replication limit solutions
    //       for (int i = NFVNodes.size(); i >= params.replicationLimit; --i) {
    //           std::cout << "Solving for " << i << '\n';
    //           css.setNbLicenses(i);
    //           css.solve();
    //       }
    //       css.getSolution().save(filename, timer.get());
    //   } else if (params.model == "path_loc") {
    //       ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD>DoublePP css(inst);
    //       css.solve();
    //       css.getSolution().save(filename, timer.get());
    //   } else if (params.model == "heur") {
    //   /*
    //   First solve a location problem for the functions based on the capacities
    //   of the nodes. Then solve the routing on the network with the previously
    //   found locations
    //   */
    //   SFC::OccLim::Localisation loc(inst);
    //   loc.solve();
    //   ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> css(inst);
    //   css.setBBounds(loc.getLocalisation());
    //   if (css.solveInteger()) {
    //       ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> cssBis(inst);
    //       cssBis.addInitConf(css.getServicePaths());
    //       css.solve();
    //       cssBis.save(filename, timer.get());
    //   } else {
    //       std::cout << "No solution found by the heuristic\n";
    //   }
    //   } else if (params.model == "heur_pf_asc") {
    //       /*
    // First solve a location problem each function based on the capacities of
    // the nodes. Then solve the routing on the network with the previously found
    // locations
    // */
    //       SFC::Placed::Instance instPlaced(
    //           inst,
    //           getFunctionPlacement_perFunc(inst, getFunctionsAscendingCores(inst)));
    //       SFC::Placed::ChainingPathThreadedBFFixed css(instPlaced);
    //       if (css.solveInteger()) {
    //           css.getSolution().save(filename, timer.get());
    //       } else {
    //           std::cout << "No solution found by the heuristic\n";
    //           exit(-1);
    //       }
    //   } else if (params.model == "heur_pf_desc") {
    //       /*
    // First solve a location problem each function based on the capacities of
    // the nodes. Then solve the routing on the network with the previously found
    // locations
    // */
    //       SFC::Placed::Instance instPlaced(
    //           inst, getFunctionPlacement_perFunc(
    //                     inst, getFunctionsDescendingCores(inst)));
    //       SFC::Placed::ChainingPathThreadedBFFixed css(instPlaced);
    //       if (css.solveInteger()) {
    //           css.getSolution().save(filename, timer.get());
    //       } else {
    //           std::cout << "No solution found by the heuristic\n";
    //           exit(-1);
    //       }
    //   } else if (params.model == "heur_pf_med_asc") {
    //       SFC::Placed::Instance instPlaced(
    //           inst,
    //           getFunctionPlacement_medoids(inst, getFunctionsAscendingCores(inst)));
    //       SFC::Placed::ChainingPathThreadedBFFixed css(instPlaced);
    //       if (css.solveInteger()) {
    //           css.getSolution().save(filename, timer.get());
    //       } else {
    //           std::cout << "No solution found by the heuristic\n";
    //           exit(-1);
    //       }
    //   } else if (params.model == "heur_pf") {
    //       std::vector<int> functions(funcCharge.size());
    //       std::iota(functions.begin(), functions.end(), 0);
    //       SFC::Placed::Instance instPlaced(
    //           inst, getFunctionPlacement_perFunc(inst, functions));
    //       SFC::Placed::ChainingPathThreadedBFFixed css(instPlaced);
    //       if (css.solveInteger()) {
    //           css.getSolution().save(filename, timer.get());
    //       } else {
    //           std::cout << "No solution found by the heuristic\n";
    //           exit(-1);
    //       }
    //   } else if (params.model == "heur_pf_med_desc") {

    //       SFC::Placed::Instance instPlaced(
    //           inst, getFunctionPlacement_medoids(
    //                     inst, getFunctionsDescendingCores(inst)));
    //       SFC::Placed::ChainingPathThreadedBFFixed css(instPlaced);

    //       if (css.solveInteger()) {
    //           css.getSolution().save(filename, timer.get());
    //       } else {
    //           std::cout << "No solution found by the heuristic\n";
    //           exit(-1);
    //       }
    //   } else if (params.model == "BB") {
    //       ColumnGenerationModel<SFC::OccLim::Instance, SFC::OccLim::Placement_DWD> css(inst);
    //       BranchAndBound bb(css);
    //       bb.solve();
    //       css.getSolution().save(filename, timer.get());
    //   } else if (params.model == "ILP") {
    //       SFC::Placed::Instance inst1(
    //           inst,
    //           getFunctionPlacement_perFunc(inst, getFunctionsAscendingCores(inst)));
    //       SFC::Placed::ChainingPathThreadedBFFixed css(inst1);

    //       SFC::OccLim::SFCOccIlp ilp(inst);
    //       if (css.solveInteger()) {
    //           ilp.addStart(css.getServicePaths());
    //       }
    //       IloAlgorithm::Status status = IloAlgorithm::Unknown;
    //       do {
    //           status = ilp.stepSolve();
    //           if (status != IloAlgorithm::Infeasible) {
    //               css.getSolution().save(filename, timer.get());
    //           }
    //       } while (status != IloAlgorithm::Optimal);
    //   } else if (params.model == "LP") {
    //       SFC::OccLim::SFCOccIlp ilp(inst);
    //       ilp.solveRelaxation();
    //   }
}

std::vector<int>
getFunctionsAscendingCores(const SFC::OccLim::Instance& inst) {
    // Get functions ascending order of used cores
    std::vector<int> totalCores(inst.funcCharge.size());
    for (int i = 0; i < inst.demands.size(); ++i) {
        for (int j = 0; j < inst.demands[i].functions.size(); ++j) {
            totalCores[inst.demands[i].functions[j]] += inst.nbCores(i, j);
        }
    }
    std::vector<int> functions(inst.funcCharge.size());
    std::iota(functions.begin(), functions.end(), 0);
    std::sort(functions.begin(), functions.end(), [&](const SFC::function_descriptor _f1, const SFC::function_descriptor _f2) {
        return totalCores[_f1] < totalCores[_f2];
    });
    std::cout << totalCores << '\n'
              << functions << '\n';
    return functions;
}

std::vector<int>
getFunctionsDescendingCores(const SFC::OccLim::Instance& inst) {
    // Get functions ascending order of used cores
    std::vector<int> totalCores(inst.funcCharge.size());
    for (int i = 0; i < inst.demands.size(); ++i) {
        for (int j = 0; j < inst.demands[i].functions.size(); ++j) {
            totalCores[inst.demands[i].functions[j]] += inst.nbCores(i, j);
        }
    }
    std::vector<int> functions(inst.funcCharge.size());
    std::iota(functions.begin(), functions.end(), 0);
    std::sort(functions.begin(), functions.end(),
        [&](const int _f1, const int _f2) {
            return totalCores[_f1] > totalCores[_f2];
        });
    std::cout << totalCores << '\n'
              << functions << '\n';
    return functions;
}

Matrix<char> getFunctionPlacement_perFunc(
    SFC::OccLim::Instance inst,
    const std::vector<SFC::function_descriptor>& _funcs) {
    std::cout << "Solving for ";
    Matrix<char> funcPlacement(inst.network.getOrder(), inst.funcCharge.size(),
        false);
    SFC::OccLim::LocalisationPerFunction loc(inst);
    for (const int f : _funcs) {
        // std::cout << "Changing model...\n";
        loc.setFunction(f);
        loc.updateCapacities();
        std::cout << f << '\t' << std::flush;
        if (loc.solve()) {
            for (const auto& u : inst.NFVNodes) {
                inst.nodeCapa[u] -= loc.getUsedCapacities(u);
            }
            IloNumArray localisations = loc.getLocalisation();
            for (const auto& u : inst.NFVNodes) {
                funcPlacement(u, f) = static_cast<bool>(localisations[u]);
            }
        } else {
            std::cout << "No solution found by the heuristic (No location found for "
                      << f << ")\n";
            exit(-1);
        }
    }
    return funcPlacement;
}

Matrix<char> getFunctionPlacement_medoids(
    const SFC::OccLim::Instance& _inst,
    const std::vector<SFC::function_descriptor>& _funcs) {
    Matrix<char> funcPlacement(_inst.network.getOrder(), _inst.funcCharge.size(),
        false);
    DiGraph<double> graph = _inst.network;
    for (auto& edge : graph.getEdges()) {
        graph.setEdgeWeight(edge, 1.0);
    }
    AllShortestPathBellmanFord<DiGraph<double>> allSh(graph);
    allSh.getAllShortestPaths();
    auto currentCapas = _inst.nodeCapa;
    for (const auto& f : _funcs) {
        // The charge of each node correspond to the sum of the demands' charge from
        // and to this node (divided by 2)
        double totalCharge = 0.0;
        std::vector<double> charge(_inst.network.getOrder(), 0.0);
        for (int i = 0; i < _inst.demands.size(); ++i) {
            for (int j = 0; j < _inst.demands[i].functions.size(); ++j) {
                if (_inst.demands[i].functions[j] == f) {
                    totalCharge += _inst.nbCores(i, j);
                    const double halfCharge = _inst.nbCores(i, j) / 2.0;
                    charge[_inst.demands[i].s] += halfCharge;
                    charge[_inst.demands[i].t] += halfCharge;
                }
            }
        }
        const auto medoids = getKMedoids(_inst.nbLicenses, allSh.getDistance(),
            currentCapas, charge);
        for (const int u : medoids) {
            funcPlacement(u, f) = true;
            currentCapas[u] -= totalCharge / double(_inst.nbLicenses);
        }
    }
    return funcPlacement;
}
