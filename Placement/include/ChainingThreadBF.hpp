#ifndef CHAINING_PATH_THREAD_BF_HPP
#define CHAINING_PATH_THREAD_BF_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <limits>
#include <map>
#include <optional>
#include <thread>
#include <tuple>

#include "ColumnGeneration.hpp"
#include "DiGraph.hpp"
#include "SFC.hpp"
#include "ShortestPath.hpp"
#include "ShortestPathBF.hpp"
#include "ThreadPool.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

namespace SFC {

class PlacementDWD_Path {
  public:
    using Column = ServicePath;
    using Solution = Solution<int>;

    explicit PlacementDWD_Path(const Instance* _inst);
    PlacementDWD_Path(const PlacementDWD_Path&) = default;
    PlacementDWD_Path(PlacementDWD_Path&&) = default;
    PlacementDWD_Path& operator=(const PlacementDWD_Path&) = default;
    PlacementDWD_Path& operator=(PlacementDWD_Path&&) = default;
    ~PlacementDWD_Path() { m_env.end(); }

    void addColumns(const std::vector<Column>& _sPaths);
    void getDuals();
    void getNetworkUsage(const IloCplex& _sol) const;
    double getObjValue() const;
    bool solveInteger();
    bool checkReducedCosts() const;
    
    template<bool CLEAR = true>
    bool solve() {
        if constexpr(CLEAR) {
            m_solver.extract(m_model);
        }
        
        if (m_solver.solve() == IloFalse) {
            m_solver.exportModel("rm.lp");
            return false;
        }
        m_fractObj = m_solver.getObjValue();
        getDuals();
        assert(checkReducedCosts());
        removeDummyIfPossible();
        if constexpr(CLEAR) {
            m_solver.clear();
        }
        return true;
    }

    double getDualSumRHS() const;
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }

    inline int getNbColumns() const {
        return m_paths.size();
    }

    inline IloNum getNodeCapacityDual(Graph::Node _u) const {
        return m_nodeCapasDuals[m_inst->NFVIndices[_u]];
    }

    inline IloNum getLinkCapacityDual(int _e) const {
        return m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual(int _demandID) const {
        return m_onePathDuals[_demandID];
    }

    double getRelaxationObjValue() const;
    double getIntegerObjValue() const;
    std::vector<ServicePath> getServicePaths() const;
    Solution getSolution() const;

  private:
    void removeDummyIfPossible();

    const Instance* m_inst;

    IloEnv m_env = IloEnv();
    IloModel m_model = IloModel(m_env);
    IloNumVarArray m_y;

    IloObjective m_obj;

    IloRangeArray m_onePathCons;
    IloRangeArray m_linkCapasCons;
    IloRangeArray m_nodeCapasCons;

    IloNumVarArray m_dummyPaths;
    bool m_dummyActive = true;

    IloNumArray m_onePathDuals = IloNumArray(m_env);
    IloNumArray m_linkCapasDuals = IloNumArray(m_env);
    IloNumArray m_nodeCapasDuals = IloNumArray(m_env);

    IloNumArray m_linkCharge = IloNumArray(m_env, m_inst->network.size());
    IloNumArray m_nodeCharge = IloNumArray(m_env, m_inst->network.getOrder());

    IloCplex m_solver;

    std::vector<ServicePath> m_paths;
    std::vector<int> m_MIPStart = std::vector<int>(m_inst->demands.size());

    double m_intObj = std::numeric_limits<double>::lowest();
    double m_fractObj = std::numeric_limits<double>::lowest();
};

class PricingProblemBF {
  public:
    using Column = ServicePath;

    explicit PricingProblemBF(const Instance* _inst);
    PricingProblemBF(const PricingProblemBF&) = default;
    PricingProblemBF(PricingProblemBF&&) = default;
    PricingProblemBF& operator=(const PricingProblemBF&) = default;
    PricingProblemBF& operator=(PricingProblemBF&&) = default;
    ~PricingProblemBF() = default;

    void setPricingID(int _demandID);
    void updateDual(const PlacementDWD_Path& _rmp);
    bool solve();
    double getObjValue() const;
    Column getColumn() const;

  private:
    const Instance* m_inst;
    int n;
    int m;
    int nbLayers;
    int m_demandID = 0;
    DiGraph m_layeredGraph;
    bool isNegative = false;
    ShortestPathBellmanFord<DiGraph> m_bf = ShortestPathBellmanFord<DiGraph>(m_layeredGraph);
    ShortestPath<DiGraph> m_dijkstra = ShortestPath<DiGraph>(m_layeredGraph);
    Graph::Path m_path = Graph::Path();
    double m_objValue = std::numeric_limits<double>::lowest();
};

class PricingProblemLP {
  public:
    explicit PricingProblemLP(const Instance* _inst);
    PricingProblemLP(const PricingProblemLP&) = default;
    PricingProblemLP(PricingProblemLP&&) = default;
    PricingProblemLP& operator=(const PricingProblemLP&) = default;
    PricingProblemLP& operator=(PricingProblemLP&&) = default;
    ~PricingProblemLP() { m_env.end(); }

    void setPricingID(int _demandID);
    void updateDual(const PlacementDWD_Path& _rmp);
    ServicePath getColumn() const;
    bool solve();
    double getObjValue() const;

  private:
    int getIndexF(const int _e, const int _j) const;
    int getIndexA(const Graph::Node _u, const int _j) const;

    const Instance* m_inst;

    int m_demandID = 0;
    const int n;
    const int m;
    const int nbLayers;

    IloEnv m_env = IloEnv();
    IloModel m_model = IloModel(m_env);

    IloObjective m_obj;

    IloNumVarArray m_a;
    IloNumVarArray m_f;

    IloRangeArray m_flowConsCons;
    IloRangeArray m_linkCapaCons;
    IloRangeArray m_nodeCapaCons;

    IloCplex m_solver;

    using Column = ServicePath;
};

} // namespace SFC
#endif
