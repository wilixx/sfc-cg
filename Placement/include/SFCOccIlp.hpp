#ifndef SFC_OCC_ILP_HPP
#define SFC_OCC_ILP_HPP

#include "AllShortestPathBF.hpp"
#include <cplex_utility.hpp>
#include <ilcplex/ilocplex.h>
#include <utility.hpp>

#include "SFC.hpp"

namespace SFC::OccLim {
class SFCOccIlp {
  public:
    SFCOccIlp(const Instance& _inst)
        : m_inst(&_inst)
        , m_f([&]() {
            std::cout << "Adding m_f\n";
            IloNumVarArray f(m_env, m_inst->network.size() * (m_inst->maxChainSize + 1) * m_inst->demands.size(), 0.0, IloInfinity);
            return IloAdd(m_model, f);
        }())
        , m_a([&]() {
            std::cout << "Adding m_a\n";
            IloNumVarArray a(m_env, m_inst->network.getOrder() * m_inst->maxChainSize * m_inst->demands.size(), 0.0, IloInfinity);
            return IloAdd(m_model, a);
        }())
        , m_b([&]() {
            std::cout << "Adding m_b\n";
            IloNumVarArray b(m_env, m_inst->network.getOrder() * m_inst->funcCharge.size(), 0.0, IloInfinity);
            return IloAdd(m_model, b);
        }())
        , m_obj([&]() {
            std::cout << "Adding m_obj\n";
            IloNumVarArray vars(m_env);
            IloNumArray vals(m_env);
            for (int e = 0; e < m_inst->network.size(); ++e) {
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size(); ++j) {
                        vars.add(m_f[getIndexF(e, i, j)]);
                        vals.add(m_inst->demands[i].d);
                    }
                }
            }
            auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
            vars.end();
            vals.end();
            return obj;
        }())
        , m_flowConservationConstraints([&]() {
            std::cout << "Adding m_flowConservationConstraints\n";
            IloRangeArray flowCons = IloRangeArray(m_env, m_inst->network.getOrder() * (m_inst->maxChainSize + 1) * m_inst->demands.size());
            for (Graph::Node u(0); u < m_inst->network.getOrder(); ++u) {
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size(); ++j) {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for (const auto& v : m_inst->network.getNeighbors(u)) {
                            // Outgoing flows
                            vars.add(m_f[getIndexF(m_inst->edgeToId(u, v), i, j)]);
                            vals.add(1.0);
                            // Incoming flows
                            vars.add(m_f[getIndexF(m_inst->edgeToId(v, u), i, j)]);
                            vals.add(-1.0);
                        }
                        if (m_inst->isNFV[u]) {
                            if (j < m_inst->demands[i].functions.size()) { // Going next layer
                                vars.add(m_a[getIndexA(u, i, j)]);
                                vals.add(1.0);
                            }
                            if (j > 0) { // Coming from last layer
                                vars.add(m_a[getIndexA(u, i, j - 1)]);
                                vals.add(-1.0);
                            }
                        }
                        flowCons[getIndexFC(u, i, j)] = IloAdd(m_model, IloScalProd(vars, vals) == 0);
                        vars.end();
                        vals.end();
                    }
                }
            }

            for (int i = 0; i < m_inst->demands.size(); ++i) {
                flowCons[getIndexFC(m_inst->demands[i].s, i, 0)].setBounds(1.0, 1.0);
                flowCons[getIndexFC(m_inst->demands[i].t, i, m_inst->demands[i].functions.size())].setBounds(-1.0, -1.0);
            }
            return flowCons;
        }())
        , m_linkCapacityConstraitns([&]() {
            std::cout << "Adding m_linkCapacityConstraitns\n";
            IloRangeArray linkCapaConstraints(m_env, m_inst->network.size(), 0.0, 0.0);
            for (int e = 0; e < m_inst->network.size(); ++e) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size(); ++j) {
                        vars.add(m_f[getIndexF(e, i, j)]);
                        vals.add(m_inst->demands[i].d);
                    }
                }
                linkCapaConstraints[e] = (IloScalProd(vars, vals) <= m_inst->network.getEdgeWeight(m_inst->edges[e]));
                vars.end();
                vals.end();
            }
            return IloAdd(m_model, linkCapaConstraints);
        }())
        , m_nodeCapacityConstraints([&]() {
            std::cout << "Adding m_nodeCapacityConstraints\n";
            IloRangeArray nodeCapaConstraints(m_env, m_inst->network.getOrder(), 0.0, 0.0);
            for (const auto& u : m_inst->NFVNodes) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (int i = 0; i < m_inst->demands.size(); ++i) {
                    for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                        vars.add(m_a[getIndexA(u, i, j)]);
                        vals.add(m_inst->nbCores(i, j));
                    }
                }
                nodeCapaConstraints[u] = IloAdd(m_model, IloScalProd(vars, vals) <= IloInt(m_inst->nodeCapa[u]));
                vars.end();
                vals.end();
            }
            return nodeCapaConstraints;
        }())
        , m_funcNodeUsageCons([&]() {
            std::cout << "Adding m_funcNodeUsageCons\n";
            IloRangeArray funcNodeUsageCons(m_env, m_inst->NFVNodes.size() * m_inst->maxChainSize * m_inst->demands.size());
                for (const auto& u : m_inst->NFVNodes) {
                    for (int i = 0; i < m_inst->demands.size(); ++i) {
                        for (int j = 0; j < m_inst->demands[i].functions.size(); ++j) {
                            funcNodeUsageCons[getIndexA(u, i, j)] = (m_b[getIndexB(u, m_inst->demands[i].functions[j])] - m_a[getIndexA(u, i, j)]) >= 0;
                        }
                    }
                }
            return IloAdd(m_model, funcNodeUsageCons);
        }())
        , m_nbLicensesCons([&]() {
            std::cout << "Adding m_nbLicensesCons\n";
            IloRangeArray nbLicensesCons(m_env, m_inst->funcCharge.size());
            IloNumVarArray vars(m_env, m_inst->NFVNodes.size());
            for (int f = 0; f < m_inst->funcCharge.size(); ++f) {
                int i = 0;
                for (const auto& u : m_inst->NFVNodes) {
                    vars[i] = m_b[getIndexB(u, f)];
                    ++i;
                }
                nbLicensesCons[f] = (IloSum(vars) <= m_inst->nbLicenses);
            }
            vars.end();
            return IloAdd(m_model, nbLicensesCons);
        }())
        , m_lowerBound([&] {
            double minBandwidth = 0.0;
            DiGraph hopNetwork(m_inst->network);
            for (const auto& edge : hopNetwork.getEdges()) {
                hopNetwork.setEdgeWeight(edge, 1.0);
            }
            AllShortestPathBellmanFord<DiGraph> shPath(hopNetwork);
            shPath.getAllShortestPaths();
            IloExpr expr(m_env);
            for (int i = 0; i < m_inst->demands.size(); ++i) {
                minBandwidth += shPath.getDistance()(m_inst->demands[i].s, m_inst->demands[i].t) * m_inst->demands[i].d;
                for (int e = 0; e < m_inst->network.size(); ++e) {
                    for (int j = 0; j <= m_inst->demands[i].functions.size(); ++j) {
                        expr += m_inst->demands[i].d * m_f[getIndexF(e, i, j)];
                    }
                }
            }
            auto range = IloRange(m_env, minBandwidth, IloNumExpr(expr), IloInfinity);
            expr.end();
            return IloAdd(m_model, range);
        }())
        , m_solver(m_model) {}

    bool solve() {
        m_model.add(IloConversion(m_env, m_a, ILOBOOL));
        m_model.add(IloConversion(m_env, m_b, ILOBOOL));
        m_model.add(IloConversion(m_env, m_f, ILOBOOL));

        for (IloInt i = 0; i < m_b.getSize(); ++i) {
            m_solver.setPriority(m_b[i], 10.0);
        }
        if (!m_solver.solve()) {
            std::cout << "No solution found!\n";
            m_solver.exportModel("ILP.lp");
            return false;
        }        
        m_intObj = m_solver.getObjValue();
        m_fractObj = m_solver.getBestObjValue();
        std::cout << "Solution found :" << m_intObj << '\n';
        return true;
    }

    IloAlgorithm::Status stepSolve(const double _seconds = 3600) {
        m_solver.setParam(IloCplex::TiLim, _seconds);
        if (!m_solver.solve()) {
            std::cout << "No solution found!\n";
            m_solver.exportModel("ILP.lp");
        }
        return m_solver.getStatus();
    }

    void addMIPStart(const std::vector<ServicePath>& _sPaths) {
        IloNumArray fVal(m_env, m_f.getSize());
        for (IloInt i = 0; i < fVal.getSize(); ++i) {
            fVal[i] = 0.0;
        }
        IloNumArray aVal(m_env, m_a.getSize());
        for (IloInt i = 0; i < aVal.getSize(); ++i) {
            aVal[i] = 0.0;
        }
        IloNumArray bVal(m_env, m_b.getSize());
        for (IloInt i = 0; i < bVal.getSize(); ++i) {
            bVal[i] = 0.0;
        }

        for (const auto & [ nPath, locations, demandID ] : _sPaths) {
            // for (int i = 0; i < _sPaths.size(); ++i) {
            assert(m_inst->demands[demandID].s == nPath.front());
            assert(m_inst->demands[demandID].t == nPath.back());
            // std::cout << i << ": " << _sPaths[i] << '\n';

            int j = 0;
            for (auto iteU = nPath.begin(), iteV = std::next(iteU);
                 iteV != nPath.end();
                 ++iteU, ++iteV) {
                // If the node contains functions
                if (j < locations.size() && locations[j] == *iteU) {
                    aVal[getIndexA(*iteU, demandID, j)] = 1.0;
                    bVal[getIndexB(*iteU, m_inst->demands[demandID].functions[j])] = 1.0;
                    ++j;
                }
                fVal[getIndexF(m_inst->edgeToId(*iteU, *iteV), demandID, j)] = 1.0;
            }
            // If the rest of the function are on t
            for (; j < locations.size(); ++j) {
                aVal[getIndexA(m_inst->demands[demandID].t, demandID, j)] = 1.0;
                bVal[getIndexB(m_inst->demands[demandID].t, m_inst->demands[demandID].functions[j])] = 1.0;
            }
        }

        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        vars.add(m_f);
        vals.add(fVal);

        vars.add(m_a);
        vals.add(aVal);

        vars.add(m_b);
        vals.add(bVal);

        m_solver.addMIPStart(vars, vals);
        m_solver.setWarning(m_env.getNullStream());
        vars.end();
        vals.end();

        IloConstraintArray consArr(m_env);
        IloNumArray numArr(m_env);
        for (IloModel::Iterator iter(m_model); iter.ok(); ++iter) {
            if ((*iter).asConstraint().getImpl()) {
                consArr.add((*iter).asConstraint());
                numArr.add(1);
            }
        }
        for (IloModel::Iterator iter(m_model); iter.ok(); ++iter) {
            if ((*iter).asVariable().getImpl()) {
                consArr.add(IloBound((*iter).asVariable(), IloBound::Lower));
                numArr.add(1);
                consArr.add(IloBound((*iter).asVariable(), IloBound::Upper));
                numArr.add(1);
            }
        }
        if (m_solver.refineMIPStartConflict(0, consArr, numArr)) {
            const auto conflict = m_solver.getConflict(consArr);
            m_env.getImpl()->useDetailedDisplay(IloTrue);
            for (IloInt i = 0; i < consArr.getSize(); ++i) {
                if (conflict[i] == IloCplex::ConflictMember) {
                    std::cout << "Proved  : " << consArr[i] << '\n';
                } else if (conflict[i] == IloCplex::ConflictPossibleMember) {
                    std::cout << "Possible: " << consArr[i] << '\n';
                }
            }
            exit(-8);
        }
    }

    std::vector<ServicePath> getServicePaths() const {
        IloNumArray aVal(m_env);
        m_solver.getValues(m_a, aVal);
        IloNumArray fVal(m_env);
        m_solver.getValues(m_f, fVal);

        std::vector<ServicePath> pathsUsed(m_inst->demands.size());
        for (int demandID = 0; demandID < m_inst->demands.size(); ++demandID) {
            Graph::Node u = m_inst->demands[demandID].s;
            int j = 0;
            pathsUsed[demandID].nPath.push_back(u);
            while (u != m_inst->demands[demandID].t || j < m_inst->demands[demandID].functions.size()) {
                if (j < m_inst->demands[demandID].functions.size() && m_inst->isNFV[u] 
                    && epsilon_equal<double>()(aVal[getIndexA(u, demandID, j)], IloTrue)) {
                    pathsUsed[demandID].locations.push_back(u);
                    // nodeUsage[u] += m_inst->nbCores(demandID, j);
                    ++j;
                } else {
                    for (const auto& v : m_inst->network.getNeighbors(u)) {
                        if (epsilon_equal<double>()(fVal[getIndexF(m_inst->edgeToId(u, v), demandID, j)], 1.0)) {
                            // linkUsage[m_inst->edgeToId(u, v)] += m_inst->demands[demandID].d;
                            pathsUsed[demandID].nPath.push_back(v);
                            u = v;
                            break;
                        }
                    }
                }
            }
        }

        return pathsUsed;
    }


    Solution<int> getSolution() const {
        return {*m_inst, m_solver.getObjValue(), m_solver.getBestObjValue(),
            getServicePaths(), 0};
    }

    bool solveRelaxation() {
        return m_solver.solve() == IloTrue;
    }

  private:
    const Instance* m_inst;
    // Results
    double m_intObj = -1;
    double m_fractObj = 1;
    // ILP
    IloEnv m_env = IloEnv();
    IloModel m_model = IloModel(m_env);

    IloNumVarArray m_f;
    IloNumVarArray m_a;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_flowConservationConstraints;
    IloRangeArray m_linkCapacityConstraitns;
    IloRangeArray m_nodeCapacityConstraints;
    IloRangeArray m_funcNodeUsageCons;
    IloRangeArray m_nbLicensesCons;
    IloRange m_lowerBound;

    IloCplex m_solver;

    int getIndexA(const Graph::Node _u, const int _i, const int _j) const {
        assert(m_inst->isNFV[_u]);
        assert(_i < m_inst->demands.size());
        assert(_j < m_inst->demands[_i].functions.size());
        return _j + _i * m_inst->maxChainSize + _u * m_inst->demands.size() * m_inst->maxChainSize;
    }

    int getIndexB(const Graph::Node _u, const int _f) const {
        assert(m_inst->isNFV[_u]);
        return m_inst->network.getOrder() * _f + _u;
    }

    int getIndexFC(const Graph::Node _u, const int _i, const int _j) const {
        assert(_u < m_inst->network.getOrder());
        assert(_i < m_inst->demands.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * (m_inst->maxChainSize + 1) + _u * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }

    int getIndexF(const int _e, const int _i, const int _j) const {
        assert(_e < m_inst->network.size());
        assert(_i < m_inst->demands.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * (m_inst->maxChainSize + 1) + _e * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }
};
} // namespace SFC::OccLim

#endif
