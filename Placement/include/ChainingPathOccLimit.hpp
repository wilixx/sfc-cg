#ifndef CHAINING_PATH_FUNCOCC_HPP
#define CHAINING_PATH_FUNCOCC_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <thread>
#include <tuple>

#include <DiGraph.hpp>
#include <ShortestPath.hpp>
#include <ShortestPathBF.hpp>
#include <ThreadPool.hpp>
#include <cplex_utility.hpp>
#include <utility.hpp>
#include <boost/multi_array.hpp>

#include "SFC.hpp"

#define RC_EPS 1.0e-6

namespace SFC::OccLim {
class Placement_DWD {
  public:
    using Column = ServicePath;
    using Solution = Solution<int>;

    struct DualValues {
        DualValues(const IloEnv& _env, IloInt _opSize, IloInt _lcSize, IloInt _ncSize, IloInt _pfSize, IloInt _nlSize)
            : m_onePathDuals([&]() {
                IloNumArray retval(_env, _opSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_linkCapasDuals([&]() {
                IloNumArray retval(_env, _lcSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_nodeCapasDuals([&]() {
                IloNumArray retval(_env, _ncSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_pathFuncDuals([&]() {
                IloNumArray retval(_env, _pfSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_nbLicensesDuals([&]() {
                IloNumArray retval(_env, _nlSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }()) {}

        IloNumArray m_onePathDuals;
        IloNumArray m_linkCapasDuals;
        IloNumArray m_nodeCapasDuals;
        IloNumArray m_pathFuncDuals;
        IloNumArray m_nbLicensesDuals;
    };

    explicit Placement_DWD(const Instance* _inst);
    Placement_DWD& operator=(const Placement_DWD&) = delete;
    Placement_DWD& operator=(Placement_DWD&&) = delete;
    Placement_DWD(const Placement_DWD&) = delete;
    Placement_DWD(Placement_DWD&&) = delete;

    ~Placement_DWD() { m_env.end(); }

    double getObjValue() const;
    void addColumns(const std::vector<Column>& _sPaths);
    void getDuals();
    void getNetworkUsage(const IloCplex& _sol) const;
    bool solveInteger();
    bool checkReducedCosts() const;
    bool solve();
    double getDualSumRHS(const DualValues& _dualValues) const;
    double getAdditionalVariable(const DualValues& _dualValues) const;
    void setAlpha(double _alpha) {
        m_alpha = _alpha;
    }
    inline int getNbPricing() const {
        return m_inst->demands.size();
    }

    inline int getNbColumns() const {
        return m_paths.size();
    }

    inline IloNum getNodeCapacityDual(Graph::Node _u) const {
        return normalDuals.m_nodeCapasDuals[m_inst->NFVIndices[_u]];
    }

    inline IloNum getLinkCapacityDual(int _e) const {
        return normalDuals.m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual(int _demandID) const {
        return normalDuals.m_onePathDuals[_demandID];
    }

    inline IloNum getPathFunctionDual(const int _demandID, int _f, int _u) const {
        return normalDuals.m_pathFuncDuals[getIndexPathFunc(_demandID, _f, _u)];
    }

    inline IloNum getNodeCapacityDual_stab(Graph::Node _u) const {
        return stabDuals.m_nodeCapasDuals[m_inst->NFVIndices[_u]];
    }

    inline IloNum getLinkCapacityDual_stab(int _e) const {
        return stabDuals.m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual_stab(int _demandID) const {
        return stabDuals.m_onePathDuals[_demandID];
    }

    inline IloNum getPathFunctionDual_stab(const int _demandID, int _f, int _u) const {
        return stabDuals.m_pathFuncDuals[getIndexPathFunc(_demandID, _f, _u)];
    }

    inline double getRelaxationObjValue() const {
        return m_intObj;
    }
    inline double getIntegerObjValue() const {
        return m_fractObj;
    }

    std::vector<ServicePath> getServicePaths() const;
    Solution getSolution() const;

    double getStabilizedLowerBound(const std::vector<Column>& _columns) const;
    double getBestLowerBound(double _bestLowerBound, const std::vector<Column>& _columns);
    double getReducedCost(const ServicePath& _sPath) const;

  private:
    int getIndexB(Graph::Node _u, function_descriptor _f) const;
    int getIndexPathFunc(int _i, function_descriptor _f, Graph::Node _u) const;

    double getReducedCost(const ServicePath& _sPath, const DualValues& _dualValues) const;
    double getLowerBound(const std::vector<Column>& _columns, const DualValues& _dualValues) const;
    double getLowerBound(const DualValues& _dualValues) const;

    const Instance* m_inst;
    IloEnv m_env = IloEnv();
    IloModel m_model = IloModel(m_env);
    IloModel m_integerConversions = IloModel(m_env);

    IloNumVarArray m_y;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_onePathCons;
    IloRangeArray m_linkCapasCons;
    IloRangeArray m_nodeCapasCons;
    IloRangeArray m_nbLicensesCons;
    IloRangeArray m_pathFuncCons;

    IloNumVarArray m_dummyPaths = IloNumVarArray(m_env);

    DualValues normalDuals = DualValues(m_env, m_onePathCons.getSize(), m_linkCapasCons.getSize(),
        m_nodeCapasCons.getSize(), m_pathFuncCons.getSize(), m_nbLicensesCons.getSize());

    double m_alpha = 0.1;

    DualValues bestDuals = DualValues(m_env, m_onePathCons.getSize(), m_linkCapasCons.getSize(),
        m_nodeCapasCons.getSize(), m_pathFuncCons.getSize(), m_nbLicensesCons.getSize());
    DualValues stabDuals = DualValues(m_env, m_onePathCons.getSize(), m_linkCapasCons.getSize(),
        m_nodeCapasCons.getSize(), m_pathFuncCons.getSize(), m_nbLicensesCons.getSize());

    IloNumArray m_linkCharge;
    IloNumArray m_nodeCharge;
    IloNumArray m_funcPath;
    IloRangeArray m_pathFuncCons_Utility;

    IloCplex m_solver;

    std::vector<ServicePath> m_paths{};

    double m_intObj = -1;
    double m_fractObj = -1;
};

class Placement_EM {
  public:
    using Column = ServicePath;
    using Solution = Solution<int>;

    struct DualValues {
        DualValues(const IloEnv& _env, int _opSize, int _lcSize, int _ncSize,
            int _pfSize, int _nlSize)
            : m_onePathDuals([&]() {
                IloNumArray retval(_env, _opSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_linkCapasDuals([&]() {
                IloNumArray retval(_env, _lcSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_nodeCapasDuals([&]() {
                IloNumArray retval(_env, _ncSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_pathFuncDuals([&]() {
                IloNumArray retval(_env, _pfSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }())
            , m_nbLicensesDuals([&]() {
                IloNumArray retval(_env, _nlSize);
                std::fill(begin(retval), end(retval), 0.0);
                return retval;
            }()) {}

        IloNumArray m_onePathDuals;
        IloNumArray m_linkCapasDuals;
        IloNumArray m_nodeCapasDuals;
        IloNumArray m_pathFuncDuals;
        IloNumArray m_nbLicensesDuals;
    };
    inline IloNum getNodeCapacityDual(Graph::Node _u) const {
        return normalDuals.m_nodeCapasDuals[m_inst->NFVIndices[_u]];
    }

    inline IloNum getLinkCapacityDual(int _e) const {
        return normalDuals.m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual(int _demandID) const {
        return normalDuals.m_onePathDuals[_demandID];
    }

    inline IloNum getPathFunctionDual(const int _demandID, int _j, int _u) const {
        return normalDuals.m_pathFuncDuals[getIndexPathFunc(_demandID, _j, _u)];
    }

    inline IloNum getNodeCapacityDual_stab(Graph::Node _u) const {
        return stabDuals.m_nodeCapasDuals[m_inst->NFVIndices[_u]];
    }

    inline IloNum getLinkCapacityDual_stab(int _e) const {
        return stabDuals.m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual_stab(int _demandID) const {
        return stabDuals.m_onePathDuals[_demandID];
    }

    inline IloNum getPathFunctionDual_stab(const int _demandID, int _j, int _u) const {
        return stabDuals.m_pathFuncDuals[getIndexPathFunc(_demandID, _j, _u)];
    }

    explicit Placement_EM(const Instance* _inst);
    Placement_EM& operator=(const Placement_EM&) = delete;
    Placement_EM& operator=(Placement_EM&&) = delete;
    Placement_EM(const Placement_EM&) = delete;
    Placement_EM(Placement_EM&&) = delete;

    ~Placement_EM() { m_env.end(); }

    double getObjValue() const;
    void addColumns(const std::vector<Column>& _sPaths);
    void addColumn(int _i);
    void getDuals();
    void getNetworkUsage(const IloCplex& _sol) const;
    bool solveInteger();
    bool checkReducedCosts() const;
    bool solve();
    double getDualSumRHS(const DualValues& _dualValues) const;
    double getAdditionalVariable(const DualValues& _dualValues) const;

    inline int getNbPricing() const {
        return m_inst->demands.size();
    }

    inline int getNbColumns() const {
        return m_paths.size();
    }

    inline double getRelaxationObjValue() const {
        return m_intObj;
    }
    inline double getIntegerObjValue() const {
        return m_fractObj;
    }

    std::vector<ServicePath> getServicePaths() const;
    Solution getSolution() const;

    double getBestLowerBound(double _bestLowerBound, const std::vector<Column>& _columns);
    double getReducedCost(const ServicePath& _sPath) const;
    double getBaseLagragian(const ServicePath& _sPath, const DualValues& _dualValues) const;

  private:
    inline int getIndexA(const Graph::Node _u, const int _i, const int _j) {
        assert(m_inst->isNFV[_u]);
        assert(_i < m_inst->demands.size());
        assert(_j < m_inst->demands[_i].functions.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * m_inst->maxChainSize + _u * m_inst->demands.size() * m_inst->maxChainSize;
    }

    inline int getIndexB(const Graph::Node _u, const int _f) const {
        assert(m_inst->isNFV[_u]);
        return m_inst->network.getOrder() * _f + _u;
    }

    inline int getIndexFC(const Graph::Node _u, const int _i) const {
        assert(_u < m_inst->network.getOrder());
        assert(_i < m_inst->demands.size());
        return _i * (m_inst->maxChainSize + 1) + _u * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }

    inline int getIndexF(const int _e, const int _i) const {
        assert(_e < m_inst->network.size());
        assert(_i < m_inst->demands.size());
        return _i + _e * m_inst->demands.size();
    }

    inline int getIndexPathFunc(
        const int _i, const int _j,
        const Graph::Node _u) const {
        assert(_i < m_inst->demands.size());
        assert(_j < m_inst->demands[_i].functions.size());
        assert(_u < m_inst->network.getOrder());
        return m_inst->NFVNodes.size() * m_inst->maxChainSize * _i + m_inst->NFVNodes.size() * _j + m_inst->NFVIndices[_u];
    }

    double getReducedCost(const ServicePath& _sPath, const DualValues& _dualValues) const;
    double getLowerBound(double _reducedCost, const DualValues& _dualValues) const;

    const Instance* m_inst;
    IloEnv m_env = IloEnv();
    IloModel m_model = IloModel(m_env);
    IloModel m_integerConversions = IloModel(m_env);

    IloNumVarArray m_f;
    IloNumVarArray m_a;
    IloNumVarArray m_y;
    IloNumVarArray m_b;

    IloObjective m_obj;

    IloRangeArray m_onePathCons;
    IloRangeArray m_linkCapasCons;
    IloRangeArray m_nodeCapasCons;
    IloRangeArray m_nbLicensesCons;
    IloRangeArray m_pathFuncCons;

    IloRangeArray m_relation_f;
    IloRangeArray m_relation_a;

    IloNumVarArray m_dummyPaths = IloNumVarArray(m_env);

    DualValues normalDuals = DualValues(m_env, m_onePathCons.getSize(), m_linkCapasCons.getSize(),
        m_nodeCapasCons.getSize(), m_pathFuncCons.getSize(), m_nbLicensesCons.getSize());

    double m_alpha = 0.1;

    DualValues bestDuals = DualValues(m_env, m_onePathCons.getSize(), m_linkCapasCons.getSize(),
        m_nodeCapasCons.getSize(), m_pathFuncCons.getSize(), m_nbLicensesCons.getSize());
    DualValues stabDuals = DualValues(m_env, m_onePathCons.getSize(), m_linkCapasCons.getSize(),
        m_nodeCapasCons.getSize(), m_pathFuncCons.getSize(), m_nbLicensesCons.getSize());

    IloNumArray relation_a;
    IloNumArray relation_f;
    IloNumArray m_funcPath;
    IloRangeArray m_pathFuncCons_Utility;

    IloCplex m_solver;

    std::vector<ServicePath> m_paths{};

    double m_intObj = -1;
    double m_fractObj = -1;
};

class Placement_DWD_Alpha {
  public:
    using Column = ServicePath;
    using Solution = Solution<int>;

    struct DualValues {
        DualValues(const Placement_DWD_Alpha& _rmp)
            : inst(_rmp.m_inst)
            , m_onePathDuals(_rmp.m_onePathCons.size(), 0.0)
            , m_linkCapasDuals(_rmp.m_linkCapasCons.size(), 0.0)
            , m_nodeCapasDuals(_rmp.m_nodeCapasCons.size(), 0.0)
            , m_pathFuncDuals(std::vector<int>(
                _rmp.m_pathFuncCons.shape(), _rmp.m_pathFuncCons.shape()+3))
            , m_nbLicensesDuals(_rmp.m_nbLicensesCons.size(), 0.0) {}

        DualValues(const DualValues& _other) = default;
        DualValues(DualValues&& _other) = default;
        DualValues& operator=(const DualValues& _other) = default;
        DualValues& operator=(DualValues&& _other) = default;
        ~DualValues() = default;

        IloNum getReducedCost(const CrossLayerLink& _link) const {
            return - inst->nbCores(_link.demandID, _link.j) * m_nodeCapasDuals[_link.u]
                  - m_pathFuncDuals[_link.demandID][_link.j][_link.u];
        }

        IloNum getReducedCost(const IntraLayerLink& _link) const {
            return inst->demands[_link.demandID].d * (1 - 
                m_linkCapasDuals[inst->edgeToId(_link.edge.first, _link.edge.second)]);
        }

        const Instance* inst;
        std::vector<IloNum> m_onePathDuals;
        std::vector<IloNum> m_linkCapasDuals;
        std::vector<IloNum> m_nodeCapasDuals;
        boost::multi_array<IloNum, 3> m_pathFuncDuals;
        std::vector<IloNum> m_nbLicensesDuals;
    };

    explicit Placement_DWD_Alpha(const Instance* _inst);
    Placement_DWD_Alpha& operator=(const Placement_DWD_Alpha&) = delete;
    Placement_DWD_Alpha& operator=(Placement_DWD_Alpha&&) = delete;
    Placement_DWD_Alpha(const Placement_DWD_Alpha&) = delete;
    Placement_DWD_Alpha(Placement_DWD_Alpha&&) = delete;
    ~Placement_DWD_Alpha() { m_env.end(); }

    const DualValues& getDualValues() const;
    double getObjValue() const;
    void addColumns(const std::vector<Column>& _sPaths);
    IloNumColumn getColumn(const Column& _col);
    void getDuals();
    void getNetworkUsage(const IloCplex& _sol) const;
    bool solveInteger();
    bool checkReducedCosts() const;
    void relax();
    template<bool CLEAR = false>
    bool solve() {
        if constexpr(CLEAR) {
            m_solver.extract(m_model);
        }
        std::cout << "# cols: " << m_solver.getNcols()
        <<  ", # rows: " << m_solver.getNrows() << '\n';
        
        if (m_solver.solve() == IloFalse) {
            return false;
        }
        std::cout << "CplexStatus: " << m_solver.getCplexStatus() << '\n';
        std::cout << "CplexSubStatus: " << m_solver.getCplexSubStatus() << '\n';
        std::cout << "Kappa: " << m_solver.getQuality(IloCplex::Kappa) << '\n';
        m_fractObj = m_solver.getObjValue();
        getDuals();
        assert(checkReducedCosts());
        removeDummyIfPossible();
        if constexpr(CLEAR) {
            m_solver.clear();
        }
        return true;
    }

    double getDualSumRHS(const DualValues& _dualValues) const;
    double getAdditionalVariable(const DualValues& _dualValues, const std::vector<Column>& _col) const;

    inline int getNbPricing() const {
        return m_inst->demands.size();
    }

    inline int getNbColumns() const {
        return m_paths.size();
    }

    inline IloNum getNodeCapacityDual(Graph::Node _u) const {
        return normalDuals.m_nodeCapasDuals[_u];
    }

    inline IloNum getLinkCapacityDual(int _e) const {
        return normalDuals.m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual(int _demandID) const {
        return normalDuals.m_onePathDuals[_demandID];
    }

    inline IloNum getPathFunctionDual(const int _demandID, int _j, int _u) const {
        return normalDuals.m_pathFuncDuals[_demandID][_j][_u];
    }

    inline IloNum getNodeCapacityDual_stab(Graph::Node _u) const {
        return stabDuals.m_nodeCapasDuals[_u];
    }

    inline IloNum getLinkCapacityDual_stab(int _e) const {
        return stabDuals.m_linkCapasDuals[_e];
    }

    inline IloNum getOnePathDual_stab(int _demandID) const {
        return stabDuals.m_onePathDuals[_demandID];
    }

    inline IloNum getPathFunctionDual_stab(const int _demandID, int _j, int _u) const {
        return stabDuals.m_pathFuncDuals[_demandID][_j][_u];
    }

    inline double getRelaxationObjValue() const {
        return m_intObj;
    }

    inline double getIntegerObjValue() const {
        return m_fractObj;
    }

    std::vector<ServicePath> getServicePaths() const;
    Solution getSolution() const;

    double getStabilizedLowerBound(const std::vector<Column>& _columns) const;
    double getBestLowerBound(double _bestLowerBound, double _rc, const std::vector<Column>& _col);
    double getReducedCost(const ServicePath& _sPath) const;
    // double getBaseLagragian(const ServicePath& _sPath, const DualValues& _dualValues) const;

    void setAlpha(double _alpha) {
        m_alpha = _alpha;
    }

  private:
    double getReducedCost(const ServicePath& _sPath, const DualValues& _dualValues) const;
    double getLowerBound(const double _rc, const std::vector<Column>& _col, const DualValues& _dualValues);
    void removeDummyIfPossible();
    void updateLagrangianObjectif(DualValues _dualValues);
    const Instance* m_inst;
    IloEnv m_env = IloEnv();
    
    IloModel m_model = IloModel(m_env);
    IloModel m_integerConversions = IloModel(m_env);
    std::vector<IloNumVar> m_y{};
    boost::multi_array<IloNumVar, 2> m_b;
    IloObjective m_obj;
    std::vector<IloRange> m_onePathCons;
    std::vector<IloRange> m_linkCapasCons;
    std::vector<IloRange> m_nodeCapasCons;
    std::vector<IloRange> m_nbLicensesCons;
    boost::multi_array<IloRange, 3> m_pathFuncCons;
    
    std::vector<IloNumVar> m_dummyPaths;
    bool m_dummyActive = true;
    DualValues normalDuals = DualValues(*this);
    double m_alpha = 0.5;
    DualValues bestDuals = DualValues(*this);
    DualValues stabDuals = DualValues(*this);
    std::vector<IloNum> m_linkCharge;
    std::vector<IloNum> m_nodeCharge;
    boost::multi_array<IloNum, 2> m_funcPath;
    IloCplex m_solver;
    std::vector<ServicePath> m_paths { };
    double m_intObj = -1;
    double m_fractObj = -1;
};

class PricingProblemLP {
  public:
    using Column = ServicePath;

    explicit PricingProblemLP(const Instance* _inst);
    PricingProblemLP(const PricingProblemLP&) = default;
    PricingProblemLP& operator=(const PricingProblemLP&) = default;
    PricingProblemLP(PricingProblemLP&&) = default;
    PricingProblemLP& operator=(PricingProblemLP&&) = default;
    ~PricingProblemLP() {
        m_env.end();
    }

    void setPricingID(int _demandID);
    void updateDual(const Placement_DWD& _rmp);
    void updateDual_stab(const Placement_DWD& _rmp);

    bool solve();
    double getObjValue() const;

    void resetBBounds();
    Column getColumn() const;

  private:
    const Instance* m_inst;
    IloEnv m_env = IloEnv();

    int m_demandID = 0;
    const int n;
    const int m;
    const int nbLayers;
    IloModel m_model = IloModel(m_env);

    IloObjective m_obj;

    IloNumVarArray m_a;
    IloNumVarArray m_b;
    IloNumVarArray m_f;

    IloRangeArray m_flowConsCons;
    IloRangeArray m_linkCapaCons;
    IloRangeArray m_nodeCapaCons;

    IloRangeArray m_funcNodeUsageCons;
    IloRangeArray m_nbLicensesCons;

    IloCplex m_solver;

    int getIndexF(int _e, int _j) const;
    int getIndexA(Graph::Node _u, int _j) const;
    int getIndexB(Graph::Node _u, function_descriptor _f) const;
};

class Lagrangian_Path {
  public:
    explicit Lagrangian_Path(const Instance* _inst);
    Lagrangian_Path(const Lagrangian_Path&) = default;
    Lagrangian_Path& operator=(const Lagrangian_Path&) = default;
    Lagrangian_Path(Lagrangian_Path&&) = default;
    Lagrangian_Path& operator=(Lagrangian_Path&&) = default;
    ~Lagrangian_Path() {
        m_env.end();
    }

    inline bool solve() {
        return m_solver.solve() == IloTrue;
    }

    inline double getObjValue() const {
        return m_solver.getObjValue();
    }

  private:
    const Instance* m_inst;
    IloEnv m_env = IloEnv();
    const int n;
    const int m;
    const int nbLayers;
    IloModel m_model = IloModel(m_env);

    IloNumVarArray m_a;
    IloNumVarArray m_b;
    IloNumVarArray m_f;

    IloObjective m_obj;

    IloRangeArray m_flowConsCons;

    IloCplex m_solver = IloCplex(m_model);

    inline int getIndexA(const Graph::Node _u, const int _i, const int _j) {
        assert(m_inst->isNFV[_u]);
        assert(_i < m_inst->demands.size());
        assert(_j < m_inst->demands[_i].functions.size());
        return _j + _i * m_inst->maxChainSize + _u * m_inst->demands.size() * m_inst->maxChainSize;
    }

    inline int getIndexB(const Graph::Node _u, const int _f) {
        assert(m_inst->isNFV[_u]);
        return m_inst->network.getOrder() * _f + _u;
    }

    inline int getIndexFC(const Graph::Node _u, const int _i, const int _j) {
        assert(_u < m_inst->network.getOrder());
        assert(_i < m_inst->demands.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * (m_inst->maxChainSize + 1) + _u * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }

    inline int getIndexF(const int _e, const int _i, const int _j) {
        assert(_e < m_inst->network.size());
        assert(_i < m_inst->demands.size());
        assert(_j <= m_inst->demands[_i].functions.size());
        return _j + _i * (m_inst->maxChainSize + 1) + _e * m_inst->demands.size() * (m_inst->maxChainSize + 1);
    }
};

class PricingProblemBF {
  public:
    using Column = ServicePath;

    explicit PricingProblemBF(const Instance* _inst);
    PricingProblemBF(const PricingProblemBF&) = default;
    PricingProblemBF(PricingProblemBF&&) = default;
    PricingProblemBF& operator=(const PricingProblemBF&) = default;
    PricingProblemBF& operator=(PricingProblemBF&&) = default;
    ~PricingProblemBF() = default;

    void setPricingID(int _demandID);
    void updateDual(const Placement_DWD_Alpha& _rmp);
    void updateDual(const Placement_DWD_Alpha::DualValues& _dualValues);
    bool solve();
    double getObjValue() const;
    Column getColumn() const;

    void updateDual(const Placement_EM& _rmp);
    void updateDual_stab(const Placement_EM& _rmp);

  private:
    const Instance* m_inst;
    int n;
    int m;
    int nbLayers;
    int m_demandID = 0;
    DiGraph m_layeredGraph;
    bool isNegative {false};
    ShortestPathBellmanFord<DiGraph, epsilon_less<double>> m_bf {m_layeredGraph};
    ShortestPath<DiGraph, epsilon_less<double>> m_dijkstra {m_layeredGraph};
    Graph::Path m_path {};
    double m_objValue {};
};

std::ostream& operator<<(std::ostream& _out, const Placement_DWD_Alpha::DualValues& _dualValues);
} // namespace SFC::OccLim
#endif
