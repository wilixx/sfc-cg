// #ifndef CHAINING_SUBSET2_HPP
// #define CHAINING_SUBSET2_HPP

// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <iostream>
// #include <map>
// #include <set>
// #include <tuple>

// #include "DiGraph.hpp"
// #include "SFC.hpp"
// #include "ShortestPath.hpp"
// #include "utility.hpp"

// #define RC_EPS 1.0e-6

// class ChainingSubset2 {
//   public:
//     typedef std::vector<std::vector<std::pair<DemandID, std::vector<int>>>> Column;

//   private:
//     class MainProblem {
//         friend ChainingSubset2;

//       private:
//         const ChainingSubset2& m_placement;
//         IloModel m_model;

//         IloObjective m_obj;

//         // Variables
//         IloNumVarArray m_g;
//         IloNumVarArray m_a;

//         // Constraints
//         IloRangeArray m_flowConservationCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_nodeCapaCons;

//         IloCplex m_solver;

//         std::vector<Column> m_cols;

//       public:
//         MainProblem(ChainingSubset2& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_g(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env, m_placement.m_inst.demands.size() * (1 + m_placement.m_funcCharge.size()) * m_placement.m_inst.network.size(),
//                       0.0, IloInfinity)))
//             , m_a(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env)))
//             , m_flowConservationCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * (1 + m_placement.m_funcCharge.size()) * m_placement.m_inst.network.getOrder(),
//                       0.0, 0.0)))
//             , m_linkCapaCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(),
//                       -IloInfinity, 0.0)))
//             , m_nodeCapaCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder(),
//                       -IloInfinity, 0.0)))
//             , m_solver()
//             , m_cols() {
//             std::cout << "Building main problem..." << '\n';

//             // Set upper bound for nodes capacity
//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 m_nodeCapaCons[u].setUB(m_placement.m_inst.nodeCapa[u]);
//                 setIloName(m_nodeCapaCons[u], "m_nodeCapaCons" + toString(u));
//             }

//             std::cout << "Set upper bound for link capacity...\n";
//             int e = 0;
//             for (const Graph::Edge& edge : m_placement.m_inst.network.getEdges()) {
//                 IloNumVarArray vars(m_placement.m_env);
//                 IloNumArray vals(m_placement.m_env);
//                 for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     const Demand& demand = m_placement.m_inst.demands[i];
//                     const double d = std::get<2>(demand);
//                     const std::vector<function_descriptor>& funcs = std::get<3>(demand);
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         const int index = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                           + j * m_placement.m_inst.network.size()
//                                           + e;
//                         vars.add(m_g[index]);
//                         vals.add(d);
//                     }
//                 }
//                 m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edge));
//                 m_linkCapaCons[e].setLinearCoefs(vars, vals);
//                 ++e;
//             }

//             std::cout << "Build flow conservation constraints...\n";
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const Demand& demand = m_placement.m_inst.demands[i];
//                 const int s = std::get<0>(demand),
//                           t = std::get<1>(demand);
//                 const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//                 for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         IloNumVarArray vars(m_placement.m_env);
//                         IloNumArray vals(m_placement.m_env);
//                         for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                             const int index1 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                                + j * m_placement.m_inst.network.size()
//                                                + m_placement.m_arcToId.at(Graph::Edge(u, v));
//                             const int index2 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                                + j * m_placement.m_inst.network.size()
//                                                + m_placement.m_arcToId.at(Graph::Edge(v, u));

//                             vars.add(m_g[index1]);
//                             vals.add(1.0);
//                             vars.add(m_g[index2]);
//                             vals.add(-1.0);
//                         }
//                         m_flowConservationCons[getFlowConsIndex(i, j, u)].setLinearCoefs(vars, vals);
//                         setIloName(m_flowConservationCons[getFlowConsIndex(i, j, u)],
//                             "m_flowConservationCons" + toString(std::make_tuple(u, j, m_placement.m_inst.demands[i])));
//                     }
//                     m_flowConservationCons[getFlowConsIndex(i, 0, s)].setBounds(1.0, 1.0);
//                     m_flowConservationCons[getFlowConsIndex(i, funcs.size(), t)].setBounds(-1.0, -1.0);
//                 }
//             }

//             std::cout << "Objective function...\n";
//             IloNumVarArray vars(m_placement.m_env);
//             IloNumArray vals(m_placement.m_env);
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const Demand& demand = m_placement.m_inst.demands[i];
//                 const double d = std::get<2>(demand);
//                 const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.network.getEdges()) {
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         const int index = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                           + j * m_placement.m_inst.network.size()
//                                           + e;
//                         vars.add(m_g[index]);
//                         vals.add(d);
//                         setIloName(m_g[index], "g" + toString(std::make_tuple(edge, m_placement.m_inst.demands[i], j)));
//                     }
//                     ++e;
//                 }
//             }
//             m_obj.setLinearCoefs(vars, vals);
//             m_solver = IloCplex(m_model);
//         }

//         inline int getFlowConsIndex(int _i, int _j, int _u) const {
//             assert(_i >= 0 && _i < int(m_placement.m_inst.demands.size()));
//             assert(_j >= 0 && _j < int(m_placement.m_inst.network.size() + 1));
//             assert(_u >= 0 && _u < m_placement.m_inst.network.getOrder());
//             return _i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.getOrder())
//                    + _j * (m_placement.m_inst.network.getOrder())
//                    + _u;
//         }

//         MainProblem& operator=(const MainProblem&) = default;
//         MainProblem(const MainProblem&) = default;
//         MainProblem& operator=(MainProblem&&) = default;
//         MainProblem(MainProblem&&) = default;

//         void addColumnCheck(const Column& _col) {
//             if (std::find(m_cols.begin(), m_cols.end(), _col) == m_cols.end()) {
//                 addColumn(_col);
//             }
//         }

//         void addColumn(const Column& _col) {
// #if !defined(NDEBUG)
//             std::cout << "addColumn " << '\n';
// #endif
//             assert(std::find(m_cols.begin(), m_cols.end(), _col) == m_cols.end()
//                    || (std::cout << _col << " is already present!" << '\n', false));

//             std::map<int, double> cons;
//             IloNumColumn col(m_placement.m_env);
//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 int charge = 0;

//                 for (const auto& pair_DemandFuncs : _col[u]) {
//                     const auto& i = pair_DemandFuncs.first;
//                     for (const auto& j : pair_DemandFuncs.second) {
//                         const auto& d = std::get<2>(m_placement.m_inst.demands[i]);
//                         const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                         const auto& sameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                         const auto& nextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                         charge += ceil(d / m_placement.m_funcCharge[funcs[j]]);

//                         cons[sameLayer] = cons[sameLayer] + 1.0;
//                         cons[nextLayer] = cons[nextLayer] - 1.0;
//                     }
//                     col += m_nodeCapaCons[u](charge);
//                 }
//                 assert(charge <= m_placement.m_inst.nodeCapa[u]
//                        || (std::cout << charge << ">" << m_placement.m_inst.nodeCapa[u] << '\n', false));
//             }
//             for (const auto& pair_ICoef : cons) {
//                 col += m_flowConservationCons[pair_ICoef.first](pair_ICoef.second);
//             }

//             IloNumVar aVar(col);
//             setIloName(aVar, "a" + toString(m_a.getSize()));
//             m_a.add(aVar);
//             m_cols.push_back(_col);
//         }

//         double getReducedCost(const IloCplex& _sol, const Column& _col) {
//             double reducedCost = 0.0;
//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 for (const auto& pair_DemandIDFunctions : _col[u]) {
//                     const auto& i = pair_DemandIDFunctions.first;
//                     for (const auto& j : pair_DemandIDFunctions.second) {
//                         const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                         const double d = std::get<2>(m_placement.m_inst.demands[i]);

//                         const double charge = ceil(d / m_placement.m_funcCharge[funcs[j]]);
//                         const int indexSameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                         const int indexNextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                         reducedCost += -charge * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[u])
//                                        - _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer])
//                                        + _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]);
//                     }
//                 }
//             }
//             return reducedCost;
//         }

//         void solveInteger() {
//             if (m_solver.solve()) {
//                 std::cout << "Final obj value: " << m_solver.getObjValue() << "\t# Paths: " << m_cols.size() << '\n';
//             } else {
//                 std::cout << "No integer solution found!" << '\n';
//             }
//         }
//     };

//     class PricingProblem {
//         friend ChainingSubset2;

//       private:
//         const ChainingSubset2& m_placement;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloNumVarArray m_x;

//         IloRangeArray m_capaCons;
//         IloRangeArray m_oneLocCons;

//       public:
//         PricingProblem(const ChainingSubset2& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_x(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.demands.size() * m_placement.m_inst.network.getOrder(),
//                       0.0, 1.0, ILOBOOL)))
//             , m_capaCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder(),
//                       -IloInfinity, 0.0)))
//             , m_oneLocCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.demands.size(),
//                       0.0, 0.0))) {
//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 m_capaCons[u].setUB(m_placement.m_inst.nodeCapa[u]);
//                 setIloName(m_capaCons[u], "m_capaCons(" + std::to_string(u) + ')');
//             }
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     setIloName(m_oneLocCons[m_placement.m_inst.demands.size() * j + i], "m_oneLocCons" + toString(std::make_pair(i, j)));
//                 }
//             }

//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const double d = std::get<2>(m_placement.m_inst.demands[i]);
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);

//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                         m_capaCons[u].setLinearCoef(m_x[getXIndex(u, i, j)], ceil(d / m_placement.m_funcCharge[funcs[j]]));
//                         m_oneLocCons[m_placement.m_inst.demands.size() * j + i].setLinearCoef(m_x[getXIndex(u, i, j)], 1.0);
//                         m_oneLocCons[m_placement.m_inst.demands.size() * j + i].setBounds(1.0, 1.0);
//                         setIloName(m_x[getXIndex(u, i, j)], "x" + toString(std::make_tuple(i, j, u)));
//                     }
//                 }
//             }
//         }

//         inline int getXIndex(const Graph::Node& _u, const DemandID& _i, const int _j) const {
//             return _u * m_placement.m_inst.demands.size() * m_placement.m_funcCharge.size()
//                    + _i * m_placement.m_funcCharge.size()
//                    + _j;
//         }

//         inline double getReducedCost(const Graph::Node _u, const DemandID _i, const int _j, const IloCplex& _sol) {
//             const int indexSameLayer = m_placement.m_placementModel.getFlowConsIndex(_i, _j, _u);
//             const int indexNextLayer = m_placement.m_placementModel.getFlowConsIndex(_i, _j + 1, _u);
// #if !defined(NDEBUG) && 0
//             std::cout << "m_x" << std::make_pair(_i, _j) << " -> "
//                       << std::make_tuple(_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer]),
//                              _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]),
//                              -_sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[_u]))
//                       << '\n';
// #endif

//             const auto& funcs = std::get<3>(m_placement.m_inst.demands[_i]);
//             const double d = std::get<2>(m_placement.m_inst.demands[_i]);
//             return -ceil(d / m_placement.m_funcCharge[funcs[_j]]) * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[_u])
//                    - _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer])
//                    + _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]);
//         }

//         void updateDual(const IloCplex& _sol) {
//             // For each demands
//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);

//                     for (int j = 0; j < int(funcs.size()); ++j) {
//                         m_obj.setLinearCoef(m_x[getXIndex(u, i, j)], getReducedCost(u, i, j, _sol));
//                     }
//                 }
//             }
// #if !defined(NDEBUG) && 1
//             std::cout << m_model << '\n';
// #endif
//         }

//         ChainingSubset2::Column getCol(const IloCplex& _sol) const {
//             ChainingSubset2::Column col(m_placement.m_inst.network.getOrder());

//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 int charge = 0;
//                 for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     std::vector<int> funcsInstalled;
//                     const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                     for (int j = 0; j < int(funcs.size()); ++j) {
//                         if (_sol.getValue(m_x[getXIndex(u, i, j)])) {
//                             funcsInstalled.emplace_back(j);
//                             std::cout << m_x[getXIndex(u, i, j)] << '\n';
//                             charge += ceil(std::get<2>(m_placement.m_inst.demands[i]) / m_placement.m_funcCharge[funcs[j]]);
//                         }
//                     }
//                     col[u].emplace_back(i, funcsInstalled);
//                 }
//                 assert(charge <= m_placement.m_inst.nodeCapa[u]
//                        || (std::cout << charge << ">" << m_placement.m_inst.nodeCapa[u] << '\n', _sol.exportModel("pp.lp"), false));
//             }

//             return col;
//         }
//     };

//   public:
//     ChainingSubset2(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_funcCharge(_funcCharge)
//         , m_inst.nodeCapa(_nodeCapa)
//         , m_arcToId(buildArcToId())
//         , m_placementModel(*this)
//         , m_pp(*this) {
//     }

//     std::map<Graph::Edge, int> buildArcToId() const {
//         std::map<Graph::Edge, int> arcToId;
//         int i = 0;
//         for (const auto& arc : m_inst.network.getEdges()) {
//             arcToId[arc] = i;
//             i++;
//         }
//         return arcToId;
//     }

//     ~ChainingSubset2() {
//     }

//     ChainingSubset2& operator=(const ChainingSubset2&) = default;
//     ChainingSubset2(const ChainingSubset2&) = default;
//     ChainingSubset2& operator=(ChainingSubset2&&) = default;
//     ChainingSubset2(ChainingSubset2&&) = default;

//     void solve() {
//         m_placementModel.m_solver.setOut(m_env.getNullStream());
//         std::vector<Column> colsToAdd;
//         do {
// #if !defined(NDEBUG) && 0
//             std::cout << m_placementModel.m_model << '\n';
// #endif
//             if (m_placementModel.m_solver.solve()) {

// #if !defined(NDEBUG)
//                 std::cout << m_placementModel.m_model << '\n';
// #endif

//                 std::cout << "Reduced Main Problem Objective Value = " << m_placementModel.m_solver.getObjValue() << '\n';
// // Report flow used

// #if !defined(NDEBUG) && 1
//                 std::cin.ignore();
//                 for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//                     std::cout << m_inst.demands[i] << '\n';
//                     const std::vector<function_descriptor>& funcs = std::get<3>(m_inst.demands[i]);
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         for (int e = 0; e < m_inst.network.size(); ++e) {
//                             // g_{fuv}^{st}
//                             const int index = i * ((m_funcCharge.size() + 1) * m_inst.network.size())
//                                               + j * m_inst.network.size()
//                                               + e;

//                             if (m_placementModel.m_solver.getValue(m_placementModel.m_g[index])) {
//                                 std::cout << '\t' << m_placementModel.m_g[index] << '\n';
//                             }
//                         }
//                     }
//                 }

//                 for (int i = 0; i < m_placementModel.m_a.getSize(); ++i) {
//                     if (m_placementModel.m_solver.getValue(m_placementModel.m_a[i])) {
//                         std::cout << m_placementModel.m_a[i] << '\n';
//                     }
//                 }

// #endif

// #if !(defined(NDEBUG))
//                 std::cout << "Reduced cost\n";
//                 for (int i = 0; i < m_placementModel.m_a.getSize(); ++i) {
//                     const auto rc = m_placementModel.getReducedCost(m_placementModel.m_solver, m_placementModel.m_cols[i]);
//                     assert(std::abs(rc - m_placementModel.m_solver.getReducedCost(m_placementModel.m_a[i])) < RC_EPS || (std::cout << rc << " vs. " << m_placementModel.m_solver.getReducedCost(m_placementModel.m_a[i]) << '\n', false));
//                 }
// #endif

//                 colsToAdd.clear();
//                 searchColumn(colsToAdd);
//                 for (auto& p : colsToAdd) {
//                     m_placementModel.addColumn(p);
//                 }
//             } else {
//                 std::cout << "No solution for reduced main problem!" << '\n';
//                 m_placementModel.m_solver.exportModel("rm.lp");
//             }
//         } while (!colsToAdd.empty());

//         m_placementModel.solveInteger();
//     }

//     void addInitConf(const Column _col) {
//         m_placementModel.addColumn(_col);
//     }

//     void searchColumn(std::vector<Column>& _cols, const double _threshold = -RC_EPS) {
//         /* For each demands, search for a shortest path in the augmented graph */
//         m_pp.updateDual(m_placementModel.m_solver);

//         IloCplex ppSolver(m_pp.m_model);
//         ppSolver.setOut(m_env.getNullStream());
//         if (ppSolver.solve()) {
//             const double reducedCost = ppSolver.getObjValue();
// #if !defined(NDEBUG)
//             std::cout << "Solved PP " << reducedCost;
// #else
//             std::cout << '\r' << "Solved PP -> " << reducedCost << std::flush;
// #endif
//             if (reducedCost <= _threshold) {
//                 _cols.push_back(m_pp.getCol(ppSolver));
//             }
// #if !defined(NDEBUG)
//             std::cout << '\n';
// #endif
//         }
// #if !defined(NDEBUG)
//         else {
//             std::cout << "No solution PP \n";
//             ppSolver.exportModel("pp.lp");
//         }
// #endif
//         ppSolver.end();
//     }

//   private:
//     const IloEnv m_env { };
//     const DiGraph m_inst.network;
//     const std::vector<Demand> m_inst.demands;
//     const std::vector<double> m_funcCharge;
//     const std::vector<int> m_inst.nodeCapa;

//     const std::map<Graph::Edge, int> m_arcToId;

//     MainProblem m_placementModel;
//     PricingProblem m_pp;

//     friend MainProblem;
// };

// #endif