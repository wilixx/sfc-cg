#ifndef CHAININGPATHBUILDER_HPP
#define CHAININGPATHBUILDER_HPP

#include <chrono>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <ilcplex/ilocplex.h>
#include <map>
#include <tuple>

#include "DiGraph.hpp"
#include "ShortestPath.hpp"
#include "cplex_utility.hpp"
#include "utility.hpp"

#define RC_EPS 1.0e-6

#include "SFC.hpp"

class ChainingPathBuilder {
    class PlacementModel {
        friend ChainingPathBuilder;

      public:
        PlacementModel(ChainingPathBuilder& _placement)
            : m_placement(_placement)
            , m_model(m_placement.m_env)
            , m_y(IloNumVarArray(m_placement.m_env))
            , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
            , m_onePathCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size(), 1.0, 1.0)))
            , m_solver(m_model)
            , m_paths() {
            for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
                setIloName(m_onePathCons[i], "m_onePathCons(" + toString(i) + ')');
            }
            m_solver.setOut(m_placement.m_env.getNullStream());
        }

        PlacementModel& operator=(const PlacementModel&) = delete;
        PlacementModel(const PlacementModel&) = delete;

        void addPath(const ServicePath& _sPath, int _i) {
            std::cout << "addPath" << _sPath << '\n';
            assert([&]() {
                auto ite = std::find(m_paths.begin(), m_paths.end(), std::make_pair(_sPath, _i));
                if (ite == m_paths.end()) {
                    return true;
                } else {
                    std::cout << _sPath << " is already present!" << '\n';
                    return false;
                }
            }());

            const auto& path = _sPath.first;
            const Demand& demand = m_placement.m_inst.demands[_i];
            // int s = std::get<0>(demand), t = std::get<1>(demand);
            const double d = std::get<2>(demand);
            int nbFunctions = 0;
            for (const auto& installs : _sPath.second) {
                nbFunctions += installs.second.size();
                assert(m_placement.m_isNFV[installs.first]);
            }
            assert(nbFunctions == m_placement.m_inst.demands[_i].functions.size() || (std::cout << demand << " -> " << _sPath << '\n', false));
            IloNumColumn inc = m_onePathCons[_i](1.0) + m_obj(d * (path.size() - 1));

            auto yVar = IloNumVar(inc);
            setIloName(yVar, "y" + toString(std::make_pair(_sPath, _i)));
            m_y.add(yVar);
            m_paths.emplace_back(_sPath, _i);
        }

        std::pair<DiGraph, std::vector<int>> solveInteger() {
            m_model.add(IloConversion(m_placement.m_env, m_y, ILOINT));
            m_solver.solve();
            std::cout << "Final obj value: " << m_solver.getObjValue() << "\t# Paths: " << m_paths.size() << '\n';

            /* Create capacties
				@Todo: Move to own function
				*/

            std::vector<double> linkCharges(m_placement.m_inst.network.size(), 0);
            std::vector<int> nodeCharges(m_placement.m_inst.network.getOrder(), -1);

            for (const auto& u : m_placement.m_inst.NFVNodes) {
                nodeCharges[u] = 0;
            }

            int totalNbCores = 0;

            for (int i = 0; i < int(m_paths.size()); ++i) {
                if (m_solver.getValue(m_y[i])) {
                    const Graph::Path& path = m_paths[i].first.first;
                    const std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>>& funcPlacement = m_paths[i].first.second;
                    const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
                    const double d = std::get<2>(demand);

                    for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                        linkCharges[m_placement.m_inst.edgeToId[Graph::Edge(*iteU, *iteV)]] += d;
                    }
                    for (const auto& pair_NodeFuncs : funcPlacement) {
                        for (const auto& func : pair_NodeFuncs.second) {
                            totalNbCores += ceil(d / m_placement.m_funcCharge[func]);
                            nodeCharges[pair_NodeFuncs.first] += ceil(d / m_placement.m_funcCharge[func]);
                        }
                    }
                }
            }
            std::cout << "Total # cores: " << totalNbCores << '\n';

            double maxCapa = *std::max_element(linkCharges.begin(), linkCharges.end());

            std::vector<double> realCapas(m_placement.m_inst.network.size(), 0);
            for (auto& edge : m_placement.m_inst.network.getEdges()) {
                if (edge.first < edge.second) {
                    double alpha = (90 + rand() % 30) / 100.0;
                    std::cout << edge << " -> " << alpha << '\n';
                    // Use max capa of all links because links sucks ass
                    realCapas[m_placement.m_inst.edgeToId[edge]] = alpha * maxCapa;
                    // realCapas[m_placement.m_inst.edgeToId[edge]] = alpha
                    // 	* std::max(linkCharges[m_placement.m_inst.edgeToId[edge]], linkCharges[m_placement.m_inst.edgeToId[Graph::Edge(edge.second, edge.first)]]);
                }
            }

            DiGraph networkWithCapacities(m_placement.m_inst.network.getOrder());
            for (auto& edge : m_placement.m_inst.network.getEdges()) {
                if (edge.first < edge.second) {
                    networkWithCapacities.addEdge(edge.first, edge.second, realCapas[m_placement.m_inst.edgeToId[edge]]);
                    networkWithCapacities.addEdge(edge.second, edge.first, realCapas[m_placement.m_inst.edgeToId[edge]]);
                    std::cout << edge << " -> " << realCapas[m_placement.m_inst.edgeToId[edge]] << '\n';
                }
            }

            std::vector<double> nodeAlphas(m_placement.m_inst.NFVNodes.size());
            for (auto& v : nodeAlphas) {
                v = (90 + rand() % 30) / 100.0;
            }

            const double factor = std::accumulate(nodeAlphas.begin(), nodeAlphas.end(), 0.0) / m_placement.m_inst.NFVNodes.size();
            std::cout << "factor: " << factor << '\n';
            for (auto& v : nodeAlphas) {
                v /= factor;
            }
            for (Graph::Node u(0); u < int(m_placement.m_inst.NFVNodes.size()); ++u) {
                nodeCharges[m_placement.m_inst.NFVNodes[u]] = std::ceil(nodeCharges[m_placement.m_inst.NFVNodes[u]] * nodeAlphas[u]);
                std::cout << m_placement.m_inst.NFVNodes[u] << "->" << nodeCharges[m_placement.m_inst.NFVNodes[u]] << " (" << nodeAlphas[u] << ")" << '\n';
            }

            // for(Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
            // 	double alpha = (90 + rand() % 30) / 100.0;
            // 	if(nodeCharges[u] != -1) {
            // 		nodeCharges[u] *= alpha;
            // 	}
            // std::cout << u << "->" << nodeCharges[u] << " ("<<alpha<<")" << '\n';
            // }

            return std::make_pair(networkWithCapacities, nodeCharges);
        }

      private:
        ChainingPathBuilder& m_placement;
        IloModel m_model;
        IloNumVarArray m_y;

        IloObjective m_obj;

        IloRangeArray m_onePathCons;

        IloCplex m_solver;

        std::vector<std::pair<ServicePath, int>> m_paths;
    };

    class PricingProblem {
        friend ChainingPathBuilder;

      public:
        PricingProblem(int _demandID, ChainingPathBuilder& _chainPlacement)
            : m_placement(_chainPlacement)
            , m_demandID(_demandID)
            , n(m_placement.m_inst.network.getOrder())
            , m(m_placement.m_inst.network.size())
            , nbLayers(m_placement.m_funcCharge.size() + 1)
            , m_model(m_placement.m_env)
            , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
            , m_a(IloAdd(m_model, IloNumVarArray(m_placement.m_env, n * nbLayers, 0, 1, ILOBOOL)))
            , m_f(IloAdd(m_model, IloNumVarArray(m_placement.m_env, m * nbLayers, 0, 1, ILOBOOL)))
            , m_flowConsCons(IloAdd(m_model, IloRangeArray(m_placement.m_env, n * nbLayers, 0.0, 0.0)))
        // m_noLoopCons( IloAdd(m_model, IloRangeArray(m_placement.m_env, 2 * n, 0.0, 1.0) ))
        {
            const int s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
                      t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
            const std::vector<function_descriptor>& functions = m_placement.m_inst.demands[m_demandID].functions;

            const auto edges = m_placement.m_inst.network.getEdges();

            for (int e = 0; e < m; ++e) {
                for (int i = 0; i < nbLayers; ++i) {

                    m_flowConsCons[n * i + edges[e].first].setLinearCoef(m_f[m * i + e], 1.0);
                    m_flowConsCons[n * i + edges[e].second].setLinearCoef(m_f[m * i + e], -1.0);
                    // m_noLoopCons[edges[e].first].setLinearCoef(m_f[m * i + e], 1.0);
                    // m_noLoopCons[n + edges[e].second].setLinearCoef(m_f[m * i + e], 1.0);

                    setIloName(m_f[m * i + e], "f" + toString(std::make_tuple(edges[e], i)));
                }
            }

            for (const auto& u : m_placement.m_inst.NFVNodes) {
                for (int i = 0; i < int(m_placement.m_funcCharge.size()); ++i) {
                    m_flowConsCons[n * i + u].setLinearCoef(m_a[n * i + u], 1.0);
                    m_flowConsCons[n * (i + 1) + u].setLinearCoef(m_a[n * i + u], -1.0);

                    setIloName(m_a[n * i + u], "a" + toString(std::make_tuple(i, u)));
                }
            }

            m_flowConsCons.setNames("flowConservation");
            // m_noLoopCons.setNames("uniqueFlow");

            m_flowConsCons[n * 0 + s].setBounds(1.0, 1.0);
            m_flowConsCons[n * functions.size() + t].setBounds(-1.0, -1.0);
        }

        inline IloNumVar getF(const int _e, const int _j) const {
            assert(0 <= _e && _e < m_placement.m_inst.network.size());
            return m_f[m * _j + _e];
        }

        inline IloNumVar getF(const int _e, const int _j) {
            assert(0 <= _e && _e < m_placement.m_inst.network.size());
            return m_f[m * _j + _e];
        }

        void updateDual(int _demandID, const IloCplex& _sol) {
            // Update demand ID
            int s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
                t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
            double d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
            std::vector<function_descriptor>& functions = m_placement.m_inst.demands[m_demandID].functions;
            int source = n * 0 + s,
                sink = n * functions.size() + t;

            m_flowConsCons[source].setBounds(0.0, 0.0);
            m_flowConsCons[sink].setBounds(0.0, 0.0);

            m_demandID = _demandID;

            s = std::get<0>(m_placement.m_inst.demands[m_demandID]),
            t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
            d = std::get<2>(m_placement.m_inst.demands[m_demandID]);
            functions = m_placement.m_inst.demands[m_demandID].functions;
            source = n * 0 + s;
            sink = n * functions.size() + t;

            // Update node capa constraints
            IloNumArray coefs(m_placement.m_env, n * nbLayers);
            for (Graph::Node u = 0; u < n; ++u) {
                for (int i = 0; i < int(functions.size()); ++i) {
                    coefs[n * i + u] = m_placement.m_funcCharge[functions[i]] * d;
                }
                for (int i = functions.size(); i < int(m_placement.m_funcCharge.size()); ++i) {
                    coefs[n * i + u] = 0;
                }
            }

            m_flowConsCons[source].setBounds(1.0, 1.0);
            m_flowConsCons[sink].setBounds(-1.0, -1.0);

            m_obj.setConstant(-_sol.getDual(m_placement.m_placementModel.m_onePathCons[m_demandID]));

            IloNumArray objCoefs(m_placement.m_env, m * nbLayers);
            IloNumVarArray vars(m_placement.m_env, m * nbLayers);

            int index = 0;
            for (int e = 0; e < m; ++e) {
                for (int i = 0; i < nbLayers; ++i) {
                    objCoefs[index] = d;
                    vars[index] = m_f[m * i + e];
                    index++;
                }
            }
            m_obj.setLinearCoefs(vars, objCoefs);
        }

        ServicePath getServicePath(const IloCplex& _sol) const {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
            std::cout << "getServicePath() -> " << m_placement.m_inst.demands[m_demandID] << '\n';
#endif
            const auto edges = m_placement.m_inst.network.getEdges();
            const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
            const auto& t = std::get<1>(m_placement.m_inst.demands[m_demandID]);
            std::vector<std::vector<function_descriptor>> installs(m_placement.m_inst.network.getOrder());

            Graph::Node u = std::get<0>(m_placement.m_inst.demands[m_demandID]);
            int j = 0;
            Graph::Path path{u};

            IloNumArray aVal(m_placement.m_env);
            _sol.getValues(aVal, m_a);
            while (u != t || j < int(funcs.size())) {
                if (j < int(funcs.size()) && m_placement.m_isNFV[u] && aVal[n * j + u]) {
                    installs[u].push_back(funcs[j]);
                    ++j;
                } else {
                    for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                        if (_sol.getValue(getF(m_placement.m_inst.edgeToId.at(Graph::Edge(u, v)), j)) > 0) {
                            path.push_back(v);
                            u = v;
                            break;
                        }
                    }
                }
            }

            // Get localization of functions
            std::vector<std::pair<Graph::Node, std::vector<function_descriptor>>> finalInstalls;
            int nbFunctions = 0;
            for (const auto& u : m_placement.m_inst.NFVNodes) {
                if (!installs[u].empty()) {
                    finalInstalls.emplace_back(u, installs[u]);
                    nbFunctions += installs[u].size();
                }
            }
            assert(nbFunctions == funcs.size());
            return ServicePath(path, finalInstalls);
        }

      private:
        ChainingPathBuilder& m_placement;
        int m_demandID;
        int n;
        int m;
        int nbLayers;
        IloModel m_model;

        IloObjective m_obj;

        IloNumVarArray m_a;
        IloNumVarArray m_f;

        IloRangeArray m_flowConsCons;
        // IloRangeArray m_noLoopCons;
    };

  public:
    ChainingPathBuilder(const DiGraph& _network, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes)
        : m_env()
        , m_inst.network(_network)
        , m_funcCharge(_funcCharge)
        , m_inst.demands(_demands)
        , m_inst.maxChainSize(*std::max_element(m_inst.demands.begin(.functions, m_inst.demands.end(),
                                               [&](const Demand& _d1, const Demand& _d2) {
                                                   return _d1.functions > _d2.functions;
                                               }))
                              .size())
        , m_inst.NFVNodes(_NFVNodes)
        , m_isNFV([&]() {
            std::vector<int> isNFV(m_inst.network.getOrder(), 0);
            for (const auto& u : m_inst.NFVNodes) {
                isNFV[u] = 1;
            }
            return isNFV;
        }())
        , m_inst.edgeToId()
        , m_placementModel(*this) {
        int i = 0;
        for (auto& edge : m_inst.network.getEdges()) {
            m_inst.edgeToId[edge] = i;
            ++i;
        }
    }

    void addInitConf(const std::vector<ServicePath>& _paths) {
        int i = 0;
        for (auto& path : _paths) {
            m_placementModel.addPath(path, i);
            ++i;
        }
    }

    std::pair<DiGraph, std::vector<int>> solve() {
        IloCplex mainSolver(m_placementModel.m_model);
        mainSolver.setOut(m_env.getNullStream());

        PricingProblem pp(0, *this);
        bool reducedCost;
        do {
            reducedCost = false;
            mainSolver.solve();
            std::cout << "Reduced Main Problem Objective Value = " << mainSolver.getObjValue() << '\n';
            std::list<std::pair<ServicePath, int>> toAdd;
            for (int i = 0; i < int(m_inst.demands.size()); ++i) {
                /* For each demands, search for a shortest path in the augmented graph */
                pp.updateDual(i, mainSolver);

                IloCplex ppSolver(pp.m_model);
                ppSolver.exportModel("pp.sav");
                ppSolver.setOut(m_env.getNullStream());
                ppSolver.solve();

                if (ppSolver.getObjValue() < -RC_EPS) {
                    ServicePath sPath = pp.getServicePath(ppSolver);
#if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
                    std::cout << "Reduced cost of " << ppSolver.getObjValue() << " with " << sPath << '\n';
#endif
                    reducedCost = true;
                    toAdd.emplace_back(sPath, i);
                }

                ppSolver.end();
            }
            std::cout << '\n';
            for (auto& p : toAdd) {
                m_placementModel.addPath(p.first, p.second);
            }

        } while (reducedCost);
        return m_placementModel.solveInteger();
    }

  private:
    IloEnv m_env { };
    DiGraph m_inst.network;
    std::vector<double> m_funcCharge;
    std::vector<Demand> m_inst.demands;
    int m_inst.maxChainSize;
    std::vector<int> m_inst.NFVNodes;
    std::vector<int> m_isNFV;
    std::map<Graph::Edge, int> m_inst.edgeToId;

    PlacementModel m_placementModel;

    friend PlacementModel;
};

#endif