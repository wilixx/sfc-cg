// #ifndef CHAINING_SUBSET3_HPP
// #define CHAINING_SUBSET3_HPP

// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <iostream>
// #include <map>
// #include <set>
// #include <tuple>

// #include "DiGraph.hpp"
// #include "SFC.hpp"
// #include "ShortestPath.hpp"
// #include "utility.hpp"

// #define RC_EPS 1.0e-6

// class ChainingSubset3 {
//   public:
//     typedef std::pair<DemandID, std::vector<std::pair<Graph::Node, std::vector<int>>>> Column;

//   private:
//     class MainProblem {
//         friend ChainingSubset3;

//       private:
//         const ChainingSubset3& m_placement;
//         IloModel m_model;

//         IloObjective m_obj;

//         // Variables
//         IloNumVarArray m_g;
//         IloNumVarArray m_a;

//         // Constraints
//         IloRangeArray m_flowConservationCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_nodeCapaCons;
//         IloRangeArray m_oneSubset;

//         IloCplex m_solver;

//         std::vector<Column> m_cols;

//       public:
//         MainProblem(ChainingSubset3& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_g(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env, m_placement.m_inst.demands.size() * (1 + m_placement.m_funcCharge.size()) * m_placement.m_inst.network.size(),
//                       0.0, IloInfinity)))
//             , m_a(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env)))
//             , m_flowConservationCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * (1 + m_placement.m_funcCharge.size()) * m_placement.m_inst.network.getOrder(),
//                       0.0, 0.0)))
//             , m_linkCapaCons(
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(),
//                       -IloInfinity, 0.0))
//             , m_nodeCapaCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder())))
//             , m_oneSubset(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * m_placement.m_funcCharge.size(),
//                       0.0, 0.0)))
//             , m_solver()
//             , m_cols() {
//             std::cout << "Building main problem..." << '\n';

//             // Set upper bound for nodes capacity
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 m_nodeCapaCons[u] = IloAdd(m_model, IloRange(m_placement.m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
//                 setIloName(m_nodeCapaCons[u], "m_nodeCapaCons" + toString(u));
//             }

//             std::cout << "Set upper bound for link capacity...\n";
//             int e = 0;
//             for (const Graph::Edge& edge : m_placement.m_inst.network.getEdges()) {
//                 IloNumVarArray vars(m_placement.m_env);
//                 IloNumArray vals(m_placement.m_env);
//                 for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     const Demand& demand = m_placement.m_inst.demands[i];
//                     const double d = std::get<2>(demand);
//                     const std::vector<function_descriptor>& funcs = std::get<3>(demand);
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         const int index = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                           + j * m_placement.m_inst.network.size()
//                                           + e;
//                         vars.add(m_g[index]);
//                         vals.add(d);
//                     }
//                 }
//                 m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edge));
//                 m_linkCapaCons[e].setLinearCoefs(vars, vals);
//                 ++e;
//             }

//             std::cout << "Build flow conservation constraints...\n";
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const Demand& demand = m_placement.m_inst.demands[i];
//                 const int s = std::get<0>(demand),
//                           t = std::get<1>(demand);
//                 const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//                 for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         IloNumVarArray vars(m_placement.m_env);
//                         IloNumArray vals(m_placement.m_env);
//                         for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                             const int index1 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                                + j * m_placement.m_inst.network.size()
//                                                + m_placement.m_arcToId.at(Graph::Edge(u, v));
//                             const int index2 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                                + j * m_placement.m_inst.network.size()
//                                                + m_placement.m_arcToId.at(Graph::Edge(v, u));

//                             vars.add(m_g[index1]);
//                             vals.add(1.0);
//                             vars.add(m_g[index2]);
//                             vals.add(-1.0);
//                         }
//                         m_flowConservationCons[getFlowConsIndex(i, j, u)].setLinearCoefs(vars, vals);
//                         setIloName(m_flowConservationCons[getFlowConsIndex(i, j, u)],
//                             "m_flowConservationCons" + toString(std::make_tuple(u, j, m_placement.m_inst.demands[i])));
//                     }
//                     m_flowConservationCons[getFlowConsIndex(i, 0, s)].setBounds(1.0, 1.0);
//                     m_flowConservationCons[getFlowConsIndex(i, funcs.size(), t)].setBounds(-1.0, -1.0);
//                 }
//             }

//             std::cout << "Objective function...\n";
//             IloNumVarArray vars(m_placement.m_env);
//             IloNumArray vals(m_placement.m_env);
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const Demand& demand = m_placement.m_inst.demands[i];
//                 const double d = std::get<2>(demand);
//                 const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.network.getEdges()) {
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         const int index = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                           + j * m_placement.m_inst.network.size()
//                                           + e;
//                         vars.add(m_g[index]);
//                         vals.add(d);
//                         setIloName(m_g[index], "g" + toString(std::make_tuple(edge, m_placement.m_inst.demands[i], j)));
//                     }
//                     ++e;
//                 }
//             }
//             m_obj.setLinearCoefs(vars, vals);

//             std::cout << "Set bounds oneSubset...\n"
//                       << std::endl;
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 for (int j = 0; j < int(std::get<3>(m_placement.m_inst.demands[i]).size()); ++j) {
//                     m_oneSubset[m_placement.m_funcCharge.size() * i + j].setBounds(1.0, 1.0);
//                     setIloName(m_oneSubset[m_placement.m_funcCharge.size() * i + j], "m_oneSubset" + toString(std::make_pair(i, j)));
//                 }
//             }

//             m_solver = IloCplex(m_model);
//             // m_solver.setParam(IloCplex::RootAlg, IloCplex::Network);
//         }

//         inline int getFlowConsIndex(int _i, int _j, int _u) const {
//             assert(_i >= 0 && _i < int(m_placement.m_inst.demands.size()));
//             assert(_j >= 0 && _j < int(m_placement.m_inst.network.size() + 1));
//             assert(_u >= 0 && _u < m_placement.m_inst.network.getOrder());
//             return _i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.getOrder())
//                    + _j * (m_placement.m_inst.network.getOrder())
//                    + _u;
//         }

//         MainProblem& operator=(const MainProblem&) = default;
//         MainProblem(const MainProblem&) = default;
//         MainProblem& operator=(MainProblem&&) = default;
//         MainProblem(MainProblem&&) = default;

//         void addColumnCheck(const Column& _col) {
//             if (std::find(m_cols.begin(), m_cols.end(), _col) == m_cols.end()) {
//                 addColumn(_col);
//             }
//         }

//         void addColumn(const Column& _col) {
//             assert(std::find(m_cols.begin(), m_cols.end(), _col) == m_cols.end()
//                    || (std::cout << _col << " is already present!" << '\n', false));

//             const auto& i = _col.first;
//             std::map<int, double> cons;

//             IloNumColumn col(m_placement.m_env);
//             for (const auto& pair_NodeFuncs : _col.second) {
//                 const auto& u = pair_NodeFuncs.first;
//                 assert(m_placement.m_isNFV[u]);
//                 double charge = 0;
//                 for (const auto& j : pair_NodeFuncs.second) {
//                     const auto& sameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                     const auto& nextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                     cons[sameLayer] = cons[sameLayer] + 1.0;
//                     cons[nextLayer] = cons[nextLayer] - 1.0;
//                     col += m_oneSubset[m_placement.m_funcCharge.size() * i + j](1.0);
//                     charge += m_placement.m_inst.nbCores(i, j);
//                 }
//                 col += m_nodeCapaCons[u](charge);
//             }

//             for (const auto& pair_ICoef : cons) {
//                 col += m_flowConservationCons[pair_ICoef.first](pair_ICoef.second);
//             }

//             IloNumVar aVar(col);
//             setIloName(aVar, "a" + toString(_col));
//             m_a.add(aVar);
//             m_cols.push_back(_col);
//         }

//         double getReducedCost(const IloCplex& _sol, const Column& _col) {
//             double reducedCost = 0.0;
//             const auto& i = _col.first;
//             for (const auto& pair_NodeFuncs : _col.second) {
//                 const auto& u = pair_NodeFuncs.first;
//                 for (const auto& j : pair_NodeFuncs.second) {
//                     const auto& sameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                     const auto& nextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                     reducedCost += -m_placement.m_inst.nbCores(i, j) * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[u])
//                                    - _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[sameLayer])
//                                    - _sol.getDual(m_placement.m_placementModel.m_oneSubset[m_placement.m_funcCharge.size() * i + j])
//                                    + _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[nextLayer]);
//                 }
//             }
//             return reducedCost;
//         }

//         void developReducedCost(const IloCplex& _sol, const Column& _col) {
//             const auto& i = _col.first;
//             for (const auto& pair_NodeFuncs : _col.second) {
//                 const auto& u = pair_NodeFuncs.first;
//                 for (const auto& j : pair_NodeFuncs.second) {
//                     const auto& sameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                     const auto& nextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                     std::cout << m_placement.m_inst.nbCores(i, j) * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[u]) << '\n'
//                               << -_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[sameLayer]) << '\n'
//                               << +_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[nextLayer]) << '\n'
//                               << -_sol.getDual(m_placement.m_placementModel.m_oneSubset[m_placement.m_funcCharge.size() * i + j]) << "\n\n";
//                 }
//             }
//         }

//         double solveInteger() {
//             if (m_solver.solve()) {
//                 auto intObj = m_solver.getObjValue();
//                 std::cout << "Final obj value: " << intObj << "\t# Paths: " << m_cols.size() << '\n';
//                 return intObj;
//             } else {
//                 std::cout << "No integer solution found!" << '\n';
//                 return -1;
//             }
//         }
//     };

//     class PricingProblem {
//         friend ChainingSubset3;

//       private:
//         const ChainingSubset3& m_placement;
//         const DemandID m_demandID;

//         IloModel m_model;

//         IloObjective m_obj;

//         IloNumVarArray m_x;

//         IloRangeArray m_capaCons;
//         IloRangeArray m_oneLocCons;

//       public:
//         PricingProblem(const ChainingSubset3& _chainPlacement, const DemandID& _demandID)
//             : m_placement(_chainPlacement)
//             , m_demandID(_demandID)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_x(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env, std::get<3>(m_placement.m_inst.demands[m_demandID]).size() * m_placement.m_inst.network.getOrder(),
//                       0.0, 1.0, ILOBOOL)))
//             , m_capaCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder())))
//             , m_oneLocCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, std::get<3>(m_placement.m_inst.demands[m_demandID]).size(),
//                       1.0, 1.0))) {
//             /*Set node capa constraints*/
//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 m_capaCons[u] = IloRange(m_placement.m_env, 0.0, m_placement.m_inst.nodeCapa[u]);
//                 setIloName(m_capaCons[u], "m_capaCons(" + std::to_string(u) + ')');
//             }

//             /*Set localization problem*/
//             const auto& funcs = std::get<3>(m_placement.m_inst.demands[m_demandID]);
//             for (int j = 0; j < int(funcs.size()); ++j) {
//                 setIloName(m_oneLocCons[j], "m_oneLocCons" + toString(j));
//             }

//             for (int j = 0; j < int(funcs.size()); ++j) {
//                 for (const auto& u : m_placement.m_inst.NFVNodes) {
//                     m_capaCons[u].setLinearCoef(m_x[getXIndex(u, j)], m_placement.m_inst.nbCores(m_demandID, j));
//                     m_oneLocCons[j].setLinearCoef(m_x[getXIndex(u, j)], 1.0);
//                     setIloName(m_x[getXIndex(u, j)], "x" + toString(std::make_tuple(j, u)));
//                 }
//             }
//         }

//         PricingProblem& operator=(const PricingProblem&) = default;
//         PricingProblem(const PricingProblem&) = default;
//         PricingProblem& operator=(PricingProblem&&) = default;
//         PricingProblem(PricingProblem&&) = default;

//         inline int getXIndex(const Graph::Node& _u, const int _j) const {
//             return _u * std::get<3>(m_placement.m_inst.demands[m_demandID]).size() + _j;
//         }

//         inline double getReducedCost(const Graph::Node _u, const int _j, const IloCplex& _sol) {
//             const int indexSameLayer = m_placement.m_placementModel.getFlowConsIndex(m_demandID, _j, _u);
//             const int indexNextLayer = m_placement.m_placementModel.getFlowConsIndex(m_demandID, _j + 1, _u);

// #if !defined(NDEBUG) && 0
//             std::cout << "m_x" << std::make_pair(m_demandID, _j) << " -> "
//                       << std::make_tuple(_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer]),
//                              _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]),
//                              -_sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[_u]))
//                       << '\n';
// #endif

//             double rc = -m_placement.m_inst.nbCores(m_demandID, _j) * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[_u])
//                         - _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer])
//                         - _sol.getDual(m_placement.m_placementModel.m_oneSubset[m_placement.m_funcCharge.size() * m_demandID + _j])
//                         + _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]);
//             return rc;
//         }

//         /* Really shitty code */
//         void updateDual(const IloCplex& _sol) {
//             const auto& funcs = std::get<3>(m_placement.m_inst.demands[m_demandID]);

//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     m_obj.setLinearCoef(m_x[getXIndex(u, j)], getReducedCost(u, j, _sol));
//                 }
//             }

// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//             std::cout << m_model << '\n';
// #endif
//         }

//         ChainingSubset3::Column getCol(const IloCplex& _sol) const {
//             ChainingSubset3::Column col;
//             col.first = m_demandID;

//             for (const auto& u : m_placement.m_inst.NFVNodes) {
//                 std::vector<int> installedOnU;
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[m_demandID]);
//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     if (_sol.getValue(m_x[getXIndex(u, j)])) {
//                         installedOnU.emplace_back(j);
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//                         std::cout << m_x[getXIndex(u, j)] << '\n';
// #endif
//                     }
//                 }
//                 if (!installedOnU.empty()) {
//                     col.second.emplace_back(u, installedOnU);
//                 }
//             }
//             return col;
//         }
//     };

//   public:
//     ChainingSubset3(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge, const std::vector<int>& _NFVNodes)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_funcCharge(_funcCharge)
//         , m_inst.nodeCapa(_nodeCapa)
//         ,

//         m_inst.maxChainSize(std::get<3>(*std::max_element(m_inst.demands.begin(), m_inst.demands.end(),
//                                        [&](const Demand& _d1, const Demand& _d2) {
//                                            return std::get<3>(_d1) > std::get<3>(_d2);
//                                        }))
//                            .size())
//         , m_inst.NFVNodes(_NFVNodes)
//         , m_isNFV([&]() {
//             std::vector<int> isNFV(m_inst.network.getOrder(), 0);
//             for (const auto& u : m_inst.NFVNodes) {
//                 isNFV[u] = 1;
//             }
//             return isNFV;
//         }())
//         , m_inst.nbCores([&]() {
//             Matrix<double> mat(m_inst.demands.size(), m_funcCharge.size(), -1);

//             for (int i(0); i < int(m_inst.demands.size()); ++i) {
//                 const auto& funcs = std::get<3>(m_inst.demands[i]);

//                 for (int j(0); j < int(funcs.size()); ++j) {
//                     mat(i, j) = ceil(std::get<2>(m_inst.demands[i]) / m_funcCharge[funcs[j]]);
//                 }
//             }
//             return mat;
//         }())
//         , m_intObj(-1)
//         , m_fractObj(-1)
//         , m_arcToId([&]() {
//             std::map<Graph::Edge, int> arcToId;
//             int i = 0;
//             for (const auto& arc : m_inst.network.getEdges()) {
//                 arcToId[arc] = i;
//                 i++;
//             }
//             return arcToId;
//         }())
//         , m_placementModel(*this)
//         , m_pp() {
//         m_pp.reserve(m_inst.demands.size());
//         for (DemandID i = 0; i < int(m_inst.demands.size()); ++i) {
//             m_pp.emplace_back(*this, i);
//         }
//     }

//     void solve() {
//         m_placementModel.m_solver.setOut(m_env.getNullStream());
//         std::vector<Column> colsToAdd;
//         do {
//             if (m_placementModel.m_solver.solve()) {
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//                 std::cout << m_placementModel.m_model << '\n';
// #endif
//                 std::cout << "Reduced Main Problem Objective Value = " << m_placementModel.m_solver.getObjValue() << '\n';
//                 m_fractObj = m_placementModel.m_solver.getObjValue();
// // Report flow used

// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//                 for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//                     std::cout << m_inst.demands[i] << '\n';
//                     const std::vector<function_descriptor>& funcs = std::get<3>(m_inst.demands[i]);
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         for (int e = 0; e < m_inst.network.size(); ++e) {
//                             // g_{fuv}^{st}
//                             const int index = i * ((m_funcCharge.size() + 1) * m_inst.network.size())
//                                               + j * m_inst.network.size()
//                                               + e;

//                             if (m_placementModel.m_solver.getValue(m_placementModel.m_g[index])) {
//                                 std::cout << '\t' << m_placementModel.m_g[index] << " -> " << m_placementModel.m_solver.getValue(m_placementModel.m_g[index]) << '\n';
//                             }
//                         }
//                     }
//                 }

//                 for (int i = 0; i < m_placementModel.m_a.getSize(); ++i) {
//                     if (m_placementModel.m_solver.getValue(m_placementModel.m_a[i])) {
//                         std::cout << m_placementModel.m_a[i] << " -> " << m_placementModel.m_solver.getValue(m_placementModel.m_a[i]) << '\n';
//                     }
//                 }

// #endif

// #if !(defined(NDEBUG))
//                 for (int i = 0; i < m_placementModel.m_a.getSize(); ++i) {
//                     const auto rc = m_placementModel.getReducedCost(m_placementModel.m_solver, m_placementModel.m_cols[i]);
//                     assert(std::abs(rc - m_placementModel.m_solver.getReducedCost(m_placementModel.m_a[i])) < RC_EPS || (std::cout << m_placementModel.m_cols[i] << " -> " << rc << " vs. " << m_placementModel.m_solver.getReducedCost(m_placementModel.m_a[i]) << '\n', m_placementModel.developReducedCost(m_placementModel.m_solver, m_placementModel.m_cols[i]), false));
//                 }
// #endif

//                 colsToAdd.clear();
//                 searchColumn(colsToAdd);
//                 for (auto& p : colsToAdd) {
//                     m_placementModel.addColumn(p);
//                 }
//             } else {
//                 std::cout << "No solution for reduced main problem!" << '\n';
//                 m_placementModel.m_solver.exportModel("rm.lp");
//             }
//         } while (!colsToAdd.empty());

//         m_intObj = m_placementModel.solveInteger();
//     }

//     void addInitConf(const std::vector<Column> _col) {
//         for (const auto& col : _col) {
//             m_placementModel.addColumn(col);
//         }
//     }

//     void save(const std::string& _filename, const std::pair<double, double>& _time) {
//         std::ofstream ofs(_filename);
//         ofs << m_intObj << '\t' << m_fractObj << '\n';
//         ofs << m_placementModel.m_cols.size() << std::endl;
//         ofs << _time.first << '\t' << _time.second << std::endl;

//         std::vector<double> linkUsage(m_inst.network.size(), 0);
//         std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
//         std::vector<IloNumVar> pathsUsed(m_inst.demands.size(), nullptr);

//         // Get link usage
//         for (int i(0); i < int(m_inst.demands.size()); ++i) {
//             for (int j(0); j < m_inst.maxChainSize + 1; ++j) {
//                 for (int e(0); e < int(m_inst.network.size()); ++e) {
//                     const int index = i * ((m_funcCharge.size() + 1) * m_inst.network.size())
//                                       + j * m_inst.network.size()
//                                       + e;
//                     if (m_placementModel.m_solver.getValue(m_placementModel.m_g[index])) {
//                         linkUsage[e] += std::get<2>(m_inst.demands[i]);
//                     }
//                 }
//             }
//         }

//         // Get node usage
//         for (int k(0); k < int(m_placementModel.m_cols.size()); ++k) {
//             const auto& col = m_placementModel.m_cols[k];
//             if (m_placementModel.m_solver.getValue(m_placementModel.m_a[k])) {
//                 const auto& i = col.first;
//                 int j = 0;
//                 for (const auto& pair_NodeFuncs : col.second) {
//                     for (const int nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
//                         nodeUsage[pair_NodeFuncs.first] += m_inst.nbCores(i, j);
//                     }
//                 }
//             }
//         }

//         for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
//             ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
//         }
//         int i = 0;
//         for (const auto& edge : m_inst.network.getEdges()) {
//             ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge) << '\n';
//             ++i;
//         }
//     }

//     void searchColumn(std::vector<Column>& _cols, const double _threshold = -RC_EPS) {
//         /* For each demands, search for a shortest path in the augmented graph */
//         for (DemandID i = 0; i < int(m_inst.demands.size()); ++i) {
//             m_pp[i].updateDual(m_placementModel.m_solver);

//             IloCplex ppSolver(m_pp[i].m_model);
//             ppSolver.setOut(m_env.getNullStream());
//             if (ppSolver.solve()) {
//                 const double reducedCost = ppSolver.getObjValue();
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//                 std::cout << "Solved PP " << reducedCost << '\n';
// #endif
//                 if (reducedCost <= _threshold) {
//                     _cols.push_back(m_pp[i].getCol(ppSolver));
//                 }
//             } else {
//                 std::cout << "No solution PP \n";
//                 ppSolver.exportModel("pp.lp");
//             }
//             ppSolver.end();
//         }
//     }

//   private:
//     const IloEnv m_env { };
//     const DiGraph m_inst.network;
//     const std::vector<Demand> m_inst.demands;
//     const std::vector<double> m_funcCharge;
//     const std::vector<int> m_inst.nodeCapa;

//     const int m_inst.maxChainSize;
//     const std::vector<int> m_inst.NFVNodes;
//     const std::vector<int> m_isNFV;
//     const Matrix<double> m_inst.nbCores;
//     double m_intObj;
//     double m_fractObj;

//     const std::map<Graph::Edge, int> m_arcToId;

//     MainProblem m_placementModel;
//     std::vector<PricingProblem> m_pp;

//     friend MainProblem;
// };

// #endif