// #ifndef CHAINING_SUBSET_HPP
// #define CHAINING_SUBSET_HPP

// #include <cmath>
// #include <ilcplex/ilocplex.h>
// #include <iostream>
// #include <map>
// #include <set>
// #include <tuple>

// #include "DiGraph.hpp"
// #include "SFC.hpp"
// #include "ShortestPath.hpp"
// #include "cplex_utility.hpp"
// #include "utility.hpp"

// #define RC_EPS 1.0e-6

// class ChainingSubset {
//   public:
//     typedef std::pair<std::vector<std::pair<int, int>>, Graph::Node> Column;

//   private:
//     class MainProblem {
//         friend ChainingSubset;

//       private:
//         const ChainingSubset& m_placement;
//         IloModel m_model;

//         IloObjective m_obj;

//         // Variables
//         IloNumVarArray m_g;
//         IloNumVarArray m_a;

//         // Constraints
//         IloRangeArray m_flowConservationCons;
//         IloRangeArray m_linkCapaCons;
//         // IloRangeArray m_maxIn;
//         // IloRangeArray m_maxOut;
//         IloRangeArray m_nodeCapaCons;
//         IloRangeArray m_oneSubset;

//         IloCplex m_solver;

//         std::vector<Column> m_cols;

//       public:
//         MainProblem(ChainingSubset& _placement)
//             : m_placement(_placement)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_g(
//                   IloNumVarArray(m_placement.m_env, m_placement.m_inst.demands.size() * (1 + m_placement.m_funcCharge.size()) * m_placement.m_inst.network.size(),
//                       0.0, 1.0))
//             , m_a(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env)))
//             , m_flowConservationCons(
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * (1 + m_placement.m_funcCharge.size()) * m_placement.m_inst.network.getOrder(),
//                       0.0, 0.0))
//             , m_linkCapaCons(
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.size(),
//                       -IloInfinity, 0.0))
//             , m_nodeCapaCons(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.network.getOrder(),
//                       -IloInfinity, 0.0)))
//             , m_oneSubset(IloAdd(m_model,
//                   IloRangeArray(m_placement.m_env, m_placement.m_inst.demands.size() * m_placement.m_funcCharge.size(),
//                       0.0, 0.0)))
//             , m_solver()
//             , m_cols() {
//             std::cout << "Building main problem..." << '\n';

//             // Set upper bound for nodes capacity
//             for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                 m_nodeCapaCons[u].setUB(m_placement.m_inst.nodeCapa[u]);
//                 setIloName(m_nodeCapaCons[u], "m_nodeCapaCons" + toString(u));
//             }

//             std::cout << "Set upper bound for link capacity...\n";
//             int e = 0;
//             for (const Graph::Edge& edge : m_placement.m_inst.network.getEdges()) {
//                 IloNumVarArray vars(m_placement.m_env);
//                 IloNumArray vals(m_placement.m_env);
//                 for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                     const Demand& demand = m_placement.m_inst.demands[i];
//                     const double d = std::get<2>(demand);
//                     const std::vector<function_descriptor>& funcs = std::get<3>(demand);
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         const int index = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                           + j * m_placement.m_inst.network.size()
//                                           + e;
//                         vars.add(m_g[index]);
//                         vals.add(d);
//                     }
//                 }
//                 m_linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edge));
//                 m_linkCapaCons[e].setLinearCoefs(vars, vals);
//                 m_model.add(m_linkCapaCons[e]);
//                 ++e;
//             }

//             std::cout << "Build flow conservation constraints...\n";
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const Demand& demand = m_placement.m_inst.demands[i];
//                 const int s = std::get<0>(demand),
//                           t = std::get<1>(demand);
//                 const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//                 for (Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         IloNumVarArray vars(m_placement.m_env);
//                         IloNumArray vals(m_placement.m_env);
//                         for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                             const int index1 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                                + j * m_placement.m_inst.network.size()
//                                                + m_placement.m_arcToId.at(Graph::Edge(u, v));
//                             const int index2 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                                + j * m_placement.m_inst.network.size()
//                                                + m_placement.m_arcToId.at(Graph::Edge(v, u));

//                             vars.add(m_g[index1]);
//                             vals.add(1.0);
//                             vars.add(m_g[index2]);
//                             vals.add(-1.0);
//                         }
//                         m_flowConservationCons[getFlowConsIndex(i, j, u)].setLinearCoefs(vars, vals);
//                         setIloName(m_flowConservationCons[getFlowConsIndex(i, j, u)],
//                             "m_flowConservationCons" + toString(std::make_tuple(u, j, m_placement.m_inst.demands[i])));
//                         m_model.add(m_flowConservationCons[getFlowConsIndex(i, j, u)]);
//                     }
//                     m_flowConservationCons[getFlowConsIndex(i, 0, s)].setBounds(1.0, 1.0);
//                     m_flowConservationCons[getFlowConsIndex(i, funcs.size(), t)].setBounds(-1.0, -1.0);
//                 }
//             }

//             std::cout << "Objective function...\n";
//             IloNumVarArray vars(m_placement.m_env);
//             IloNumArray vals(m_placement.m_env);
//             for (DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const Demand& demand = m_placement.m_inst.demands[i];
//                 const double d = std::get<2>(demand);
//                 const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.network.getEdges()) {
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         const int index = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//                                           + j * m_placement.m_inst.network.size()
//                                           + e;
//                         vars.add(m_g[index]);
//                         vals.add(d);
//                         setIloName(m_g[index], "g" + toString(std::make_tuple(edge, m_placement.m_inst.demands[i], j)));
//                     }
//                     ++e;
//                 }
//             }
//             m_obj.setLinearCoefs(vars, vals);

//             // std::cout << "Max In/out...\n";
//             // for(DemandID i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//             // 	const Demand& demand = m_placement.m_inst.demands[i];
//             // 	const int s = std::get<0>(demand),
//             // 		t = std::get<1>(demand);
//             // 	const std::vector<function_descriptor>& funcs = std::get<3>(demand);

//             // 	for(Graph::Node u = 0; u < m_placement.m_inst.network.getOrder(); ++u) {
//             // 		IloNumVarArray varsOut(m_placement.m_env);
//             // 		IloNumVarArray varsIn(m_placement.m_env);
//             // 		IloNumArray vals(m_placement.m_env);

//             // 		for(int j = 0; j <= int(funcs.size()); ++j) {
//             // 			for(const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//             // 				const int index1 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//             // 						+ j * m_placement.m_inst.network.size()
//             // 						+ m_placement.m_arcToId.at(Graph::Edge(u, v));
//             // 				const int index2 = i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.size())
//             // 						+ j * m_placement.m_inst.network.size()
//             // 						+ m_placement.m_arcToId.at(Graph::Edge(v, u));

//             // 				varsOut.add(m_g[index1]);
//             // 				varsIn.add(m_g[index2]);
//             // 				vals.add(1.0);
//             // 			}
//             // 		}
//             // m_maxIn[m_placement.m_inst.network.getOrder() * i + u].setLinearCoefs(varsIn, vals);
//             // setIloName(m_maxIn[m_placement.m_inst.network.getOrder() * i + u],
//             // "m_maxIn" + toString(std::make_pair(u, m_placement.m_inst.demands[i])));
//             // m_model.add(m_maxIn[m_placement.m_inst.network.getOrder() * i + u]);

//             // m_maxOut[m_placement.m_inst.network.getOrder() * i + u].setLinearCoefs(varsOut, vals);
//             // setIloName(m_maxOut[m_placement.m_inst.network.getOrder() * i + u],
//             // "m_maxOut" + toString(std::make_pair(u, m_placement.m_inst.demands[i])));
//             // m_model.add(m_maxOut[m_placement.m_inst.network.getOrder() * i + u]);

//             // m_flowConservationCons[getFlowConsIndex(i, 0, s)].setBounds(1.0, 1.0);
//             // m_flowConservationCons[getFlowConsIndex(i, funcs.size(), t)].setBounds(-1.0, -1.0);
//             // }
//             // }

//             std::cout << "Set bounds oneSubset...\n"
//                       << std::endl;
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 for (int j = 0; j < int(std::get<3>(m_placement.m_inst.demands[i]).size()); ++j) {
//                     m_oneSubset[m_placement.m_funcCharge.size() * i + j].setBounds(1.0, 1.0);
//                     setIloName(m_oneSubset[m_placement.m_funcCharge.size() * i + j], "m_oneSubset" + toString(std::make_pair(i, j)));
//                 }
//             }

//             m_solver = IloCplex(m_model);
//             // m_solver.setParam(IloCplex::RootAlg, IloCplex::Network);
//         }

//         inline int getFlowConsIndex(int _i, int _j, int _u) const {
//             assert(_i >= 0 && _i < int(m_placement.m_inst.demands.size()));
//             assert(_j >= 0 && _j < int(m_placement.m_inst.network.size() + 1));
//             assert(_u >= 0 && _u < m_placement.m_inst.network.getOrder());
//             return _i * ((m_placement.m_funcCharge.size() + 1) * m_placement.m_inst.network.getOrder())
//                    + _j * (m_placement.m_inst.network.getOrder())
//                    + _u;
//         }

//         MainProblem& operator=(const MainProblem&) = default;
//         MainProblem(const MainProblem&) = default;
//         MainProblem& operator=(MainProblem&&) = default;
//         MainProblem(MainProblem&&) = default;

//         void addColumnCheck(const Column& _col) {
//             if (std::find(m_cols.begin(), m_cols.end(), _col) == m_cols.end()) {
//                 addColumn(_col);
//             }
//         }

//         void addColumn(const Column& _col) {
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//             std::cout << "addColumn " << _col.first.size() << '\n';
// #endif
//             assert(std::find(m_cols.begin(), m_cols.end(), _col) == m_cols.end()
//                    || (std::cout << _col << " is already present!" << '\n', false));

//             const Graph::Node u = _col.second;

//             IloNumColumn col(m_placement.m_env);
//             int charge = 0;
//             std::map<int, double> cons;
//             for (const auto& pair_IJ : _col.first) {
//                 const int i = pair_IJ.first, // demand id
//                     j = pair_IJ.second;      // index of function
//                 const double d = std::get<2>(m_placement.m_inst.demands[i]);
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);

//                 charge += ceil(d / m_placement.m_funcCharge[funcs[j]]);
//                 const int sameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                 const int nextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                 cons[sameLayer] = cons[sameLayer] + 1.0;
//                 cons[nextLayer] = cons[nextLayer] - 1.0;
//                 col += m_oneSubset[m_placement.m_funcCharge.size() * i + j](1.0);
//             }

//             for (const auto& pair_ICoef : cons) {
//                 col += m_flowConservationCons[pair_ICoef.first](pair_ICoef.second);
//             }

//             assert(charge <= m_placement.m_inst.nodeCapa[u]
//                    || (std::cout << charge << ">" << m_placement.m_inst.nodeCapa[u] << '\n', false));
//             col += m_nodeCapaCons[u](charge);

//             IloNumVar aVar(col);
//             setIloName(aVar, "a" + toString(_col));
//             m_a.add(aVar);
//             m_cols.push_back(_col);
//         }

//         double getReducedCost(const IloCplex& _sol, const Column& _col) {
//             const Graph::Node u = _col.second;
//             double reducedCost = 0.0;
//             for (const auto& pair_IJ : _col.first) {
//                 const auto i = pair_IJ.first;
//                 const auto j = pair_IJ.second;
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                 const double d = std::get<2>(m_placement.m_inst.demands[i]);

//                 const double charge = ceil(d / m_placement.m_funcCharge[funcs[j]]);
//                 const int indexSameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                 const int indexNextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                 reducedCost += -charge * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[u])
//                                - _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer])
//                                + _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer])
//                                - _sol.getDual(m_placement.m_placementModel.m_oneSubset[m_placement.m_funcCharge.size() * i + j]);
//             }
//             return reducedCost;
//         }

//         void developRC(const IloCplex& _sol, const Column& _col) {
//             const Graph::Node u = _col.second;
//             for (const auto& pair_IJ : _col.first) {
//                 const auto i = pair_IJ.first;
//                 const auto j = pair_IJ.second;
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                 const double d = std::get<2>(m_placement.m_inst.demands[i]);

//                 const double charge = ceil(d / m_placement.m_funcCharge[funcs[j]]);
//                 const int indexSameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, u);
//                 const int indexNextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, u);

//                 std::cout << 'c' << std::make_tuple(u, i, j) << " -> " << std::make_tuple(
//                                                                               /*std::make_pair(*/ -charge * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[u]) /*,
// 											_sol.getSlack( m_placement.m_placementModel.m_nodeCapaCons[u]) )*/
//                                                                               ,
//                                                                               /*std::make_pair(*/ -_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer]) /*,
// 											_sol.getSlack( m_placement.m_placementModel.m_flowConservationCons[indexSameLayer]) )*/
//                                                                               ,
//                                                                               /*std::make_pair(*/ _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]) /*,
// 											_sol.getSlack( m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]) )*/
//                                                                               ,
//                                                                               -_sol.getDual(m_placement.m_placementModel.m_oneSubset[m_placement.m_funcCharge.size() * i + j]))
//                           << '\t';
//             }
//         }

//         void solveInteger() {
//             if (m_solver.solve()) {
//                 std::cout << "Final obj value: " << m_solver.getObjValue() << "\t# Paths: " << m_cols.size() << '\n';
//             } else {
//                 std::cout << "No integer solution found!" << '\n';
//             }
//         }
//     };

//     class PricingProblem {
//         friend ChainingSubset;

//       private:
//         const ChainingSubset& m_placement;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloNumVarArray m_x;

//         IloRange m_capaCons;

//         Graph::Node m_u;

//       public:
//         PricingProblem(const ChainingSubset& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , m_model(m_placement.m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_placement.m_env)))
//             , m_x(IloAdd(m_model,
//                   IloNumVarArray(m_placement.m_env, m_placement.m_funcCharge.size() * m_placement.m_inst.demands.size(), 0.0, 1.0, ILOBOOL)))
//             , m_capaCons(IloAdd(m_model,
//                   IloRange(m_placement.m_env, -IloInfinity, 0.0)))
//             , m_u(-1) {
//             IloNumVarArray vars(m_placement.m_env);
//             IloNumArray vals(m_placement.m_env);
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const double d = std::get<2>(m_placement.m_inst.demands[i]);
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);

//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     vars.add(m_x[i * m_placement.m_funcCharge.size() + j]);
//                     vals.add(ceil(d / m_placement.m_funcCharge[funcs[j]]));
//                     setIloName(m_x[i * m_placement.m_funcCharge.size() + j], "x" + toString(std::make_pair(i, j)));
//                 }
//             }
//             m_capaCons.setLinearCoefs(vars, vals);
//         }

//         double getReducedCost(const int i, const int j, const IloCplex& _sol) {
//             const int indexSameLayer = m_placement.m_placementModel.getFlowConsIndex(i, j, m_u);
//             const int indexNextLayer = m_placement.m_placementModel.getFlowConsIndex(i, j + 1, m_u);
// #if !defined(NDEBUG) && 0
//             std::cout << "m_x" << std::make_pair(i, j) << " -> "
//                       << std::make_tuple(_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer]),
//                              _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer]),
//                              -_sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[m_u]))
//                       << '\n';
// #endif

//             const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//             const double d = std::get<2>(m_placement.m_inst.demands[i]);
//             std::cout << 'm' << std::make_tuple(m_u, i, j) << " -> "
//                       << std::make_tuple(-ceil(d / m_placement.m_funcCharge[funcs[j]]) * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[m_u]), -_sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer]), _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer])) << '\n';

//             return -ceil(d / m_placement.m_funcCharge[funcs[j]]) * _sol.getDual(m_placement.m_placementModel.m_nodeCapaCons[m_u])
//                    - _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexSameLayer])
//                    + _sol.getDual(m_placement.m_placementModel.m_flowConservationCons[indexNextLayer])
//                    - _sol.getDual(m_placement.m_placementModel.m_oneSubset[m_placement.m_funcCharge.size() * i + j]);
//         }

//         void updateDual(const Graph::Node _u, const IloCplex& _sol) {
//             assert(_u >= 0);
//             assert(_u < m_placement.m_inst.network.getOrder());

//             m_u = _u;
//             m_capaCons.setUB(m_placement.m_inst.nodeCapa[m_u]);
//             setIloName(m_capaCons, "m_capaCons" + std::to_string(m_u));

//             IloNumVarArray vars(m_placement.m_env);
//             IloNumArray vals(m_placement.m_env);
//             // For each demands
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 // For each function
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     vars.add(m_x[i * m_placement.m_funcCharge.size() + j]);
//                     vals.add(getReducedCost(i, j, _sol));
//                 }
//             }
//             m_obj.setLinearCoefs(vars, vals);

// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
//             std::cout << m_model << '\n';
// #endif
//         }

//         ChainingSubset::Column getCol(const IloCplex& _sol) const {
//             ChainingSubset::Column col;
//             col.second = m_u;
//             int charge = 0;
//             for (int i = 0; i < int(m_placement.m_inst.demands.size()); ++i) {
//                 const auto& funcs = std::get<3>(m_placement.m_inst.demands[i]);
//                 for (int j = 0; j < int(funcs.size()); ++j) {
//                     if (_sol.getValue(m_x[i * m_placement.m_funcCharge.size() + j])) {
//                         col.first.emplace_back(i, j);
//                         charge += ceil(std::get<2>(m_placement.m_inst.demands[i]) / m_placement.m_funcCharge[funcs[j]]);
//                     }
//                 }
//             }
//             assert(charge <= m_placement.m_inst.nodeCapa[m_u]
//                    || (std::cout << charge << ">" << m_placement.m_inst.nodeCapa[m_u] << '\n', _sol.exportModel("pp.lp"), false));
//             return col;
//         }
//     };

//   public:
//     ChainingSubset(const DiGraph& _network, const std::vector<int> _nodeCapa, const std::vector<Demand>& _demands, const std::vector<double>& _funcCharge)
//         : m_env()
//         , m_inst.network(_network)
//         , m_inst.demands(_demands)
//         , m_funcCharge(_funcCharge)
//         , m_inst.nodeCapa(_nodeCapa)
//         , m_arcToId(buildArcToId())
//         , m_placementModel(*this)
//         , m_pp(*this) {
//     }

//     std::map<Graph::Edge, int> buildArcToId() const {
//         std::map<Graph::Edge, int> arcToId;
//         int i = 0;
//         for (const auto& arc : m_inst.network.getEdges()) {
//             arcToId[arc] = i;
//             i++;
//         }
//         return arcToId;
//     }

//     ~ChainingSubset() {
//     }

//     ChainingSubset(const ChainingSubset&) = delete;
//     ChainingSubset& operator=(const ChainingSubset&) = delete;

//     void solve() {
//         m_placementModel.m_solver.setOut(m_env.getNullStream());
//         std::vector<Column> colsToAdd;
//         do {
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//             std::cout << m_placementModel.m_model << '\n';
// #endif
//             if (m_placementModel.m_solver.solve()) {

//                 std::cout << "Reduced Main Problem Objective Value = " << m_placementModel.m_solver.getObjValue() << '\n';
// // Report flow used

// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1
//                 for (int i = 0; i < int(m_inst.demands.size()); ++i) {
//                     std::cout << m_inst.demands[i] << '\n';
//                     const std::vector<function_descriptor>& funcs = std::get<3>(m_inst.demands[i]);
//                     for (int j = 0; j <= int(funcs.size()); ++j) {
//                         for (int e = 0; e < m_inst.network.size(); ++e) {
//                             // g_{fuv}^{st}
//                             const int index = i * ((m_funcCharge.size() + 1) * m_inst.network.size())
//                                               + j * m_inst.network.size()
//                                               + e;

//                             if (m_placementModel.m_solver.getValue(m_placementModel.m_g[index])) {
//                                 std::cout << '\t' << m_placementModel.m_g[index] << '\n';
//                             }
//                         }
//                     }
//                 }

//                 for (int i = 0; i < m_placementModel.m_a.getSize(); ++i) {
//                     if (m_placementModel.m_solver.getValue(m_placementModel.m_a[i])) {
//                         std::cout << m_placementModel.m_a[i] << '\n';
//                     }
//                 }

//                 for (int i(0), sz(m_placementModel.m_a.getSize()); i < sz; ++i) {
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 0
//                     m_placementModel.developRC(m_placementModel.m_solver, m_placementModel.m_cols[i]);
// #endif
//                     assert(std::abs(m_placementModel.getReducedCost(m_placementModel.m_solver, m_placementModel.m_cols[i]) - m_placementModel.m_solver.getReducedCost(m_placementModel.m_a[i])) < 1.0e-6 || (std::cout << m_placementModel.getReducedCost(m_placementModel.m_solver, m_placementModel.m_cols[i]) << " vs. " << m_placementModel.m_solver.getReducedCost(m_placementModel.m_a[i]) << '\n', m_placementModel.developRC(m_placementModel.m_solver, m_placementModel.m_cols[i]), false));
//                 }
// #endif

//                 colsToAdd.clear();
//                 searchColumn(colsToAdd);
//                 for (auto& p : colsToAdd) {
//                     m_placementModel.addColumn(p);
//                 }
//             } else {
//                 std::cout << "No solution for reduced main problem!" << '\n';
//                 m_placementModel.m_solver.exportModel("rm.lp");
//             }
//         } while (!colsToAdd.empty());

//         m_placementModel.solveInteger();
//     }

//     void addInitConf(const std::vector<Column>& _columns) {
//         for (auto& col : _columns) {
//             if (!col.first.empty()) {
//                 m_placementModel.addColumn(col);
//             }
//         }
//         // std::cout << m_placementModel.m_model << '\n';
//     }

//     void searchColumn(std::vector<Column>& _cols, const double _threshold = -RC_EPS) {
//         /* For each demands, search for a shortest path in the augmented graph */
//         for (Node u = 0; u < m_inst.network.getOrder(); ++u) {
//             m_pp.updateDual(u, m_placementModel.m_solver);

//             IloCplex ppSolver(m_pp.m_model);
//             ppSolver.setOut(m_env.getNullStream());
//             if (ppSolver.solve()) {
//                 const double reducedCost = ppSolver.getObjValue();
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//                 std::cout << "Solved PP for " << u << " -> " << reducedCost;
// #else
//                 std::cout << '\r' << "Solved PP for " << u << " -> " << reducedCost << std::flush;
// #endif
//                 if (reducedCost <= _threshold) {
//                     _cols.push_back(m_pp.getCol(ppSolver));
//                 }
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//                 std::cout << '\n';
// #endif
//             }
// #if !defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 2
//             else {
//                 std::cout << "No solution PP for " << u << '\n';
//             }
// #endif
//             ppSolver.end();
//         }
//     }

//   private:
//     const IloEnv m_env { };
//     const DiGraph m_inst.network;
//     const std::vector<Demand> m_inst.demands;
//     const std::vector<double> m_funcCharge;
//     const std::vector<int> m_inst.nodeCapa;

//     const std::map<Graph::Edge, int> m_arcToId;

//     MainProblem m_placementModel;
//     PricingProblem m_pp;

//     friend MainProblem;
// };

// #endif