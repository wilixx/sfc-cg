cmake_minimum_required(VERSION 3.3)
project (SFC::Placement LANGUAGES CXX)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake ${PROJECT_SOURCE_DIR}/externals/sanitizers-cmake/cmake)

find_package(Sanitizers)

# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fpermissive -Wextra -Werror -Wshadow -Wnon-virtual-dtor -pedantic -Wno-c++11-compat -DIL_STD -march=native -Wno-c++98-compat -Wno-sign-compare")
# if($<CXX_COMPILER_ID:Clang>)
#     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Weveything -stdlib=libc++ -I/usr/local/opt/llvm/include -I/usr/local/opt/llvm/include/c++/v1/")
# endif()
# set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -fsanitize=${SANITIZERS} -fno-omit-frame-pointer")
# set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG -DBOOST_UBLAS_NDEBUG")
# set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-g -Og")
# set(CMAKE_INCLUDE_SYSTEM_FLAG_CXX "-isystem ")
# set(CMAKE_EXE_LINKER_FLAGS "-fsanitize=${SANITIZERS}")

#Boost
# find_package(Boost REQUIRED)
# if(Boost_FOUND)
#     include_directories(SYSTEM ${Boost_INCLUDE_DIRS})
#     message(STATUS "Boost found in: " ${Boost_INCLUDE_DIRS})
# endif()

add_executable(LP_OCC ${PROJECT_SOURCE_DIR}/main-occ_limit.cpp)
target_include_directories(LP_OCC PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_sources(LP_OCC
    PUBLIC 
    ${CMAKE_CURRENT_SOURCE_DIR}/src/ChainingThreadOccLimit.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/ChainingPathOccLimitDoublePP.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/OccLim/Placement_DWD_Alpha.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/OccLim/Placement_DWD.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/OccLim/Placement_EM.cpp)
target_compile_options(LP_OCC
    PRIVATE
    -Werror
    -Wall
    -fpermissive
    -Wextra
    -Wshadow
    -Wnon-virtual-dtor
    -pedantic
    -Wno-c++11-compat
    -DIL_STD
    -march=native
    -Wno-c++98-compat
    -Wno-sign-compare
    -Weverything
    -Wno-sign-compare
    -Wno-sign-conversion
    -Wno-c++98-compat-pedantic
    -Wno-error=padded
    -Wno-error=unused-variable
    -Wno-shorten-64-to-32
    -std=libc++
    -Wno-padded)

target_link_libraries(LP_OCC
    CppRo::CppRo CppRo::TCLAP
    SFC
    m pthread dl cplex-library cplex-concert ilocplex LLVM::llvm)


add_executable(LP ${PROJECT_SOURCE_DIR}/main.cpp)
target_include_directories(LP PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_sources(LP 
    PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/src/ChainingThreadBF.cpp)
target_compile_features(LP PUBLIC cxx_std_17)
target_link_libraries(LP CppRo::TCLAP SFC m pthread dl cplex-library cplex-concert ilocplex)
target_compile_options(LP
    PRIVATE
    -Werror
    -Wall
    -fpermissive
    -Wextra
    -Wshadow
    -Wnon-virtual-dtor
    -pedantic
    -Wno-c++11-compat
    -DIL_STD
    -march=native
    -Wno-c++98-compat
    -Wno-sign-compare
    -Weverything
    -Wno-sign-compare
    -Wno-sign-conversion
    -Wno-c++98-compat-pedantic
    -Wno-error=padded
    -Wno-error=unused-variable
    -Wno-shorten-64-to-32
    -std=libc++
    -Wno-padded)
IF(CMAKE_BUILD_TYPE MATCHES DEBUG)
    add_sanitizers(LP)
    add_sanitizers(LP_OCC)
ENDIF(CMAKE_BUILD_TYPE MATCHES DEBUG)



# add_custom_target(makeSimpleSFC 
#     COMMAND python -c "import launchEmb; launchEmb.genAll(['internet2', 'atlanta', 'germany50', 'ta2'], 12)" | parallel -j1 --bar --joblog exec_LP.log --resume-failed
#     DEPENDS LP
#     WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin/
#     VERBATIM)

install(TARGETS LP
        RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
install(TARGETS LP_OCC
        RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

# install(DIRECTORY instances/ DESTINATION ${CMAKE_BINARY_DIR}/bin/instances)
# install(DIRECTORY script/ DESTINATION ${CMAKE_BINARY_DIR}/bin)
# install(DIRECTORY script/ DESTINATION ${CMAKE_BINARY_DIR}/bin/)
# install(DIRECTORY DESTINATION ${CMAKE_BINARY_DIR}/bin/results/occlim)
# install(DIRECTORY DESTINATION ${CMAKE_BINARY_DIR}/bin/figures/occlim)
# install(DIRECTORY DESTINATION ${CMAKE_BINARY_DIR}/bin/figures/emb)