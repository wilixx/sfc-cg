import csv
import sys
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import metrics
import sys
sys.path.append("../../../MyPython")
from math import sqrt
import utility
import collections
# import sndlib	
from matplotlib.ticker import FuncFormatter

def latexify(fig_width=None, fig_height=None, columns=1, fontsize=25):
	assert(columns in [1,2])
	fig_widths = [3.39, 6.9]
	if fig_width is None:
	    fig_width = fig_widths[columns-1] # width in inches

	if fig_height is None:
	    fig_height = fig_width*((sqrt(5)-1.0)/2.0) # height in inches

	MAX_HEIGHT_INCHES = 8.0
	if fig_height > MAX_HEIGHT_INCHES:
	    print("WARNING: fig_height too large:" + fig_height + 
	          "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
	    fig_height = MAX_HEIGHT_INCHES

	params = {'backend': 'pdf',
	          # 'text.latex.preamble': ['\usepackage{gensymb}'],
	          'axes.labelsize': 10, # fontsize for x and y labels (was 10)
	          'axes.titlesize': 10,
	          'font.size': fontsize, # was 10
	          'legend.fontsize': 10, # was 10
	          'xtick.labelsize': 10,
	          'ytick.labelsize': 10,
	          # 'text.usetex': True,
	          'figure.figsize': [fig_width, fig_height],
	          'font.family': 'serif', 
	}
	matplotlib.rcParams.update(params)

orders = {"atlanta": 15, "internet2": 10, "germany50": 50}
periods = ["D1", "D2", "D3", "D4", "D5"]
heuristic = {
	"germany50": [19214.8606903, 23104.5269685, 25930.108961, 31267.4138086, 32546.0834435], 
	"atlanta": [27274.4577074, 55594.0517419, 83582.1606231, 0, 0], 
	"ta2": [0, 0, 0, 0, 0], 
	"zib54": [0, 0, 0, 0, 0],
	"abilene": [0, 0, 0, 0, 0],
	"pdh": [0, 0, 0, 0, 0]
}

percents = {
	"germany50": [1, 2.25, 3.5, 5.25, 5.5], 
	"germany50_HW": [1, 2.25, 3.5, 5.25, 6], 
	"atlanta": [1, 2.25, 3.5, 5.5, 6], 
	"atlanta_HW": [1, 2.25, 3.5, 5.25, 6], 
	"ta2": [1, 2.75, 4.5, 6.25, 8], 
	"zib54": [1, 3, 5, 7, 9],
	"abilene": [1],
	"pdh": [1, 2.25, 3.5, 5.25, 6],#[1, 2.75, 4.5, 6.25, 8],
	"pdh_HW": [1, 2.25, 3.5, 5.25, 6]#[1, 2.75, 4.5, 6.25, 8]
}

funcCoefs = {
	"germany50": 5, 
	"germany50_HW": 5, 
	"atlanta": 6,
	"atlanta_HW": 6,
	"ta2": 5, 
	"zib54": 5,
	"abilene": 5,
	"pdh": 5,
	"pdh_HW": 5
}

maxEnergy = {
	"germany50": [21881.11671222458, 24507.51260250524, 27623.45426855006, 31399.68394930134, 32229.3943729099], 
	"atlanta": [7530.605416201316,10768.862186452967,14207.11895670461,18650.302933052117,20466.4231486021],
	"germany50_HW": 27374.8023786, 
	"atlanta_HW": 20213.6985123,
	"pdh_HW": 12116.1457503,
	"ta2": 5, 
	"zib54": 5,
	"abilene": 5,
	"pdh": [8360.293267659003, 9890.700668240595, 11215.303355322116, 12975.912405792402, 13775.577330753053]
}

colors = {
	"germany50": "red", 
	"germany50_HW": "red", 
	"atlanta": '0.75', 
	"atlanta_HW": '0.75',
	"ta2": "yellow", 
	"zib54": "cyan",
	"abilene": "magenta",
	"pdh": "green",
	"pdh_HW": "green"
}

labels = {
	"germany50": "germany50", 
	"germany50_HW": "germany50", 
	"atlanta": "atlanta", 
	"atlanta_HW": "atlanta",
	"ta2": "ta2", 
	"zib54": "zib54",
	"abilene": "abilene",
	"pdh": "pdh",
	"pdh_HW": "pdh"
}

ylimDelay = {
	"germany50_HW": [0, 45],
	"atlanta_HW": [0, 45],
	"germany50": [0, 20],
	"atlanta": [0, 20],
	"pdh": [0, 20]
}

modelNames = ["path3", "path", "path2", "path_fromILPHeur", "path2_fromILPHeur"]
modelILPHeur = ["path3_fromILPHeur", "path_fromILPHeur", "path2_fromILPHeur"]

labels = {
	"path": "CG-cuts", 
	"path2": "CG-cut+", 
	"path3": "CG-simple", 
	"path_fromILPHeur":"CG-cuts", 
	"path2_fromILPHeur":"CG-cut+",
	"path3_fromILPHeur":"CG-simple",
	"ILPHeur": "GreenChains"
}
colors = {
	"path": "red", 
	"path2": "green", 
	"path3": '0.75', 
	"path_fromILPHeur":"red", 
	"path2_fromILPHeur":"green",
	"path3_fromILPHeur":'0.75',
	"ILPHeur": "black"
}

latexify()

def plotILPValue(name):
	print "plotILPValue(", name, ")"
	listInt = []
	listHeur = []
	delta = []
	listX = []

	i = 0
	for period in xrange(5):
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = metrics.getEnergyCGMetrics(\
				"./results/energy/{}_path_fromILPHeur_{:.6f}_{}.res".format(name, percents[name][period], funcCoefs[name]))
			if intObj != -1:
				listInt.append(intObj / maxEnergy[name][period])
				delta.append((fracObj - intObj)  / maxEnergy[name][period])
			else:
				listInt.append(0)
				delta.append(0)
		except Exception as e:
			listInt.append(0)
			delta.append(0)
		try:
			ILPHeurMetrics = metrics.getILPHeurMetrics(\
				"./results/energy/{}_ILPHeur_{:.6f}_{}.res".format(name, percents[name][period], funcCoefs[name]))
			listHeur.append(ILPHeurMetrics["intObj"] / maxEnergy[name][period])
		except Exception as e:
			print e
			listHeur.append(0)
		listX.append(i)
		i += 1
		

	plt.clf()
	f, ax = plt.subplots()
	print name, "listInt: ", listInt, "listHeur: ", listHeur, [ 100*(a - b) for a, b in zip(listHeur, listInt)]
	plt.bar([x-0.45 for x in listX], listInt, width=.45, yerr=([0 for _ in range(len(delta))], delta), label="CG", color="red")
	plt.bar([x for x in listX], listHeur, width=.45, label="Heuristic", color="blue")
	
	plt.legend(loc="lower right", framealpha=1)
	plt.xlabel("Time periods")
	plt.xticks([0, 1, 2, 3, 4], periods)

	plt.ylabel("Energy used")
	plt.yticks([i / 100.0 for i in xrange(0, 101, 10)], xrange(0, 101, 10))
	print "Saved to ./figs/energy/"+name+"_ILPValue.pdf"
	plt.savefig("./figs/energy/"+name+"_ILPValue.pdf", bbox_inches='tight')

def plotDelay(name, model):
	delays = []

	for period in xrange(5):
		try:
			if model == "ILPHeur":
				metricsILPHeur = metrics.getILPHeurMetrics(\
					"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name]))
				# print metricsILPHeur
				if metricsILPHeur["intObj"] != -1:
					delays.append( [(len(path)-1) * 1.8 for path in metricsILPHeur["paths"]] )
				else:
					delays.append(0)
			else:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = metrics.getEnergyCGMetrics(\
				"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name]))
				if intObj != -1:
					delays.append([(len(path)-1) * 1.8 for path in paths])
					# print period, delays[-1].count(1.8)
				else:
					delays.append(0)

			# print name, " -> ", intObj, fracObj, nbNFV
		except IOError as e:
			delays.append(0)
			print "Exception: ", e
			pass
	# print [len(d) for d in delays]
	plt.clf()
	bp = plt.boxplot(delays, positions=xrange(0, 5), widths=.8, sym='', whis=[10, 90])
	for val in bp['medians']:
		print "medians: ", val.get_ydata()
	for val in bp["whiskers"]:
		print "whiskers: ", val.get_ydata()
	# plt.legend(loc="upper left")
	plt.xlabel("Time periods")
	plt.xticks(xrange(0, 5), periods)
	plt.ylabel("Delay (ms)")
	if name in ylimDelay:
		plt.ylim(ylimDelay[name][0], ylimDelay[name][1])

	# plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
 #                        wspace=0.2, hspace=0.2)
	filename = "./figs/energy/{}_{}_delay.pdf".format(name, model)
	print "Saved to ", filename
	plt.savefig(filename, bbox_inches='tight')

def plotEnergySaved(name):
	x = [0, 2, 3, 7, 8, 9, 11, 18, 23, 24]

	savingsHeur = []
	for period in xrange(5):
		try:
			metricsILPHeur = metrics.getILPHeurMetrics(\
				"./results/energy/{}_ILPHeur_{:.6f}_{}.res".format(name, percents[name][period], funcCoefs[name]))
			savings.append(100 * metricsILPHeur["intObj"] - legacy[name][period] / float(legacy[name][period]))
		except:
			savings.append(0)

	savingsHard = [100 * hardware[name][period] - hardware[name][period] / float(hardware[name][period]) for period in xrange(5)]

	plt.step(x, savingsHard, where='post', label="Hardware") 
	plt.step(x, savingsHeur, where='post', label="GreenChains") 

def plotActiveLinks(name, model):
	listInt = []
	# listHeur = []
	delta = []
	listX = []

	i = 0
	for period in xrange(5):
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinksPath = metrics.getEnergyCGMetrics(\
				"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name]))
			# metricsILPHeur = metrics.getILPHeurMetrics(\
				# "./results/energy/{}_ILPHeur_{:.6f}_{}.res".format(name, percents[name][period], funcCoefs[name]))
			
			listInt.append(len([edge for edge, b in activeLinksPath if b]))
			# listHeur.append(len([edge for edge, b in activeLinksHeur if b]))
			listX.append(i)
			# print listX, listInt, listHeur
			i += 1
		except IOError as e:
			print "Exception: ", e
			pass

	plt.clf()
	print [x-0.45 for x in listX], listInt
	plt.bar([x-0.45 for x in listX], listInt, width=.45, label="CG", color="red")
	# plt.bar([x for x in listX], listHeur, width=.45, label="Heuristic", color="blue")
	plt.legend(loc="lower right")
	plt.xlabel("Time periods")
	plt.xticks([0, 1, 2, 3, 4], periods)
	plt.ylabel(r"# links")
	plt.axhline(len(linkUsage), label="m")
	plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	print "Saved to ./figs/energy/"+name+"_activeLinks.pdf"
	plt.savefig("./figs/energy/"+name+"_activeLinks.pdf", bbox_inches='tight')

def plotRatio(names):
	listX = collections.defaultdict(list)
	delta = collections.defaultdict(list)

	for name in names:
		i = 0
		for period in xrange(5):
			try:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = metrics.getEnergyCGMetrics(\
					"./results/energy/{}_path_{:.6f}_{}.res".format(name, percents[name][period], funcCoefs[name]))
				delta[name].append((intObj - fracObj) / float(fracObj))
				print name, percents[name][period], intObj, fracObj
				listX[name].append(i)
			except IOError as e:
				print "Exception: ", e
				pass
			i += 1

	plt.clf()
	for shift, name in zip([-.45, -.15, .15], names):
		print name, [x+shift for x in listX[name]], delta[name]
		plt.bar([x+shift for x in listX[name]], delta[name], width=.3, label=labels[name], color=colors[name])
	plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

	plt.xlabel("Time periods")	
	plt.xticks([0, 1, 2, 3, 4], periods)
	plt.ylabel(r'$\varepsilon$ ratio')
	plt.subplots_adjust(left=0.22, bottom=0.22, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	print "Saved to ./figs/energy/ratios.pdf"
	plt.savefig("./figs/energy/ratios.pdf", bbox_inches='tight')

def plotLinkUsage(name, model):
	usages = collections.defaultdict(list)
	ratioNames = ["pdh", "atlanta", "germany50"]
	linkPeriods = [0, 4]
	i = 0
	for period in linkPeriods:
		try:
			intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = \
			metrics.getEnergyCGMetrics(\
				"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name])\
			)
			for _, (usage, capa) in linkUsage.iteritems():
				usages[period].append(usage / float(capa))
		except IOError as e:
			usages[period] = [0]
			print "Exception: ", e
			pass
		i += 1

	plt.clf()	
	for period in linkPeriods:
		# linkUsages = sorted(usages[period].keys())
		# nbLinks = [usages[period][key] for key in linkUsages]
		# print linkUsages, nbLinks
		cdfLinks = sorted(utility.cdf(usages[period]).items(), key=lambda x:x[0])
		print cdfLinks[0], cdfLinks[-1], cdfLinks[-2]
		plt.step(*zip(*cdfLinks), where="post", label=periods[period])
		
	plt.legend(loc="lower right")
	plt.xlabel("Link usage")
	plt.xlim(0, 1)
	plt.xticks([i / 10.0 for i in range(0, 11, 2)], ["{}%".format(i) for i in range(0, 101, 20)])
	plt.ylabel("% of links")
	plt.ylim(0, plt.ylim()[1])
	plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	filename = "./figs/energy/{}_{}_linkUsage.pdf".format(name, model)
	print "Saved to ", filename
	plt.savefig(filename, bbox_inches='tight')

def plotCompModels(name, models=modelNames):
	objValue = {}
	deltas = {}

	for model in models:
		objValue[model] = [0 for i in xrange(0, 5)]
		deltas[model] = [0 for i in xrange(0, 5)]
		for period in xrange(0, 5):
			try:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = \
				metrics.getEnergyCGMetrics(\
					"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name])\
				)
				objValue[model][period] = intObj / maxEnergy[name][period]
				deltas[model][period] = (fracObj - intObj) / maxEnergy[name][period]
			except IOError as e:
				print "Exception: ", e
				pass
	print objValue
	plt.clf()
	f, ax = plt.subplots()

	barWidth = .9 / float(len(models))
	shifts = [(x / float(len(models)) - .5) * .9 for x in range(0, len(models))]

	for shift, model in zip(shifts, models):
		print objValue[model], deltas[model], [x + shift for x in xrange(0, 5)]
		plt.bar([x + shift for x in xrange(0, 5)], objValue[model], yerr=([0 for _ in range(len(deltas[model]))], deltas[model]), \
			width=barWidth, label=labels[model], color=colors[model], ecolor="black")
		
	# plt.legend(loc="lower right", framealpha=1)
	plt.xlabel("Time periods")
	plt.xticks([0, 1, 2, 3, 4], periods)
	plt.ylabel("Energy used")
	plt.ylim(0, plt.ylim()[1])

	plt.yticks([i / 100.0 for i in xrange(0, 101, 10)], xrange(0, 101, 10))
	# ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: "{:.0f}".format(100.0 * x / float(maxEnergy[name]))))
	# plt.subplots_adjust(left=0.22, bottom=0.22, right=0.96, top=0.93,
                        # wspace=0.2, hspace=0.2)
	filename = "./figs/energy/{}_compModels.pdf".format(name)
	print "Saved to ", filename
	plt.savefig(filename, bbox_inches='tight')

def plotCompModelsRatio(name, models=modelNames):
	objValue = {}

	for model in models:
		objValue[model] = [0 for i in xrange(0, 5)]
		for period in xrange(0, 5):
			try:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = \
				metrics.getEnergyCGMetrics(\
					"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name])\
				)
				objValue[model][period] = (intObj - fracObj) / fracObj
			except IOError as e:
				print "Exception: ", e
				pass
	print objValue
	plt.clf()	
	# f, ax = plt.subplots()
	i = -2

	barWidth = .9 / float(len(models))
	shifts = [(x / float(len(models)) - .5) * .9 for x in range(0, len(models))]

	for shift, model in zip(shifts, models):		
		print model, objValue[model], min(objValue[model]), max(objValue[model])
		plt.bar([x + shift for x in xrange(0, 5)], objValue[model], \
			width=barWidth, label=labels[model], color=colors[model], ecolor="black")
		i += 1
		
	# plt.legend(loc="lower left", framealpha=1, ncol=3)
	plt.xlabel("Time periods")
	plt.xticks([0, 1, 2, 3, 4], periods)

	plt.ylabel(r"$\varepsilon$ ratio")
	# plt.ylim(0, plt.ylim()[1])
	plt.yscale('log')

	plt.grid()
	# plt.subplots_adjust(left=0.22, bottom=0.22, right=0.96, top=0.93,
 #                        wspace=0.2, hspace=0.2)
	filename = "./figs/energy/{}_compModelsRatio.pdf".format(name)
	print "Saved to ", filename
	plt.savefig(filename, bbox_inches='tight')

def plotCompModelsRatioLegend():
	plt.clf()
	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	
	# for model in ["path3_fromILPHeur", "path_fromILPHeur", "path2_fromILPHeur"]:
		# plt.bar([0], [0], width=0, label=labels[model], color=colors[model], ecolor="black")
	plt.bar([0], [0], label=labels["path3_fromILPHeur"], color=colors["path3_fromILPHeur"], ecolor="black")
	plt.bar([0], [0], label=labels["path_fromILPHeur"], color=colors["path_fromILPHeur"], ecolor="black")
	plt.bar([0], [0], label=labels["path2_fromILPHeur"], color=colors["path2_fromILPHeur"], ecolor="black")

	ax.axis('off')
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

	leg = ax.legend(loc='center', ncol=3)
	fig.savefig("figs/energy/legend_comp_models.pdf", bbox_inches='tight', pad_inches=0)

def plotCompModelsTime(name, models=modelNames):
	objValue = {}

	for model in models:
		objValue[model] = [0 for i in xrange(0, 5)]
		for period in xrange(0, 5):
			try:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = \
				metrics.getEnergyCGMetrics(\
					"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name])\
				)
				objValue[model][period] = WallTime / 1000.0
			except IOError as e:
				print "Exception: ", e
				pass
	print objValue
	plt.clf()	
	i = -2

	barWidth = .9 / float(len(models))
	shifts = [(x / float(len(models)) - .5) * .9 for x in range(0, len(models))]
	print "shifts: ", shifts
	for shift, model in zip(shifts, models):
		print model, objValue[model], min(objValue[model]), max(objValue[model])
		plt.bar([x + shift for x in xrange(0, 5)], objValue[model], \
			width=barWidth, label=labels[model], color=colors[model], ecolor="black")
		i += 1
		
	# plt.legend(loc="upper right", framealpha=0)
	
	plt.xlabel("Time periods")
	plt.xticks([0, 1, 2, 3, 4], periods)
	
	plt.ylabel("Time (s)")
	plt.yscale('log')
	plt.grid()
	plt.yticks([.001, .1, 1, 60, 60000], ["1ms", "100ms", "1s", "1min", "10min"])

	plt.subplots_adjust(left=0.22, bottom=0.22, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	filename = "./figs/energy/{}_compModelsTime.pdf".format(name)
	print "Saved to ", filename
	plt.savefig(filename, bbox_inches='tight')

def plotCompHeur(name):
	objValue = {}
	
	models = ["ILPHeur", "path_fromILPHeur"]

	for model in models:
		objValue[model] = [0 for i in xrange(0, 5)]
		for period in xrange(0, 5):
			try:
				intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks = \
				metrics.getEnergyCGMetrics(\
					"./results/energy/{}_{}_{:.6f}_{}.res".format(name, model, percents[name][period], funcCoefs[name])\
				)
				objValue[model][period] = intObj
			except IOError as e:
				print "Exception: ", e
				pass
	print objValue
	plt.clf()	
	i = -2

	barWidth = .9 / float(len(models))
	shifts = [(x / float(len(models)) - .5) * .9 for x in range(0, len(models))]
	print "shifts: ", shifts
	for shift, model in zip(shifts, models):
		print objValue[model], [x + shift for x in xrange(0, 5)]
		plt.bar([x + shift for x in xrange(0, 5)], objValue[model], \
			width=barWidth, label=labels[model], color=colors[model], ecolor="black")
		i += 1
		
	plt.legend(loc="upper right", framealpha=0)
	
	plt.xlabel("Time periods")
	plt.xticks([0, 1, 2, 3, 4], periods)
	
	plt.ylabel("Time (s)")
	# plt.yscale('log')
	# plt.grid()
	# plt.yticks([.001, .1, 1, 60, 60000], ["1ms", "100ms", "1s", "1min", "10min"])

	plt.subplots_adjust(left=0.22, bottom=0.22, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	filename = "./figs/energy/{}_compHeur.pdf".format(name)
	print "Saved to ", filename
	plt.savefig(filename, bbox_inches='tight')

def plotAllCompModels(name, models=modelNames):
	plotCompModels(name, models)
	plotCompModelsRatio(name, models)
	plotCompModelsTime(name, models)

def plot_NRJ_journal():
	for name in ["pdh", "atlanta", "germany50"]:
		plotAllCompModels(name, modelILPHeur) if name != "germany50" else plotAllCompModels(name, ["path3_fromILPHeur", "path_fromILPHeur"])
		plotILPValue(name)
		for heur in ["checkSolution", "path_fromILPHeur"]:
			plotDelay(name, heur)
			plotLinkUsage(name, heur)

if __name__ == "__main__":
	if len(sys.argv) < 4:
		print "Not enough arguments: {name} {model}"
		sys.exit()
	plot = sys.argv[1]
	names = [sys.argv[2]] if sys.argv[2] != "all" else ["atlanta", "germany50", "pdh", "atlanta_HW", "germany50_HW", "pdh_HW"]
	models = [sys.argv[3]] if sys.argv[3] != "all" else ["path", "heur"]
	
	if "all" == plot or "ratio" == plot:
		plotRatio(["atlanta", "germany50", "pdh"])
	if "all" == plot or "comp" == plot:
		for name in names:
			plotCompModels(name)
			plotCompModelsRatio(name)
	if "all" == plot or "obj" == plot:
		for name in names:
			plotILPValue(name)
	if "all" == plot or "delay" == plot:
		for name in names:
			plotDelay(name)
	# if "all" == plot or "adddelay" == plot:
	# 	for name in names:
	# 		plotAdditionalDelay(name)
	if "all" == plot or "link" == plot:
		for name in names:
			plotActiveLinks(name)
	if "all" == plot or "load" == plot:
		for name in names:
			for model in models:
				plotLinkUsage(name, model)

