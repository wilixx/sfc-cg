import csv
import collections

def getCGMetrics(filename):
	print "Getting metrics for ", filename
	with open(filename, 'r') as csvfile:
		dataReader = csv.reader(csvfile, delimiter='\t')
		row = dataReader.next()
		intObj = float(row[0])
		fracObj = float(row[1])

		row = dataReader.next()
		nbColumn = int(row[0])

		row = dataReader.next()
		CPUTime = float(row[0])
		WallTime = float(row[1])

		nbNFV = 0
		paths = []
		chains = collections.defaultdict(lambda: collections.defaultdict(int))
		try:
			nodeUsage = {}
			n = int(dataReader.next()[0])
			for _ in range(n):
				row = dataReader.next()
				if int(row[2]) != -1:
					nbNFV += 1
				nodeUsage[int(row[0])] = ((int(row[1]), int(row[2])))
 
			linkUsage = {}
			m = int(dataReader.next()[0])
			for _ in range(m):
				row = dataReader.next()
				linkUsage[(int(row[0]), int(row[1]))] = (float(row[2]), float(row[3]))

			nbPaths = int(dataReader.next()[0])
			
			for _ in range(nbPaths):
				row = dataReader.next()

				pathLength = int(row[0])
				paths.append([int(i) for i in row[1:1+pathLength]])
				nbLocal = int(row[pathLength])
				chainLocal = ()
				chain = ()
				for i in range(pathLength+1, len(row)-1, 2):
					chainLocal += (int(row[i]), )
					chain += (int(row[i+1]), )
				chains[chain][chainLocal] += 1
		except Exception as e:
			print e
			pass
	return intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage

def getILPMetrics(filename, n):
	print "Getting metrics for ", filename
	with open(filename, 'r') as csvfile:
		dataReader = csv.reader(csvfile, delimiter='\t')
		row = dataReader.next()
		intObj = float(row[0])
		fracObj = float(row[1])

		row = dataReader.next()
		CPUTime = float(row[0])
		WallTime = float(row[1])

	return intObj, fracObj, 0, CPUTime, WallTime, 0

def getILPHeurMetrics(filename):
	print "Getting metrics for ", filename
	with open(filename, 'r') as csvfile:
		dataReader = csv.reader(csvfile, delimiter="\t")
		# Get energy values
		metrics = {}
		row = dataReader.next()
		metrics["intObj"] = float(row[0])
		metrics["linkCost"] = float(row[1])
		metrics["coreCost"] = float(row[2])

		# Get paths
		metrics["paths"] = []
		for row in dataReader:
			# print row
			demandId = int(row[0])
			pathLength = int(row[1])
			metrics["paths"].append([int(x) for x in row[2:2+pathLength]])
			locations = [int(x) for x in row[2+pathLength:-1]]

	return metrics

def getEnergyCGMetrics(filename):
	print "Getting metrics for ", filename
	with open(filename, 'r') as csvfile:
		dataReader = csv.reader(csvfile, delimiter='\t')
		row = dataReader.next()
		intObj = float(row[0])
		fracObj = float(row[1])

		row = dataReader.next()
		nbColumn = int(row[0])

		row = dataReader.next()
		CPUTime = float(row[0])
		WallTime = float(row[1])

		nbNFV = 0
		paths = []
		chains = collections.defaultdict(lambda: collections.defaultdict(int))
		activeLinks = {}
		nodeUsage = {}
		linkUsage = {}
		try:
			n = int(dataReader.next()[0])
			for _ in range(n):
				row = dataReader.next()
				if int(row[2]) != -1:
					nbNFV += 1
				nodeUsage[int(row[0])] = ((int(row[1]), int(row[2])))

			m = int(dataReader.next()[0])
			for _ in range(m):
				edge = (int(row[0]), int(row[1]))
				row = dataReader.next()
				linkUsage[edge] = (float(row[2]), float(row[3]))
				activeLinks[edge] = float(row[4]) == 1.0000

			nbPaths = int(dataReader.next()[0])
			for _ in range(nbPaths):
				row = dataReader.next()

				pathLength = int(row[0])
				paths.append([int(i) for i in row[1:1+pathLength]])
				nbLocal = int(row[pathLength])
				chainLocal = ()
				chain = ()
				for i in range(pathLength+1, len(row)-1, 2):
					chainLocal += (int(row[i]), )
					chain += (int(row[i+1]), )
				chains[chain][chainLocal] += 1
		except Exception as e:
			print e
			pass

	return intObj, fracObj, nbColumn, CPUTime, WallTime, nbNFV, paths, chains, nodeUsage, linkUsage, activeLinks
