from subprocess import call
import sys

orders = {"atlanta": 15, "internet2": 10, "germany50": 50, "ta2": 65}

names = [sys.argv[1]] if sys.argv[1] != "all" else ["internet2", "atlanta", "germany50", "ta2"]
for name in names:
	for i in range(1, orders[name]+1):
		instName = "{}_emb_{}".format(name, i)
		print "./LP_NRJ {}".format(instName)
		if call("./LP_NRJ {}".format(instName), shell=True) != 1:
			print "Failed to lauch ", "./LP_NRJ {}".format(instName)
			break
	# nbIte = 0
	# while call("./LP_NRJ {} {} 1 4".format(model, instName), shell=True) != 1 and nbIte < 10:
	# 	call("python changeNodeCapa.py {} 1.01".format(instName), shell=True)
	# 	nbIte += 1
 