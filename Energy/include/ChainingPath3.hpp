#ifndef ENERGY_CHAINING_PATH3_HP
#define ENERGY_CHAINING_PATH3_HP

#include <chrono>
#include <cmath>
#include <ilcplex/ilocplex.h>
#include <iostream>
#include <map>
#include <tuple>

#include <DiGraph.hpp>
#include <ShortestPath.hpp>
#include <ThreadPool.hpp>
#include <cplex_utility.hpp>
#include <utility.hpp>

#include "Energy.h"
#include "SFC.hpp"

// namespace Energy {
// class ChainPlacement3 {
//     class ReducedMainProblem {
//         friend ChainPlacement3;

//       public:
//         explicit ReducedMainProblem(const ChainPlacement3& _placement)
//             : m_placement(_placement)
//             , m_env()
//             , m_model(m_env)
//             , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
//             , m_x([&]() {
//                 IloNumVarArray x(m_env, m_placement.m_inst.network.size());
//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.edges) {
//                     if (edge.first < edge.second) {
//                         x[e] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0));
//                         setIloName(x[e], "x" + toString(edge));
//                     }
//                     ++e;
//                 }
//                 return x;
//             }())
//             , m_k([&]() {
//                 IloNumVarArray k(m_env, m_placement.m_inst.NFVNodes.size());
//                 for (const auto u : m_placement.m_inst.NFVNodes) {
//                     k[m_placement.m_inst.NFVIndices[u]] = IloNumVar(m_env, 0.0, IloInfinity);
//                     setIloName(k[m_placement.m_inst.NFVIndices[u]], "k(" + toString(u) + ")");
//                 }
//                 return IloAdd(m_model, k);
//             }())
//             , m_obj([&]() {
//                 IloNumVarArray vars(m_env);
//                 IloNumArray vals(m_env);
//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.edges) {
//                     if (edge.first < edge.second) {
//                         vars.add(m_x[e]);
//                         vals.add(2 * m_placement.m_inst.energyCons.activeLink);
//                     }
//                     ++e;
//                 }
//                 for (const auto u : m_placement.m_inst.NFVNodes) {
//                     vars.add(m_k[m_placement.m_inst.NFVIndices[u]]);
//                     vals.add(m_placement.m_inst.energyCons.activeCore);
//                 }
//                 return IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
//             }())
//             , m_onePathCons([&]() {
//                 IloRangeArray onePathCons(m_env, m_placement.m_inst.demands.size(), 1.0, 1.0);
//                 for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
//                     setIloName(onePathCons[i], "onePathCons" + toString(m_placement.m_inst.demands[i]));
//                 }
//                 return IloAdd(m_model, onePathCons);
//             }())
//             , m_linkCapasCons1([&]() {
//                 IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity);
//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.edges) {
//                     int xIndex = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
//                     linkCapaCons[e].setLinearCoef(m_x[xIndex], m_placement.m_inst.network.getEdgeWeight(edge));
//                     setIloName(linkCapaCons[e], "linkCapas1" + toString(edge));
//                     ++e;
//                 }
//                 return IloAdd(m_model, linkCapaCons);
//             }())
//             , m_linkCapasCons2([&]() {
//                 IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), 0.0, IloInfinity);
//                 int e = 0;
//                 for (const Graph::Edge& edge : m_placement.m_inst.edges) {
//                     linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(edge));
//                     setIloName(linkCapaCons[e], "linkCapas2" + toString(edge));
//                     ++e;
//                 }
//                 return IloAdd(m_model, linkCapaCons);
//             }())
//             , m_nodeCapasCons1([&]() {
//                 IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
//                 for (const auto u : m_placement.m_inst.NFVNodes) {
//                     const int i = m_placement.m_inst.NFVIndices[u];
//                     nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, -IloInfinity, -m_k[i], 0.0));
//                     setIloName(nodeCapasCons[i], "nodeCapasCons1" + std::to_string(u));
//                 }
//                 return nodeCapasCons;
//             }())
//             , m_nodeCapasCons2([&]() {
//                 IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
//                 for (const auto u : m_placement.m_inst.NFVNodes) {
//                     const int i = m_placement.m_inst.NFVIndices[u];
//                     nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, 0.0, m_k[i], m_placement.m_inst.nodeCapa[u]));
//                     setIloName(nodeCapasCons[i], "nodeCapasCons2" + std::to_string(u));
//                 }
//                 return nodeCapasCons;
//             }())
//             , m_linkCapaDuals1(m_env)
//             , m_linkCapaDuals2(m_env)
//             , m_inst.nodeCapaDuals(m_env)
//             , m_onePathDuals(m_env)
//             , m_solver([&]() {
//                 IloCplex solver(m_model);
//                 solver.setOut(m_env.getNullStream());
//                 // solver.setParam(IloCplex::EpGap, 0.0077);
//                 // solver.setParam(IloCplex::VarSel, 3);
//                 solver.setParam(IloCplex::EpRHS, 1e-9);
//                 return solver;
//             }())
//             , m_paths() {
//         }

//         ~ReducedMainProblem() {
//             m_env.end();
//         }

//         ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
//         ReducedMainProblem(const ReducedMainProblem&) = delete;

//         bool checkReducedCosts() const {
//             for (int k = 0; k < m_paths.size()); ++k; {
//                 int i = m_paths[k].second;
//                 double d = m_placement.m_inst.demands[i].d;
//                 double myRC = -m_onePathDuals[i];
//                 for (auto iteU = m_paths[k].first.first.begin(), iteV = std::next(iteU);
//                      iteV != m_paths[k].first.first.end();
//                      ++iteU, ++iteV) {
//                     const int e = m_placement.m_inst.edgeID(*iteU, *iteV);
//                     myRC += m_placement.m_inst.energyCons.propLink * (d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV))
//                             + d * m_linkCapaDuals1[e]
//                             - d * m_linkCapaDuals2[e];
//                 }
//                 std::size_t j = 0;
//                 for (const auto& pair_NodeFuncs : m_paths[k].first.second) {
//                     for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
//                         const int u = pair_NodeFuncs.first;
//                         // std::cout << u << '\t' << m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] << '\n';
//                         myRC += -m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(i, j);
//                     }
//                 }
//                 if(std::abs(myRC - m_solver.getReducedCost(m_y[k])) > RC_EPS) {
//                     return false;
//                 }
//             }
//             return true;
//         }

//         bool solve() {
//             return m_solver.solve();
//         }

//         double getObjValue() const {
//             return m_solver.getObjValue();
//         }

//         void getDuals() {
//             m_solver.getDuals(m_linkCapaDuals1, m_linkCapasCons1);
//             m_solver.getDuals(m_linkCapaDuals2, m_linkCapasCons2);
//             m_solver.getDuals(m_inst.nodeCapaDuals, m_nodeCapasCons1);
//             m_solver.getDuals(m_onePathDuals, m_onePathCons);
//         }

//         void addPath(const ServicePath& _sPath, const int _i) {
//             const auto pair = std::make_pair(_sPath, _i);
//             assert([&]() {
//                 const auto ite = std::find(m_paths.begin(), m_paths.end(), pair);
//                 if (ite != m_paths.end()) {
//                     std::cout << pair << " is already present!" << '\n';
//                     return false;
//                 }
//                 return true;
//             }());

//             const auto& path = _sPath.first;
//             const auto& funcPlacement = _sPath.second;

//             const auto& demand = m_placement.m_inst.demands[_i];

//             assert(path.front() == demand.s);
//             assert(path.back() == demand.t);
//             const auto d = demand.d;

//             double objCoef = 0.0;
//             IloNumArray valsLink1(m_env, m_placement.m_inst.network.size());
//             IloNumArray valsLink2(m_env, m_placement.m_inst.network.size());
//             for (int e(0); e < m_placement.m_inst.network.size()); ++e; {
//                 valsLink1[e] = 0;
//                 valsLink2[e] = 0;
//             }
//             for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                 valsLink1[m_placement.m_inst.edgeID(*iteU, *iteV)] -= d;
//                 valsLink2[m_placement.m_inst.edgeID(*iteU, *iteV)] += d;
//                 objCoef += m_placement.m_inst.energyCons.propLink * (d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV));
//             }

//             IloNumArray valsNode(m_env, m_placement.m_inst.NFVNodes.size());

//             for (std::size_t i = 0; i < valsNode.getSize()); ++i; {
//                 valsNode[i] = 0;
//             }

//             std::size_t j = 0;
//             for (const auto& pair_NodeFuncs : funcPlacement) {
//                 for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
//                     assert(0 <= pair_NodeFuncs.first && pair_NodeFuncs.first < m_placement.m_inst.network.getOrder());
//                     assert(std::find(path.begin(), path.end(), pair_NodeFuncs.first) != path.end());
//                     valsNode[m_placement.m_inst.NFVIndices[pair_NodeFuncs.first]] += m_placement.m_inst.nbCores(_i, j);
//                 }
//             }

//             IloNumColumn inc = m_onePathCons[_i](1.0)
//                                + m_linkCapasCons1(valsLink1)
//                                + m_linkCapasCons2(valsLink2)
//                                + m_nodeCapasCons1(valsNode)
//                                + m_obj(objCoef);

//             auto yVar = IloNumVar(inc);
//             setIloName(yVar, "y" + toString(std::make_tuple(demand, _sPath)));
//             m_y.add(yVar);
//             m_paths.emplace_back(_sPath, _i);
//         }

//         double solveInteger() {
//             m_model.add(IloConversion(m_env, m_y, ILOBOOL));
//             m_model.add(IloConversion(m_env, m_k, ILOINT));

//             int e = 0;
//             for (const Graph::Edge& edge : m_placement.m_inst.edges) {
//                 if (edge.first < edge.second) {
//                     m_model.add(IloConversion(m_env, m_x[e], ILOBOOL));
//                 }
//                 ++e;
//             }

//             if (m_solver.solve()) {
//                 auto obj = m_solver.getObjValue();
//                 std::cout << "Final obj value: " << obj << "\t# Paths: " << m_paths.size() << '\n';
//                 getNetworkUsage();
//                 return obj;
//             }
//             if ((m_solver.getStatus() == IloAlgorithm::Infeasible) || (m_solver.getStatus() == IloAlgorithm::InfeasibleOrUnbounded)) {
//                 std::cout << '\n'
//                           << "No solution - starting Conflict refinement" << '\n';

//                 IloConstraintArray infeas(m_env);
//                 IloNumArray preferences(m_env);

//                 infeas.add(m_onePathCons);
//                 infeas.add(m_linkCapasCons1);
//                 infeas.add(m_linkCapasCons2);
//                 infeas.add(m_nodeCapasCons1);
//                 infeas.add(m_nodeCapasCons2);
//                 for (IloInt i = 0; i < m_k.getSize(); ++i) {
//                     if (m_k[i].getType() != IloNumVar::Bool) {
//                         infeas.add(IloBound(m_k[i], IloBound::Lower));
//                         infeas.add(IloBound(m_k[i], IloBound::Upper));
//                     }
//                 }

//                 for (IloInt i = 0; i < infeas.getSize(); ++i) {
//                     preferences.add(1.0); // user may wish to assign unique preferences
//                 }

//                 if (m_solver.refineConflict(infeas, preferences)) {
//                     IloCplex::ConflictStatusArray conflict = m_solver.getConflict(infeas);
//                     m_env.getImpl()->useDetailedDisplay(IloTrue);
//                     std::cout << "Conflict :" << '\n';
//                     for (IloInt i = 0; i < infeas.getSize()); ++i; {
//                         if (conflict[i] == IloCplex::ConflictMember)
//                             std::cout << "Proved  : " << infeas[i] << '\n';
//                         if (conflict[i] == IloCplex::ConflictPossibleMember)
//                             std::cout << "Possible: " << infeas[i] << '\n';
//                     }
//                 } else {
//                     std::cout << "Conflict could not be refined\n\n";
//                 }
//             }
//             m_solver.exportModel("mp.sav");
//             return -1.0;
//         }

//         void getNetworkUsage() const {
//             std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
//             std::vector<double> nodeUsage(m_placement.m_inst.network.getOrder(), 0);
//             std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);
//             std::vector<char> active(m_placement.m_inst.network.size(), 0);

//             for (std::size_t i = 0; i < m_paths.size(); ++i) {
//                 if (m_solver.getValue(m_y[i]) > 0.0) {
//                     const auto& path = m_paths[i].first.first;
//                     const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
//                     const auto d = demand.d;
//                     pathsUsed[m_paths[i].second] = m_y[i];

//                     for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                         linkUsage[m_placement.m_inst.edgeID(*iteU, *iteV)] += d * m_solver.getValue(m_y[i]);
//                         active[m_placement.m_inst.edgeID(*iteU, *iteV)] = 1;
//                         active[m_placement.m_inst.edgeID(*iteV, *iteU)] = 1;
//                     }
//                 }
//             }
//             for (const auto u : m_placement.m_inst.NFVNodes) {
//                 nodeUsage[u] = m_solver.getValue(m_k[m_placement.m_inst.NFVIndices[u]]);
//             }
//             std::cout << "Node usage: \n";
//             double nodeEnergy = 0;
//             for (const auto u : m_placement.m_inst.NFVNodes) {
//                 nodeEnergy += nodeUsage[u] * m_placement.m_inst.energyCons.activeCore;
//                 std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u]
//                           << " -> " << nodeUsage[u] * m_placement.m_inst.energyCons.activeCore << '\n';
//                 assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
//             }
//             std::cout << '\t' << nodeEnergy << '\n';
//             double linkEnergy = 0.0;
//             std::cout << "Link usage: \n";
//             std::size_t i = 0;
//             for (const auto& edge : m_placement.m_inst.edges) {
//                 double energy = m_placement.m_inst.energyCons.propLink * linkUsage[i] / m_placement.m_inst.network.getEdgeWeight(edge);
//                 if (active[i]) {
//                     energy += m_placement.m_inst.energyCons.activeLink;
//                 }
//                 linkEnergy += energy;
//                 std::cout << std::fixed << '\t' << edge << " -> " << linkUsage[i] << " / " << m_placement.m_inst.network.getEdgeWeight(edge)
//                           << " -> " << energy << '\n';
//                 assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] > -RC_EPS
//                        || (std::cerr << std::fixed
//                                      << m_linkCapasCons1[i] << '\n'
//                                      << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[i] << '\n',
//                               false));
//                 ++i;
//             }
//             std::cout << nodeEnergy << " + " << linkEnergy << " = " << nodeEnergy + linkEnergy << '\n';
//         }

//       private:
//         const ChainPlacement3& m_placement;
//         IloEnv m_env { };
//         IloModel m_model;

//         IloNumVarArray m_y;
//         IloNumVarArray m_x;
//         IloNumVarArray m_k;

//         IloObjective m_obj;

//         IloRangeArray m_onePathCons;
//         IloRangeArray m_linkCapasCons1;
//         IloRangeArray m_linkCapasCons2;

//         IloRangeArray m_nodeCapasCons1;
//         IloRangeArray m_nodeCapasCons2;

//         IloNumArray m_linkCapaDuals1;
//         IloNumArray m_linkCapaDuals2;
//         IloNumArray m_inst.nodeCapaDuals;
//         IloNumArray m_onePathDuals;

//         IloCplex m_solver;

//         std::vector<std::pair<ServicePath, int>> m_paths;
//     };

//     class PricingProblem {
//         friend ChainPlacement3;

//       public:
//         explicit PricingProblem(const ChainPlacement3& _chainPlacement)
//             : m_placement(_chainPlacement)
//             , m_env()
//             , m_demandID(0)
//             , n(m_placement.m_inst.network.getOrder())
//             , m(m_placement.m_inst.network.size())
//             , nbLayers(m_placement.m_inst.maxChainSize + 1)
//             , m_model(m_env)
//             , m_obj(IloAdd(m_model, IloMinimize(m_env)))
//             , m_a([&]() {
//                 IloNumVarArray a(m_env, m_placement.m_inst.NFVNodes.size() * (nbLayers - 1));
//                 for (const auto u : m_placement.m_inst.NFVNodes) {
//                     for (int j(0); j < nbLayers - 1); ++j; {
//                         a[getIndexA(u, j)] = IloAdd(m_model,
//                             IloNumVar(m_env, 0, 1, ILOBOOL));
//                         setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
//                     }
//                 }
//                 return a;
//             }())
//             , m_f([&]() {
//                 IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
//                 for (int e(0); e < m; ++e) {
//                     for (int j(0); j < nbLayers); ++j; {
//                         setIloName(f[getIndexF(e, j)], "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
//                     }
//                 }
//                 return IloAdd(m_model, f);
//             }())
//             , m_flowConsCons([&]() {
//                 IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
//                 for (Graph::Node u(0); u < m_placement.m_inst.network.getOrder()); ++u; {
//                     for (int j(0); j < nbLayers); ++j; {
//                         IloNumVarArray vars(m_env);
//                         IloNumArray vals(m_env);
//                         for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                             vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(u, v), j)]);
//                             vals.add(1.0);
//                             vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(v, u), j)]);
//                             vals.add(-1.0);
//                         }
//                         if (m_placement.m_inst.isNFV[u]) {
//                             if (j > 0) {
//                                 vars.add(m_a[getIndexA(u, j - 1)]);
//                                 vals.add(-1.0);
//                             }
//                             if (j < nbLayers - 1) {
//                                 vars.add(m_a[getIndexA(u, j)]);
//                                 vals.add(1.0);
//                             }
//                         }
//                         flowConsCons[n * j + u] = IloAdd(m_model,
//                             IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
//                     }
//                 }
//                 return flowConsCons;
//             }())
//             , m_linkCapaCons([&]() {
//                 IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
//                 for (int e(0); e < m_placement.m_inst.edges.size()); ++e; {
//                     linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
//                     setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
//                 }
//                 return IloAdd(m_model, linkCapaCons);
//             }())
//             , m_nodeCapaCons([&]() {
//                 IloRangeArray nodeCapaCons(m_env, n);
//                 for (const auto u : m_placement.m_inst.NFVNodes) {
//                     nodeCapaCons[u] = IloAdd(m_model,
//                         IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
//                     setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
//                 }
//                 return nodeCapaCons;
//             }())
//             , m_solver([&]() {
//                 IloCplex solver(m_model);
//                 solver.setParam(IloCplex::Threads, 1);
//                 solver.setParam(IloCplex::EpRHS, 1e-9);
//                 solver.setOut(m_env.getNullStream());
//                 solver.setWarning(m_env.getNullStream());
//                 return solver;
//             }()) {}

//         ~PricingProblem() {
//             m_env.end();
//         }

//         int getIndexF(const int _e, const int _j) const {
//             assert(0 <= _e && _e < m_placement.m_inst.network.size());
//             assert(0 <= _j && _j < nbLayers);
//             return m * _j + _e;
//         }

//         int getIndexA(const int _u, const int _j) const {
//             assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
//             assert(0 <= _j && _j < nbLayers);
//             assert(m_placement.m_inst.NFVIndices[_u] != -1);
//             return m_placement.m_inst.NFVNodes.size() * _j + m_placement.m_inst.NFVIndices[_u];
//         }

//         void updateModel(const int _i) {
//             m_solver.end();
//             int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
//             m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(0.0, 0.0);
//             m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(0.0, 0.0);

//             m_demandID = _i;
//             nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
//             m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(1.0, 1.0);
//             m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(-1.0, -1.0);

//             for (int e(0); e < m_placement.m_inst.edges.size()); ++e; {
//                 IloNumVarArray vars(m_env);
//                 IloNumArray vals(m_env);
//                 for (int j(0); j <= nbFunctions); ++j; {
//                     vars.add(m_f[getIndexF(e, j)]);
//                     vals.add(m_placement.m_inst.demands[m_demandID].d);
//                 }
//                 for (int j(nbFunctions + 1); j < nbLayers; ++j) {
//                     vars.add(m_f[getIndexF(e, j)]);
//                     vals.add(0);
//                 }
//                 m_linkCapaCons[e].setLinearCoefs(vars, vals);
//                 vars.end();
//                 vals.end();
//             }

//             for (const auto u : m_placement.m_inst.NFVNodes) {
//                 IloNumVarArray vars(m_env);
//                 IloNumArray vals(m_env);
//                 for (int j(0); j < nbFunctions); ++j; {
//                     vars.add(m_a[getIndexA(u, j)]);
//                     vals.add(m_placement.m_inst.nbCores(m_demandID, j));
//                 }
//                 for (int j(nbFunctions); j < nbLayers - 1); ++j; {
//                     vars.add(m_a[getIndexA(u, j)]);
//                     vals.add(0);
//                 }
//                 m_nodeCapaCons[u].setLinearCoefs(vars, vals);
//                 setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
//                 vars.end();
//                 vals.end();
//             }
//         }

//         bool solve() {
//             return m_solver.solve();
//         }

//         double getObjValue() const {
//             return m_solver.getObjValue();
//         }

//         void updateDual(const std::size_t _i) {
//             updateModel(_i);
//             const auto& d = m_placement.m_inst.demands[m_demandID].d;
//             const auto& functions = m_placement.m_inst.demands[m_demandID].functions;
//             const int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
//             IloNumVarArray vars(m_env, m * nbLayers + m_placement.m_inst.NFVNodes.size() * nbFunctions);
//             IloNumArray vals(m_env, m * nbLayers + m_placement.m_inst.NFVNodes.size() * nbFunctions);

//             int index = 0;
//             for (int e(0); e < m; ++e) {
//                 for (int j(0); j <= nbFunctions; ++j) {
//                     vars[index] = m_f[getIndexF(e, j)];
//                     vals[index] = m_placement.m_inst.energyCons.propLink * (d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]))
//                                   + d * m_placement.m_mainProblem.m_linkCapaDuals1[e]
//                                   - d * m_placement.m_mainProblem.m_linkCapaDuals2[e];
//                     ++index;
//                 }
//             }

//             for (const auto u : m_placement.m_inst.NFVNodes) {
//                 for (std::size_t j = 0; j < functions.size()); ++j; {
//                     vals[index] = -m_placement.m_mainProblem.m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]] * m_placement.m_inst.nbCores(m_demandID, j);
//                     vars[index] = m_a[getIndexA(u, j)];
//                     ++index;
//                 }
//                 for (int j(functions.size()); j < nbLayers - 1); ++j; {
//                     vals[index] = 0;
//                     vars[index] = m_a[getIndexA(u, j)];
//                     ++index;
//                 }
//             }
//             m_obj.setConstant(-m_placement.m_mainProblem.m_onePathDuals[m_demandID]);
//             m_obj.setLinearCoefs(vars, vals);
//             vars.end();
//             vals.end();
//             m_solver = IloCplex(m_model);
//             m_solver.setOut(m_env.getNullStream());
//         }

//         ServicePath getServicePath() const {
//             const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
//             const auto& t = m_placement.m_inst.demands[m_demandID].t;
//             std::vector<std::pair<Graph::Node, std::vector<FunctionDescriptor>>> installs;

//             Graph::Node u = m_placement.m_inst.demands[m_demandID].s;
//             std::size_t j = 0;
//             Graph::Path path{u};

//             IloNumArray aVal(m_env);
//             m_solver.getValues(aVal, m_a);

//             IloNumArray fVal(m_env);
//             m_solver.getValues(fVal, m_f);

//             while (u != t || j < funcs.size()); {
//                 if (j < int(funcs.size()) && m_placement.m_inst.isNFV[u] && aVal[getIndexA(u, j)]) {
//                     if (installs.empty() || installs.back().first != u) {
//                         installs.emplace_back(u, std::vector<FunctionDescriptor>());
//                     }
//                     installs.back().second.push_back(funcs[j]);
//                     ++j;
//                 } else {
//                     for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
//                         if (fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] > 0) {
//                             fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] = 0;
//                             path.push_back(v);
//                             u = v;
//                             break;
//                         }
//                     }
//                 }
//             }

//             assert(path.back() == t);
//             return ServicePath(path, installs);
//         }

//       private:
//         const ChainPlacement3& m_placement;
//         IloEnv m_env { };

//         int m_demandID;
//         const std::size_t n;
//         const std::size_t m;
//         const std::size_t nbLayers;
//         IloModel m_model;

//         IloObjective m_obj;

//         IloNumVarArray m_a;
//         IloNumVarArray m_f;

//         IloRangeArray m_flowConsCons;
//         IloRangeArray m_linkCapaCons;
//         IloRangeArray m_nodeCapaCons;

//         IloCplex m_solver;
//     };

//         //####################################################################################################################
//         //####################################################################################################################
//         //################################################# PricingProblemBF #################################################
//         //####################################################################################################################
//         //####################################################################################################################

//     class PricingProblemBF {
//         friend ChainPlacement3;

//         public:
//             PricingProblemBF(ChainPlacement3& _chainPlacement)
//                 : m_placement(_chainPlacement)
//                 , n(m_placement.m_inst.network.getOrder())
//                 , m(m_placement.m_inst.network.size())
//                 , nbLayers(m_placement.m_inst.maxChainSize + 1)
//                 , m_demandID(0)
//                 , m_layeredGraph([=]() {
//                     DiGraph<double> layeredGraph(n * nbLayers);
//                     for (const auto& u : m_placement.m_inst.NFVNodes) {
//                         for (std::size_t j = 0; j < nbLayers - 1; ++j) {
//                             layeredGraph.addEdge(n * j + u, n * (j + 1) + u);
//                         }
//                     }
//                     for (std::size_t j = 0; j < nbLayers; ++j) {
//                         for (const auto& edge : m_placement.m_inst.network.getEdges()) {
//                             layeredGraph.addEdge(n * j + edge.first, n * j + edge.second);
//                         }
//                     }
//                     return layeredGraph;
//                 }())
//                 , m_bf(m_layeredGraph)
//                 , m_path()
//                 , m_objValue(0) {}

//             void updateDual(const std::size_t _i) {
//             #if defined(LOG_LEVEL) && LOG_LEVEL <= 1
//                 std::cout << "updateDual(" << m_placement.m_inst.demands[_i] << ")\n";
//             #endif
//                 m_demandID = _i;

//                 for (const auto& u : m_placement.m_inst.NFVNodes) {
//                     for (std::size_t j = 0; j < nbLayers - 1; ++j) {
//                         m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
//                             -m_placement.m_mainProblem.m_inst.nodeCapaDuals[m_placement.m_inst.NFVIndices[u]]
//                                 * m_placement.m_inst.nbCores(m_demandID, j));
//                     }
//                 }
//                 const auto edges = m_placement.m_inst.network.getEdges();
//                 for (std::size_t j = 0; j < nbLayers; ++j) {
//                     for (std::size_t e = 0; e < m; ++e) {
//                         m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
//                             m_placement.m_inst.energyCons.propLink
//                                     * (m_placement.m_inst.demands[m_demandID].d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]))
//                                     + m_placement.m_inst.demands[m_demandID].d * m_placement.m_mainProblem.m_linkCapaDuals1[e]
//                                     - m_placement.m_inst.demands[m_demandID].d * m_placement.m_mainProblem.m_linkCapaDuals2[e]);
//                     }
//                 }
//             }

//             bool solve() {
//                 m_bf.clear();
//                 const Graph::Node dest = n * (m_placement.m_inst.demands[m_demandID].functions.size()) + m_placement.m_inst.demands[m_demandID].t;
//                 m_path = m_bf.getShortestPath(m_placement.m_inst.demands[m_demandID].s, dest);
//                 m_objValue = -m_placement.m_mainProblem.m_onePathDuals[m_demandID] + m_bf.getDistance(dest);
//                 return !m_path.empty();
//             }

//             double getObjValue() const {
//                 return m_objValue;
//             }

//             ServicePath getServicePath() const {
//                 Graph::Path realPath{m_path.front()};
//                 std::vector<std::pair<Graph::Node, std::vector<FunctionDescriptor>>> installedFunctions;

//                 for (auto iteU = m_path.begin(), iteV = std::next(iteU); iteV != m_path.end(); ++iteU, ++iteV) {
//                     const FunctionDescriptor layerU = *iteU / n,
//                                     layerV = *iteV / n;
//                     const Graph::Node baseU = *iteU % n,
//                                       baseV = *iteV % n;
//                     if (layerU == layerV) {
//                         assert(baseU != baseV);
//                         realPath.push_back(baseV);
//                     } else {
//                         if (installedFunctions.empty() || installedFunctions.back().first != baseU) { // First function or new node
//                             installedFunctions.emplace_back(baseU, std::vector<FunctionDescriptor>{m_placement.m_inst.demands[m_demandID].functions[layerU]});
//                         } else if (installedFunctions.back().first == baseU) { // Still same node
//                             installedFunctions.back().second.emplace_back(m_placement.m_inst.demands[m_demandID].functions[layerU]);
//                         }
//                     }
//                 }

//             #if !defined(NDEBUG)
//                 std::size_t nbFunctions = 0;
//                 for (const auto& installs : installedFunctions) {
//                     nbFunctions += installs.second.size();
//                 }
//                 assert([=]() {
//                     if (nbFunctions != m_placement.m_inst.demands[m_demandID].functions.size()); {
//                         std::cout << m_placement.m_inst.demands[m_demandID] << ": " << nbFunctions << " -> " << installedFunctions << '\n';
//                         return false;
//                     }
//                     return true;
//                 }());
//             #endif

//                 return ServicePath(realPath, installedFunctions);
//             }

//         private:
//             ChainPlacement3& m_placement;
//             std::size_t n;
//             std::size_t m;
//             std::size_t nbLayers;

//             int m_demandID;

//             DiGraph<double> m_layeredGraph;
//             ShortestPathBellmanFord<DiGraph<double>> m_bf;
//             Graph::Path m_path;
//             double m_objValue;
//     };

//   public:
//     explicit ChainPlacement3(Instance& _inst)
//         : m_inst(_inst)
//         , m_intObj(-1)
//         , m_fractObj(-1)
//         , m_mainProblem(*this)
//         , m_pool(std::max(1u, std::thread::hardware_concurrency()))
//         , m_threadID([&]() {
//             std::map<std::thread::id, std::size_t> pps;
//             std::size_t i = 0;
//             for (const auto& id : m_pool.getIds()) {
//                 pps.emplace(id, i);
//                 ++i;
//             }
//             return pps;
//         }())
//         , m_pps([&]() {
//             std::vector<PricingProblemBF> pps;
//             std::size_t concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
//             pps.reserve(concurentThreadsSupported);
//             for (std::size_t i = 0; i < concurentThreadsSupported; ++i) {
//                 pps.emplace_back(*this);
//             }
//             return pps;
//         }()) {
//     }

//     ChainPlacement3(const ChainPlacement3&) = default;
//     ChainPlacement3(ChainPlacement3&&) = default;
//     ChainPlacement3& operator=(const ChainPlacement3&) = default;
//     ChainPlacement3& operator=(ChainPlacement3&&) = default;

//     void addInitConf(const std::vector<ServicePath>& _paths) {
//         std::cout << "Adding initial configuration...." << std::flush;
//         std::size_t i = 0;
//         for (auto& path : _paths) {
//             m_mainProblem.addPath(path, i);
//             ++i;
//         }
//         std::cout << "done\n";
//     }

//     void addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths) {
//         std::cout << "Adding initial configuration...." << std::flush;
//         for (auto& path : _paths) {
//             m_mainProblem.addPath(path.first, path.second);
//         }
//         std::cout << "done\n";
//     }

//     std::vector<std::pair<ServicePath, int>> getPaths() const {
//         return m_mainProblem.m_paths;
//     }

//     void save(const std::string& _filename, const std::pair<double, double>& _time) {
//         std::ofstream ofs(_filename);
//         ofs << m_intObj << '\t' << m_fractObj << '\n';
//         ofs << m_mainProblem.m_paths.size() << std::endl;
//         ofs << _time.first << '\t' << _time.second << std::endl;

//         std::vector<double> linkUsage(m_inst.network.size(), 0);
//         std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
//         std::vector<int> pathsUsed(m_inst.demands.size());

//         for (std::size_t i = 0; i < m_mainProblem.m_paths.size(); ++i) {
//             if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) != 0) {
//                 const auto& path = m_mainProblem.m_paths[i].first.first;
//                 const auto& funcPlacement = m_mainProblem.m_paths[i].first.second;
//                 const auto& demand = m_inst.demands[m_mainProblem.m_paths[i].second];
//                 const auto d = demand.d;
//                 pathsUsed[m_mainProblem.m_paths[i].second] = i;

//                 for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
//                     linkUsage[m_inst.edgeID(*iteU, *iteV)] += d * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]);
//                 }
//                 std::size_t j = 0;
//                 for (const auto& pair_NodeFuncs : funcPlacement) {
//                     for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
//                         nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) * m_inst.nbCores(m_mainProblem.m_paths[i].second, j);
//                     }
//                 }
//             }
//         }

//         ofs << m_inst.network.getOrder() << '\n';
//         for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
//             ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
//         }

//         ofs << m_inst.network.size() << '\n';
//         std::size_t i = 0;
//         for (const auto& edge : m_inst.edges) {
//             ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge);
//             int xIndex;
//             if (edge.first < edge.second) {
//                 xIndex = i;
//             } else {
//                 xIndex = m_inst.edgeID(edge.second, edge.first);
//             }
//             ofs << '\t' << m_mainProblem.m_solver.getValue(m_mainProblem.m_x[xIndex]) << '\n';
//             ++i;
//         }

//         ofs << pathsUsed.size() << '\n';
//         for (const auto& index : pathsUsed) {
//             const auto& sPath = m_mainProblem.m_paths[index].first;
//             ofs << sPath.first.size() << '\t';
//             for (const auto& u : sPath.first) {
//                 ofs << u << '\t';
//             }
//             for (const auto& pair_NodeFuncs : sPath.second) {
//                 for (const auto& func : pair_NodeFuncs.second) {
//                     ofs << pair_NodeFuncs.first << '\t' << func << '\t';
//                 }
//             }
//             ofs << '\n';
//         }
//         std::cout << "Results saved to :" << _filename << '\n';
//     }

//     bool solveInteger() {
//         if (!solve()) {
//             return false;
//         }
//         m_intObj = m_mainProblem.solveInteger();
//         return m_intObj != -1;
//     }

//     bool solve() {
//         bool reducedCost;
//         std::vector<std::future<std::pair<ServicePath, std::size_t>>> futures;
//         futures.reserve(m_inst.demands.size());
//         do {
//             futures.clear();
//             reducedCost = false;
//             if (m_mainProblem.solve()) {
//                 m_fractObj = m_mainProblem.getObjValue();
//                 std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
//                 m_mainProblem.getDuals();
//                 assert(m_mainProblem.checkReducedCosts());
//                 for (std::size_t i = 0;
//                      i < m_inst.demands.size();
//                      ++i) {
//                     futures.emplace_back(
//                         m_pool.enqueue([&, this, i]() {
//                             // PricingProblem pp(*this);
//                             auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
//                             pp.updateDual(i);
//                             if (pp.solve() && pp.getObjValue() < -RC_EPS) {
//                                 return std::make_pair(pp.getServicePath(), i);
//                             } else {
//                                 return std::make_pair(ServicePath(), i);
//                             }
//                         }));
//                 }
//                 for (auto& fut : futures) {
//                     auto sPath = fut.get();
//                     if (!sPath.first.first.empty()) {
//                         reducedCost = true;
//                         m_mainProblem.addPath(sPath.first, sPath.second);
//                     }
//                 }
//             } else {
//                 m_mainProblem.m_solver.exportModel("rm.lp");
//                 std::cout << "No solution found for RMP" << '\n';
//             }
//         } while (reducedCost);
//         return m_fractObj != -1;
//     }

//   private:
//     Instance const* m_inst;
//     double m_intObj;
//     double m_fractObj;
//     ReducedMainProblem m_mainProblem;
//     ThreadPool m_pool;
//     std::map<std::thread::id, std::size_t> m_threadID;
//     std::vector<PricingProblemBF> m_pps;

//     friend ReducedMainProblem;
//     friend PricingProblem;
//     friend PricingProblemBF;
// };
// } // namespace Energy

namespace SFC::Energy {
class ChainPlacement3 {
    class ReducedMainProblem {
        friend ChainPlacement3;

      public:
        ReducedMainProblem(const ChainPlacement3& _placement);
        ReducedMainProblem& operator=(const ReducedMainProblem&) = delete;
        ReducedMainProblem(const ReducedMainProblem&) = delete;
        ~ReducedMainProblem();

        void addCut(const GenCut::Column& _col);
        void checkReducedCosts() const;
        void addPath(const ServicePath& _sPath, const int _i);
        double solveInteger();
        void getNetworkUsage() const;
        void getDuals();
        bool solve();
        double getObjValue() const;

        inline void setEdge(const Graph::Edge& _edge, bool _state);

      private:
        const ChainPlacement3& m_placement;
        IloEnv m_env{};
        IloModel m_model;

        IloNumVarArray m_y;
        IloNumVarArray m_x;
        IloNumVarArray m_k;

        IloObjective m_obj;

        IloRangeArray m_onePathCons;
        IloRangeArray m_linkCapasCons;

        IloRangeArray m_nodeCapasCons1;
        IloRangeArray m_nodeCapasCons2;

        IloNumArray m_linkCapasDuals;
        IloNumArray m_nodeCapasDuals;
        IloNumArray m_onePathDuals;

        IloNumArray m_valsLink;
        IloNumArray m_valsNode;

        IloCplex m_solver;

        std::vector<std::pair<ServicePath, int>> m_paths;
    };

    class PricingProblem {
        friend ChainPlacement3;

      public:
        PricingProblem(const ChainPlacement3& _chainPlacement);
        ~PricingProblem();
        void updateModel(const int _i);
        void updateDual(const std::size_t _i);
        bool solve();
        double getObjValue() const;
        ServicePath getServicePath() const;

      private:
        const ChainPlacement3& m_placement;
        IloEnv m_env{};

        int m_demandID;
        const std::size_t n;
        const std::size_t m;
        const std::size_t nbLayers;
        IloModel m_model;

        IloObjective m_obj;

        IloNumVarArray m_a;
        IloNumVarArray m_f;

        IloRangeArray m_flowConsCons;
        IloRangeArray m_linkCapaCons;
        IloRangeArray m_nodeCapaCons;

        IloCplex m_solver;

        int getIndexF(const int _e, const int _j) const;
        int getIndexA(const int _u, const int _j) const;
    };

    class PricingProblemBF {
      public:
        explicit PricingProblemBF(ChainPlacement3& _chainPlacement);
        PricingProblemBF(const PricingProblemBF&) = default;
        PricingProblemBF(PricingProblemBF&&) = default;
        PricingProblemBF& operator=(const PricingProblemBF&) = default;
        PricingProblemBF& operator=(PricingProblemBF&&) = default;
        ~PricingProblemBF() = default;

        void updateDual(std::size_t _i);
        bool solve();
        double getObjValue() const;
        ServicePath getServicePath() const;

      private:
        ChainPlacement3& m_placement;
        std::size_t n;
        std::size_t m;
        std::size_t nbLayers;

        int m_demandID;

        DiGraph<double> m_layeredGraph;
        ShortestPathBellmanFord<DiGraph<double>> m_bf;
        Graph::Path m_path;
        double m_objValue;
    };

  public:
    ChainPlacement3(Instance& _inst);
    ChainPlacement3(const ChainPlacement3&) = default;
    ChainPlacement3& operator=(const ChainPlacement3&) = default;
    ChainPlacement3(ChainPlacement3&&) = default;
    ChainPlacement3& operator=(ChainPlacement3&&) = default;
    ~ChainPlacement3() = default;
    void addInitConf(const std::vector<ServicePath>& _paths);
    void addCut(const GenCut::Column& _col);
    void addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths);
    std::vector<std::pair<ServicePath, int>> getPaths() const;
    void save(const std::string& _filename, const std::pair<double, double>& _time);
    bool solveInteger();
    void setEdge(const Graph::Edge& _edge, bool _state);
    bool solve();

  private:
    Instance const* m_inst;

    double m_intObj;
    double m_fractObj;
    ReducedMainProblem m_mainProblem;
    ThreadPool m_pool;
    std::map<std::thread::id, std::size_t> m_threadID;
    std::vector<PricingProblemBF> m_pps;

    friend ReducedMainProblem;
    friend PricingProblem;
    friend PricingProblemBF;
};

ChainPlacement3::ChainPlacement3(Instance& _inst)
    : m_inst(_inst)
    , m_intObj(-1)
    , m_fractObj(-1)
    , m_mainProblem(*this)
    , m_pool(std::max(1u, std::thread::hardware_concurrency()))
    , m_threadID([&]() {
        std::map<std::thread::id, std::size_t> pps;
        std::size_t i = 0;
        for (const auto& id : m_pool.getIds()) {
            pps.emplace(id, i);
            ++i;
        }
        return pps;
    }())
    , m_pps([&]() {
        std::vector<PricingProblemBF> pps;
        std::size_t concurentThreadsSupported = std::max(1u, std::thread::hardware_concurrency());
        pps.reserve(concurentThreadsSupported);
        for (std::size_t i = 0; i < concurentThreadsSupported; ++i) {
            pps.emplace_back(*this);
        }
        return pps;
    }()) {}

void ChainPlacement3::setEdge(const Graph::Edge& _edge, bool _state) {
    m_mainProblem.setEdge(_edge, _state);
}

void ChainPlacement3::addInitConf(const std::vector<ServicePath>& _paths) {
    std::cout << "Adding initial configuration...." << std::flush;
    std::size_t i = 0;
    for (auto& path : _paths) {
        m_mainProblem.addPath(path, i);
        ++i;
    }
    std::cout << "done\n";
}

void ChainPlacement3::addCut(const GenCut::Column& _col) {
    m_mainProblem.addCut(_col);
}

void ChainPlacement3::addInitConf(const std::vector<std::pair<ServicePath, int>>& _paths) {
    std::cout << "Adding initial configuration...." << std::flush;
    for (auto& path : _paths) {
        m_mainProblem.addPath(path.first, path.second);
    }
    std::cout << "done\n";
}

std::vector<std::pair<ServicePath, int>> ChainPlacement3::getPaths() const {
    return m_mainProblem.m_paths;
}

void ChainPlacement3::save(const std::string& _filename, const std::pair<double, double>& _time) {
    std::ofstream ofs(_filename);
    ofs << m_intObj << '\t' << m_fractObj << '\n';
    ofs << m_mainProblem.m_paths.size() << std::endl;
    ofs << _time.first << '\t' << _time.second << std::endl;

    std::vector<double> linkUsage(m_inst.network.size(), 0);
    std::vector<int> nodeUsage(m_inst.network.getOrder(), 0);
    std::vector<int> pathsUsed(m_inst.demands.size());

    for (std::size_t i = 0; i < m_mainProblem.m_paths.size(); ++i) {
        if (m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]) > 0.0) {
            const auto& path = m_mainProblem.m_paths[i].first.first;
            const auto& funcPlacement = m_mainProblem.m_paths[i].first.second;
            const auto& demand = m_inst.demands[m_mainProblem.m_paths[i].second];
            const auto d = demand.d;
            pathsUsed[m_mainProblem.m_paths[i].second] = i;

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_inst.edgeID(*iteU, *iteV)] += d
                                                          * m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i]);
            }
            std::size_t j = 0;
            for (const auto& pair_NodeFuncs : funcPlacement) {
                for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                    nodeUsage[pair_NodeFuncs.first] += m_mainProblem.m_solver.getValue(m_mainProblem.m_y[i])
                                                       * m_inst.nbCores(m_mainProblem.m_paths[i].second, j);
                }
            }
        }
    }

    ofs << m_inst.network.getOrder() << '\n';
    for (Graph::Node u = 0; u < m_inst.network.getOrder(); ++u) {
        ofs << u << '\t' << nodeUsage[u] << '\t' << m_inst.nodeCapa[u] << '\n';
    }

    ofs << m_inst.network.size() << '\n';
    std::size_t i = 0;
    for (const auto& edge : m_inst.edges) {
        ofs << std::fixed << edge.first << '\t' << edge.second << '\t' << linkUsage[i] << '\t' << m_inst.network.getEdgeWeight(edge);
        int xIndex;
        if (edge.first < edge.second) {
            xIndex = i;
        } else {
            xIndex = m_inst.edgeID(edge.second, edge.first);
        }
        ofs << '\t' << m_mainProblem.m_solver.getValue(m_mainProblem.m_x[xIndex]) << '\n';
        ++i;
    }

    ofs << pathsUsed.size() << '\n';
    for (const auto& index : pathsUsed) {
        const auto& sPath = m_mainProblem.m_paths[index].first;
        ofs << sPath.first.size() << '\t';
        for (const auto& u : sPath.first) {
            ofs << u << '\t';
        }
        for (const auto& pair_NodeFuncs : sPath.second) {
            for (const auto& func : pair_NodeFuncs.second) {
                ofs << pair_NodeFuncs.first << '\t' << func << '\t';
            }
        }
        ofs << '\n';
    }
    std::cout << "Results saved to :" << _filename << '\n';
}

bool ChainPlacement3::solveInteger() {
    if (solve()) {
        m_intObj = m_mainProblem.solveInteger();
        return m_intObj != -1;
    } else {
        return false;
    }
}

bool ChainPlacement3::solve() {
    bool reducedCost;
    std::vector<std::future<std::pair<ServicePath, std::size_t>>> futures;
    futures.reserve(m_inst.demands.size());
    do {
        futures.clear();
        reducedCost = false;
        if (m_mainProblem.solve()) {
            m_fractObj = m_mainProblem.getObjValue();
            std::cout << "Reduced Main Problem Objective Value = " << m_fractObj << '\n';
            std::cout << "Searching for paths..." << std::flush;
            m_mainProblem.getDuals();
            for (std::size_t i = 0;
                 i < m_inst.demands.size();
                 ++i) {
                futures.emplace_back(
                    m_pool.enqueue([&, this, i]() {
                        // PricingProblem pp(*this);
                        auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                        pp.updateDual(i);
                        if (pp.solve() && pp.getObjValue() < -RC_EPS) {
                            return std::make_pair(pp.getServicePath(), i);
                        } else {
                            return std::make_pair(ServicePath(), i);
                        }
                    }));
            }
            std::cout << "getting paths..." << std::flush;
            for (auto& fut : futures) {
                auto sPath = fut.get();
                if (!sPath.first.first.empty()) {
                    reducedCost = true;
                    m_mainProblem.addPath(sPath.first, sPath.second);
                }
            }
            std::cout << "done!\n";
        } else {
            m_mainProblem.m_solver.exportModel("rm.lp");
            std::cout << "No solution found for RMP" << '\n';
        }
    } while (reducedCost);
    return m_fractObj != -1;
}

//####################################################################################################################
//####################################################################################################################
//################################################ ReducedMainProblem ################################################
//####################################################################################################################
//####################################################################################################################

ChainPlacement3::ReducedMainProblem::ReducedMainProblem(const ChainPlacement3& _placement)
    : m_placement(_placement)
    , m_env()
    , m_model(m_env)
    , m_y(IloAdd(m_model, IloNumVarArray(m_env)))
    , m_x([&]() {
        IloNumVarArray x(m_env, m_placement.m_inst.network.size());
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            if (edge.first < edge.second) {
                x[e] = IloAdd(m_model, IloNumVar(m_env, 0.0, 1.0));
                setIloName(x[e], "x" + toString(edge));
            }
            ++e;
        }
        return x;
    }())
    , m_k([&]() {
        IloNumVarArray k(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            k[m_placement.m_inst.NFVIndices[u]] = IloNumVar(m_env, 0.0, IloInfinity);
            setIloName(k[m_placement.m_inst.NFVIndices[u]], "k(" + toString(u) + ")");
        }
        return IloAdd(m_model, k);
    }())
    , m_obj([&]() {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            if (edge.first < edge.second) {
                vars.add(m_x[e]);
                vals.add(2 * m_placement.m_inst.energyCons.activeLink);
            }
            ++e;
        }
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            vars.add(m_k[m_placement.m_inst.NFVIndices[u]]);
            vals.add(m_placement.m_inst.energyCons.activeCore);
        }
        auto obj = IloAdd(m_model, IloMinimize(m_env, IloScalProd(vars, vals)));
        vars.end();
        vals.end();
        return obj;
    }())
    , m_onePathCons([&]() {
        IloRangeArray onePathCons(m_env, m_placement.m_inst.demands.size(), 1.0, 1.0);
        for (std::size_t i = 0; i < m_placement.m_inst.demands.size(); ++i) {
            setIloName(onePathCons[i], "onePathCons" + toString(m_placement.m_inst.demands[i]));
        }
        return IloAdd(m_model, onePathCons);
    }())
    , m_linkCapasCons([&]() {
        IloRangeArray linkCapaCons(m_env, m_placement.m_inst.network.size(), -IloInfinity, 0.0);
        int e = 0;
        for (const Graph::Edge& edge : m_placement.m_inst.edges) {
            const int realE = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
            linkCapaCons[e].setLinearCoef(m_x[realE], -m_placement.m_inst.network.getEdgeWeight(edge));
            setIloName(linkCapaCons[e], "linkCapasCons" + toString(edge));
            ++e;
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapasCons1([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            const int i = m_placement.m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model, IloRange(m_env, -IloInfinity, -m_k[i], 0.0));
            setIloName(nodeCapasCons[i], "nodeCapasCons1" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_nodeCapasCons2([&]() {
        IloRangeArray nodeCapasCons(m_env, m_placement.m_inst.NFVNodes.size());
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            const int i = m_placement.m_inst.NFVIndices[u];
            nodeCapasCons[i] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_k[i], m_placement.m_inst.nodeCapa[u]));
            setIloName(nodeCapasCons[i], "nodeCapasCons2" + std::to_string(u));
        }
        return nodeCapasCons;
    }())
    , m_linkCapasDuals(m_env)
    , m_nodeCapasDuals(m_env)
    , m_onePathDuals(m_env)
    , m_valsLink(m_env, m_placement.m_inst.network.size())
    , m_valsNode(m_env, m_placement.m_inst.NFVNodes.size())
    , m_solver([&]() {
        IloCplex solver(m_model);
        // solver.setParam(IloCplex::EpGap, 0.0077);
        // solver.setParam(IloCplex::VarSel, 3);
        // solver.setParam(IloCplex::Threads, 1);
        solver.setParam(IloCplex::EpRHS, 1e-9);
        solver.setOut(m_env.getNullStream());
        return solver;
    }())
    , m_paths() {
}

ChainPlacement3::ReducedMainProblem::~ReducedMainProblem() {
    m_env.end();
}

void ChainPlacement3::ReducedMainProblem::setEdge(const Graph::Edge& _edge, bool _state) {
    assert(_edge.first < _edge.second);
    m_x[m_placement.m_inst.edgeID(_edge.first, _edge.second)].setBounds(_state, _state);
}

void ChainPlacement3::ReducedMainProblem::addCut(const GenCut::Column& _col) {
    const auto& edges = m_placement.m_inst.network.getEdges();
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);
    for (const int e : _col.edgeIDs) {
        const int realE = edges[e].first < edges[e].second ? e : m_placement.m_inst.edgeID(edges[e].second, edges[e].first);
        vars.add(m_x[realE]);
        vals.add(1.0);
    }
    IloRange cut = IloScalProd(vars, vals) >= _col.minEdge;
    vars.end();
    vals.end();
    std::cout << cut << '\n';
    m_model.add(cut);
}

void ChainPlacement3::ReducedMainProblem::checkReducedCosts() const {
    for (int k = 0; k < m_paths.size())
        ;
    ++k;
    {
        const int i = m_paths[k].second;
        double myRC = -m_onePathDuals[i];

        for (auto iteU = m_paths[k].first.first.begin(), iteV = std::next(iteU);
             iteV != m_paths[k].first.first.end(); ++iteU, ++iteV) {
            const int e = m_placement.m_inst.edgeID(*iteU, *iteV);

            myRC += m_placement.m_inst.energyCons.propLink
                        * (m_placement.m_inst.demands[i].d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV))
                    - m_placement.m_inst.demands[i].d * m_linkCapasDuals[e];
        }

        std::size_t j = 0;
        for (const auto& pair_NodeFuncs : m_paths[k].first.second) {
            for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
                const int u = pair_NodeFuncs.first;
                myRC += -m_nodeCapasDuals[m_placement.m_inst.NFVIndices[u]]
                        * m_placement.m_inst.nbCores(i, j);
            }
        }
        assert(std::abs(myRC - m_solver.getReducedCost(m_y[k])) < RC_EPS || (std::cout << myRC << ' ' << m_solver.getReducedCost(m_y[k]) << '\n', false));
    }
}

void ChainPlacement3::ReducedMainProblem::addPath(const ServicePath& _sPath, const int _i) {
    const auto pair = std::make_pair(_sPath, _i);
    assert([&]() {
        const auto ite = std::find(m_paths.begin(), m_paths.end(), pair);
        if (ite == m_paths.end()) {
            return true;
        } else {
            std::cout << pair << " is already present!" << '\n';
            return false;
        }
    }());

    const auto& path = _sPath.first;
    const auto& funcPlacement = _sPath.second;
    const auto& demand = m_placement.m_inst.demands[_i];
    const auto d = demand.d;

    assert(path.front() == demand.s);
    assert(path.back() == demand.t);

    double objCoef = 0.0;
    for (int e(0); e < m_placement.m_inst.network.size())
        ;
    ++e;
    {
        m_valsLink[e] = 0;
    }
    for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
        m_valsLink[m_placement.m_inst.edgeID(*iteU, *iteV)] += d;
        objCoef += m_placement.m_inst.energyCons.propLink
                   * (d / m_placement.m_inst.network.getEdgeWeight(*iteU, *iteV));
    }
    for (std::size_t i = 0; i < m_valsNode.getSize())
        ;
    ++i;
    {
        m_valsNode[i] = 0.0;
    }
    std::size_t j = 0;
    for (const auto& pair_NodeFuncs : funcPlacement) {
        for (const std::size_t nbFunc = pair_NodeFuncs.second.size() + j; j < nbFunc; ++j) {
            assert(0 <= pair_NodeFuncs.first && pair_NodeFuncs.first < m_placement.m_inst.network.getOrder());
            assert(std::find(path.begin(), path.end(), pair_NodeFuncs.first) != path.end());
            m_valsNode[m_placement.m_inst.NFVIndices[pair_NodeFuncs.first]] += m_placement.m_inst.nbCores(_i, j);
        }
    }

    IloNumColumn cplexCol = m_onePathCons[_i](1.0)
                            + m_linkCapasCons(m_valsLink)
                            + m_nodeCapasCons1(m_valsNode)
                            + m_obj(objCoef);
    auto yVar = IloNumVar(cplexCol);
    setIloName(yVar, "y" + toString(std::make_tuple(demand, _sPath)));
    m_y.add(yVar);
    m_paths.emplace_back(_sPath, _i);
}

double ChainPlacement3::ReducedMainProblem::solveInteger() {
    m_model.add(IloConversion(m_env, m_y, ILOBOOL));
    m_model.add(IloConversion(m_env, m_k, ILOINT));

    int e = 0;
    for (const Graph::Edge& edge : m_placement.m_inst.edges) {
        if (edge.first < edge.second) {
            m_model.add(IloConversion(m_env, m_x[e], ILOBOOL));
        }
        ++e;
    }

    m_solver.extract(m_model);
    if (m_solver.solve()) {
        double obj = m_solver.getObjValue();
        std::cout << "Final obj value: " << obj << "\tFrac Obj Value: "
                  << m_placement.m_fractObj << "\t# Paths: " << m_paths.size() << '\n';
        getNetworkUsage();
        return obj;
    } else {
        m_solver.exportModel("mp.sav");
        return -1.0;
    }
}

void ChainPlacement3::ReducedMainProblem::getNetworkUsage() const {
    std::vector<double> linkUsage(m_placement.m_inst.network.size(), 0);
    std::vector<IloNumVar> pathsUsed(m_placement.m_inst.demands.size(), nullptr);
    std::vector<int> active(m_placement.m_inst.network.size(), 0);

    for (std::size_t i = 0; i < m_paths.size(); ++i) {
        if (m_solver.getValue(m_y[i]) > 0.0) {
            const auto& path = m_paths[i].first.first;
            const auto& demand = m_placement.m_inst.demands[m_paths[i].second];
            const auto d = demand.d;
            pathsUsed[m_paths[i].second] = m_y[i];

            for (auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                linkUsage[m_placement.m_inst.edgeID(*iteU, *iteV)] += d * m_solver.getValue(m_y[i]);
                active[m_placement.m_inst.edgeID(*iteU, *iteV)] = 1;
                active[m_placement.m_inst.edgeID(*iteV, *iteU)] = 1;
            }
        }
    }

    // Get node usage
    IloNumArray nodeUsage(m_env);
    m_solver.getValues(nodeUsage, m_k);
    for (const auto& u : m_placement.m_inst.NFVNodes) {
        nodeUsage[u] = m_solver.getValue(m_k[m_placement.m_inst.NFVIndices[u]]);
    }
    std::cout << "Node usage: \n";
    double nodeEnergy = 0;
    for (const auto& u : m_placement.m_inst.NFVNodes) {
        nodeEnergy += nodeUsage[u] * m_placement.m_inst.energyCons.activeCore;
        std::cout << '\t' << u << " -> " << nodeUsage[u] << " / " << m_placement.m_inst.nodeCapa[u]
                  << " -> " << nodeUsage[u] * m_placement.m_inst.energyCons.activeCore << '\n';
        assert(nodeUsage[u] <= m_placement.m_inst.nodeCapa[u]);
    }
    std::cout << '\t' << nodeEnergy << '\n';

    // Get link usage
    double linkEnergy = 0.0;
    std::cout << "Link usage: \n";
    int e = 0;
    for (const auto& edge : m_placement.m_inst.edges) {
        int realE = edge.first < edge.second ? e : m_placement.m_inst.edgeID(edge.second, edge.first);
        double energy = m_placement.m_inst.energyCons.propLink
                        * linkUsage[e] / m_placement.m_inst.network.getEdgeWeight(edge);

        if (active[e]) {
            energy += m_placement.m_inst.energyCons.activeLink;
        }
        linkEnergy += energy;
        std::cout << std::fixed << '\t' << edge << " -> "
                  << linkUsage[e] << " / " << m_placement.m_inst.network.getEdgeWeight(edge)
                  << " -> " << energy << " ->" << active[e] << "/" << m_solver.getValue(m_x[realE]) << '\n';
        assert(m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[e] > -1.0e-6
               || (std::cerr << m_placement.m_inst.network.getEdgeWeight(edge) - linkUsage[e] << '\n', false));
        ++e;
    }
    std::cout << nodeEnergy << " + " << linkEnergy << " = " << nodeEnergy + linkEnergy << '\n';
}

void ChainPlacement3::ReducedMainProblem::getDuals() {
    m_solver.getDuals(m_linkCapasDuals, m_linkCapasCons);
    m_solver.getDuals(m_nodeCapasDuals, m_nodeCapasCons1);
    m_solver.getDuals(m_onePathDuals, m_onePathCons);
}

bool ChainPlacement3::ReducedMainProblem::solve() {
    return m_solver.solve();
}

double ChainPlacement3::ReducedMainProblem::getObjValue() const {
    return m_solver.getObjValue();
}
//####################################################################################################################
//####################################################################################################################
//################################################# PricingProblemBF #################################################
//####################################################################################################################
//####################################################################################################################

ChainPlacement3::PricingProblemBF::PricingProblemBF(ChainPlacement3& _chainPlacement)
    : m_placement(_chainPlacement)
    , n(m_placement.m_inst.network.getOrder())
    , m(m_placement.m_inst.network.size())
    , nbLayers(m_placement.m_inst.maxChainSize + 1)
    , m_demandID(0)
    , m_layeredGraph([=]() {
        DiGraph<double> layeredGraph(n * nbLayers);
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            for (std::size_t j = 0; j < nbLayers - 1; ++j) {
                layeredGraph.addEdge(n * j + u, n * (j + 1) + u);
            }
        }
        for (std::size_t j = 0; j < nbLayers; ++j) {
            for (const auto& edge : m_placement.m_inst.network.getEdges()) {
                layeredGraph.addEdge(n * j + edge.first, n * j + edge.second);
            }
        }
        return layeredGraph;
    }())
    , m_bf(m_layeredGraph)
    , m_path()
    , m_objValue(0) {}

void ChainPlacement3::PricingProblemBF::updateDual(const std::size_t _i) {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
    std::cout << "updateDual(" << m_placement.m_inst.demands[_i] << ")\n";
#endif
    m_demandID = _i;

    for (const auto& u : m_placement.m_inst.NFVNodes) {
        for (std::size_t j = 0; j < nbLayers - 1; ++j) {
            m_layeredGraph.setEdgeWeight(n * j + u, n * (j + 1) + u,
                -m_placement.m_mainProblem.m_nodeCapasDuals[m_placement.m_inst.NFVIndices[u]]
                    * m_placement.m_inst.nbCores(m_demandID, j));
        }
    }
    const auto edges = m_placement.m_inst.network.getEdges();
    for (std::size_t j = 0; j < nbLayers; ++j) {
        for (std::size_t e = 0; e < m; ++e) {
            m_layeredGraph.setEdgeWeight(n * j + edges[e].first, n * j + edges[e].second,
                m_placement.m_inst.energyCons.propLink
                        * (m_placement.m_inst.demands[m_demandID].d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]))
                    - m_placement.m_inst.demands[m_demandID].d * m_placement.m_mainProblem.m_linkCapasDuals[e]);
        }
    }
}

bool ChainPlacement3::PricingProblemBF::solve() {
    m_bf.clear();
    const Graph::Node dest = n * (m_placement.m_inst.demands[m_demandID].functions.size()) + m_placement.m_inst.demands[m_demandID].t;
    m_path = m_bf.getShortestPath(m_placement.m_inst.demands[m_demandID].s, dest);
    m_objValue = -m_placement.m_mainProblem.m_onePathDuals[m_demandID] + m_bf.getDistance(dest);
    return !m_path.empty();
}

double ChainPlacement3::PricingProblemBF::getObjValue() const {
    return m_objValue;
}

ServicePath ChainPlacement3::PricingProblemBF::getServicePath() const {
    Graph::Path realPath{m_path.front()};
    std::vector<std::pair<Graph::Node, std::vector<FunctionDescriptor>>> installedFunctions;

    for (auto iteU = m_path.begin(), iteV = std::next(iteU); iteV != m_path.end(); ++iteU, ++iteV) {
        const FunctionDescriptor layerU = *iteU / n,
                                 layerV = *iteV / n;
        const Graph::Node baseU = *iteU % n,
                          baseV = *iteV % n;
        if (layerU == layerV) {
            assert(baseU != baseV);
            realPath.push_back(baseV);
        } else {
            if (installedFunctions.empty() || installedFunctions.back().first != baseU) { // First function or new node
                installedFunctions.emplace_back(baseU, std::vector<FunctionDescriptor>{m_placement.m_inst.demands[m_demandID].functions[layerU]});
            } else if (installedFunctions.back().first == baseU) { // Still same node
                installedFunctions.back().second.emplace_back(m_placement.m_inst.demands[m_demandID].functions[layerU]);
            }
        }
    }

#if !defined(NDEBUG)
    std::size_t nbFunctions = 0;
    for (const auto& installs : installedFunctions) {
        nbFunctions += installs.second.size();
    }
    assert([=]() {
        if (nbFunctions != m_placement.m_inst.demands[m_demandID].functions.size())
            ;
        {
            std::cout << m_placement.m_inst.demands[m_demandID] << ": " << nbFunctions << " -> " << installedFunctions << '\n';
            return false;
        }
        return true;
    }());
#endif

    return ServicePath(realPath, installedFunctions);
}

//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################
//####################################################################################################################

ChainPlacement3::PricingProblem::PricingProblem(const ChainPlacement3& _chainPlacement)
    : m_placement(_chainPlacement)
    , m_env()
    , m_demandID(0)
    , n(m_placement.m_inst.network.getOrder())
    , m(m_placement.m_inst.network.size())
    , nbLayers(m_placement.m_inst.maxChainSize + 1)
    , m_model(m_env)
    , m_obj(IloAdd(m_model, IloMinimize(m_env)))
    , m_a([&]() {
        IloNumVarArray a(m_env, m_placement.m_inst.NFVNodes.size() * (nbLayers - 1));
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            for (int j(0); j < nbLayers - 1)
                ;
            ++j;
            {
                a[getIndexA(u, j)] = IloAdd(m_model,
                    IloNumVar(m_env, 0, 1, ILOBOOL));
                setIloName(a[getIndexA(u, j)], "a" + toString(std::make_tuple(j, u)));
            }
        }
        return a;
    }())
    , m_f([&]() {
        IloNumVarArray f(m_env, m * nbLayers, 0, 1, ILOBOOL);
        for (int e(0); e < m; ++e) {
            for (int j(0); j < nbLayers)
                ;
            ++j;
            {
                setIloName(f[getIndexF(e, j)],
                    "f" + toString(std::make_tuple(m_placement.m_inst.edges[e], j)));
            }
        }
        return IloAdd(m_model, f);
    }())
    , m_flowConsCons([&]() {
        IloRangeArray flowConsCons(m_env, n * nbLayers, 0.0, 0.0);
        for (Graph::Node u(0); u < m_placement.m_inst.network.getOrder())
            ;
        ++u;
        {
            for (int j(0); j < nbLayers)
                ;
            ++j;
            {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(u, v), j)]);
                    vals.add(1.0);
                    vars.add(m_f[getIndexF(m_placement.m_inst.edgeID(v, u), j)]);
                    vals.add(-1.0);
                }
                if (m_placement.m_inst.isNFV[u]) {
                    if (j > 0) {
                        vars.add(m_a[getIndexA(u, j - 1)]);
                        vals.add(-1.0);
                    }
                    if (j < nbLayers - 1) {
                        vars.add(m_a[getIndexA(u, j)]);
                        vals.add(1.0);
                    }
                }
                flowConsCons[n * j + u] = IloAdd(m_model,
                    IloRange(m_env, 0.0, IloScalProd(vars, vals), 0.0));
            }
        }
        return flowConsCons;
    }())
    , m_linkCapaCons([&]() {
        IloRangeArray linkCapaCons(m_env, m, 0.0, 0.0);
        for (int e(0); e < m_placement.m_inst.edges.size())
            ;
        ++e;
        {
            linkCapaCons[e].setUB(m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]));
            setIloName(linkCapaCons[e], "linkCapaCons(" + toString(e) + ")");
        }
        return IloAdd(m_model, linkCapaCons);
    }())
    , m_nodeCapaCons([&]() {
        IloRangeArray nodeCapaCons(m_env, n);
        for (const auto& u : m_placement.m_inst.NFVNodes) {
            nodeCapaCons[u] = IloAdd(m_model,
                IloRange(m_env, 0.0, m_placement.m_inst.nodeCapa[u]));
            setIloName(nodeCapaCons[u], "nodeCapaCons(" + std::to_string(u) + ")");
        }
        return nodeCapaCons;
    }())
    , m_solver([&]() {
        IloCplex solver(m_model);
        solver.setParam(IloCplex::Threads, 1);
        solver.setOut(m_env.getNullStream());
        solver.setWarning(m_env.getNullStream());
        return solver;
    }()) {}

ChainPlacement3::PricingProblem::~PricingProblem() {
    m_env.end();
}

int ChainPlacement3::PricingProblem::getIndexF(const int _e, const int _j) const {
    assert(0 <= _e && _e < m_placement.m_inst.network.size());
    assert(0 <= _j && _j < nbLayers);
    return m * _j + _e;
}

int ChainPlacement3::PricingProblem::getIndexA(const int _u, const int _j) const {
    assert(0 <= _u && _u < m_placement.m_inst.network.getOrder());
    assert(0 <= _j && _j < nbLayers);
    assert(m_placement.m_inst.NFVIndices[_u] != -1);
    return m_placement.m_inst.NFVNodes.size() * _j + m_placement.m_inst.NFVIndices[_u];
}

void ChainPlacement3::PricingProblem::updateModel(const int _i) {
    int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(0.0, 0.0);
    m_flowConsCons[n * nbFunctions + m_placement.m_inst.demands[m_demandID].t].setBounds(0.0, 0.0);

    m_demandID = _i;
    nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    m_flowConsCons[n * 0 + m_placement.m_inst.demands[m_demandID].s].setBounds(1.0, 1.0);
    m_flowConsCons[n * nbFunctions
                   + m_placement.m_inst.demands[m_demandID].t]
        .setBounds(-1.0, -1.0);

    for (int e(0); e < m_placement.m_inst.edges.size())
        ;
    ++e;
    {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j(0); j <= nbFunctions)
            ;
        ++j;
        {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_placement.m_inst.demands[m_demandID].d);
        }
        for (int j(nbFunctions + 1); j < nbLayers; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(0);
        }
        m_linkCapaCons[e].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
    }

    for (const auto& u : m_placement.m_inst.NFVNodes) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
        for (int j(0); j < nbFunctions)
            ;
        ++j;
        {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(m_placement.m_inst.nbCores(m_demandID, j));
        }
        for (int j(nbFunctions); j < nbLayers - 1)
            ;
        ++j;
        {
            vars.add(m_a[getIndexA(u, j)]);
            vals.add(0);
        }
        m_nodeCapaCons[u].setLinearCoefs(vars, vals);
        vars.end();
        vals.end();
        setIloName(m_nodeCapaCons[u], "m_nodeCapaCons(" + std::to_string(u) + ")");
    }
}

void ChainPlacement3::PricingProblem::updateDual(const std::size_t _i) {
    updateModel(_i);
    const auto& d = m_placement.m_inst.demands[m_demandID].d;
    const auto& functions = m_placement.m_inst.demands[m_demandID].functions;
    const int nbFunctions = m_placement.m_inst.demands[m_demandID].functions.size();
    IloNumVarArray vars(m_env);
    IloNumArray vals(m_env);

    for (int e(0); e < m; ++e) {
        for (int j(0); j <= nbFunctions; ++j) {
            vars.add(m_f[getIndexF(e, j)]);
            vals.add(m_placement.m_inst.energyCons.propLink
                         * (d / m_placement.m_inst.network.getEdgeWeight(m_placement.m_inst.edges[e]))
                     - d * m_placement.m_mainProblem.m_linkCapasDuals[e]);
        }
    }

    for (const auto& u : m_placement.m_inst.NFVNodes) {
        for (std::size_t j = 0; j < functions.size())
            ;
        ++j;
        {
            vals.add(-m_placement.m_mainProblem.m_nodeCapasDuals[m_placement.m_inst.NFVIndices[u]]
                     * m_placement.m_inst.nbCores(m_demandID, j));
            vars.add(m_a[getIndexA(u, j)]);
        }
        for (int j(functions.size()); j < nbLayers - 1)
            ;
        ++j;
        {
            vals.add(0);
            vars.add(m_a[getIndexA(u, j)]);
        }
    }
    m_obj.setConstant(-m_placement.m_mainProblem.m_onePathDuals[_i]);
    m_obj.setLinearCoefs(vars, vals);
    vars.end();
    vals.end();
}

bool ChainPlacement3::PricingProblem::solve() {
    return m_solver.solve();
}

double ChainPlacement3::PricingProblem::getObjValue() const {
    return m_solver.getObjValue();
}

ServicePath ChainPlacement3::PricingProblem::getServicePath() const {
#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
    std::cout << "getServicePath() -> " << m_placement.m_inst.demands[m_demandID] << '\n';
#endif
    const auto& funcs = m_placement.m_inst.demands[m_demandID].functions;
    const auto& t = m_placement.m_inst.demands[m_demandID].t;
    std::vector<std::pair<Graph::Node, std::vector<FunctionDescriptor>>> installs;

    Graph::Node u = m_placement.m_inst.demands[m_demandID].s;
    std::size_t j = 0;
    Graph::Path path{u};

    IloNumArray aVal(m_env);
    m_solver.getValues(aVal, m_a);

    IloNumArray fVal(m_env);
    m_solver.getValues(fVal, m_f);

    while (u != t || j < funcs.size())
        ;
    {
        if (j < int(funcs.size()) && m_placement.m_inst.isNFV[u] && aVal[getIndexA(u, j)]) {
            if (installs.empty() || installs.back().first != u) {
                installs.emplace_back(u, std::vector<FunctionDescriptor>());
            }
            installs.back().second.push_back(funcs[j]);
            ++j;
        } else {
            for (const auto& v : m_placement.m_inst.network.getNeighbors(u)) {
                if (fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] > 0) {
                    fVal[getIndexF(m_placement.m_inst.edgeID(u, v), j)] = 0;
                    path.push_back(v);
                    u = v;
                    break;
                }
            }
        }
    }

    assert(path.back() == t);
    return ServicePath(path, installs);
}
} // namespace SFC::Energy

#endif