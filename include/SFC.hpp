#ifndef SFC_HPP
#define SFC_HPP

#include <iostream>
#include <numeric>
#include <tuple>
#include <utility>
#include <vector>

#include "DiGraph.hpp"
#include "utility.hpp"

namespace SFC {

using ::operator<<;
using function_descriptor = int;
using demand_descriptor = int;
struct Demand;
struct ServicePath {
    ServicePath() = default;
    ServicePath(Graph::Path _nPath,
        std::vector<SFC::function_descriptor> _locations,
        demand_descriptor _demand);

    Graph::Path nPath;
    std::vector<SFC::function_descriptor> locations;
    demand_descriptor demand;
};

struct CrossLayerLink {
    demand_descriptor demandID;
    Graph::Node u;
    int j;
};

struct IntraLayerLink {
    demand_descriptor demandID;
    Graph::Edge edge;
    int j;
};

using DiGraph = DiGraph<double>;
ServicePath getServicePath(const Graph::Path& augPath, int _n, int _demandID);
bool operator==(const ServicePath& _sp1, const ServicePath& _sp2);

struct Demand {
    Demand(Graph::Node _s, Graph::Node _t, double _d,
        std::vector<function_descriptor> _functions);
    Demand(const Demand&) = default;
    Demand& operator=(const Demand&) = default;
    Demand(Demand&&) = default;
    Demand& operator=(Demand&&) = default;
    ~Demand() = default;

    Graph::Node s;
    Graph::Node t;
    double d;
    std::vector<function_descriptor> functions;
};

struct Instance {
    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand> _demands, std::vector<double> _funcCharge,
        std::vector<Graph::Node> _NFVNodes);
    DiGraph network;
    std::vector<Graph::Edge> edges;
    std::vector<int> nodeCapa;
    std::vector<double> funcCharge;
    std::vector<Demand> demands;
    int maxChainSize;
    std::vector<Graph::Node> NFVNodes;
    std::vector<int> NFVIndices;
    std::vector<bool> isNFV;
    Matrix<double> nbCores;
    Matrix<int> edgeToId;
};

struct unused {};

template <typename AdditionalInformation = unused>
class Solution {
  public:
    Solution(const Instance& _inst, double _objValue, double _lowerBound,
        std::vector<ServicePath> _paths,
        AdditionalInformation _additionalInformation)
        : inst(&_inst)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , paths(std::move(_paths))
        , additionalInformation(std::move(_additionalInformation)) {}

    void save(const std::string& _filename,
        const std::pair<double, double>& _time) {
        std::ofstream ofs(_filename);
        ofs << objValue << '\t' << lowerBound << '\n';
        ofs << additionalInformation << std::endl;
        ofs << _time.first << '\t' << _time.second << std::endl;

        std::vector<double> linkUsage(inst->network.size(), 0);
        std::vector<int> nodeUsage(inst->network.getOrder(), 0);

        for (const auto & [ nPath, locations, demandID ] : paths) {
            for (auto iteU = nPath.begin(), iteV = std::next(iteU);
                 iteV != nPath.end(); ++iteU, ++iteV) {
                linkUsage[inst->edgeToId(*iteU, *iteV)] += inst->demands[demandID].d;
            }
            for (int j = 0; j < locations.size(); ++j) {
                nodeUsage[locations[j]] += inst->nbCores(demandID, j);
            }
        }

        ofs << inst->network.getOrder() << '\n';
        for (Graph::Node u = 0; u < inst->network.getOrder(); ++u) {
            ofs << u << '\t' << nodeUsage[u] << '\t' << inst->nodeCapa[u] << '\n';
        }

        ofs << inst->network.size() << '\n';
        int e = 0;
        for (const auto& edge : inst->network.getEdges()) {
            ofs << std::fixed << edge.first << '\t' << edge.second << '\t'
                << linkUsage[e] << '\t' << inst->network.getEdgeWeight(edge) << '\n';
            ++e;
        }

        ofs << paths.size() << '\n';
        for (const auto & [ nPath, locations, demandID ] : paths) {
            ofs << nPath.size() << '\t';
            for (const auto& u : nPath) {
                ofs << u << '\t';
            }

            for (int j = 0; j < locations.size(); ++j) {
                ofs << locations[j] << '\t' << inst->demands[demandID].functions[j]
                    << '\t';
            }

            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

  private:
    const Instance* inst;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> paths;
    AdditionalInformation additionalInformation;
};

void showNetworkUsage(const Instance& _inst,
    const std::vector<ServicePath>& _sPaths);

namespace Placed {
using ::operator<<;
struct Instance : public SFC::Instance {
    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand> _demands, std::vector<double> _funcCharge,
        std::vector<Graph::Node> _NFVNodes, Matrix<char> _funcPlacement);
    Instance(SFC::Instance _inst, Matrix<char> _funcPlacement);
    using SFC::Instance::NFVIndices;
    using SFC::Instance::NFVNodes;
    using SFC::Instance::demands;
    using SFC::Instance::edgeToId;
    using SFC::Instance::edges;
    using SFC::Instance::funcCharge;
    using SFC::Instance::isNFV;
    using SFC::Instance::maxChainSize;
    using SFC::Instance::nbCores;
    using SFC::Instance::network;
    using SFC::Instance::nodeCapa;
    Matrix<char> funcPlacement;
};
} // namespace Placed

namespace OccLim {
using ::operator<<;
struct Instance : public SFC::Instance {

    Instance(DiGraph _network, std::vector<int> _nodeCapa,
        std::vector<Demand> _demands, std::vector<double> _funcCharge,
        std::vector<Graph::Node> _NFVNodes, int _nbLicenses);

    using SFC::Instance::NFVIndices;
    using SFC::Instance::NFVNodes;
    using SFC::Instance::demands;
    using SFC::Instance::edgeToId;
    using SFC::Instance::edges;
    using SFC::Instance::funcCharge;
    using SFC::Instance::isNFV;
    using SFC::Instance::maxChainSize;
    using SFC::Instance::nbCores;
    using SFC::Instance::network;
    using SFC::Instance::nodeCapa;
    int nbLicenses;
};

template <typename AdditionalInformation = unused>
class Solution {
  public:
    Solution(const Instance& _inst, double _objValue, double _lowerBound,
        std::vector<ServicePath> _paths,
        AdditionalInformation _additionalInformation)
        : inst(&_inst)
        , objValue(_objValue)
        , lowerBound(_lowerBound)
        , paths(std::move(_paths))
        , additionalInformation(std::move(_additionalInformation)) {}

    std::vector<int> getNodeUsage() const {
        std::vector<int> nodeUsage(inst->network.getOrder(), 0);
        for (const auto& sPath : paths) {
            for (int j = 0; j < sPath.locations.size(); ++j) {
                nodeUsage[sPath.locations[j]] += inst->nbCores(sPath.demand, j);
            }
        }
        return nodeUsage;
    }

    std::vector<double> getLinkUsage() const {
        std::vector<double> linkUsage(inst->network.size(), 0);
        for (const auto& sPath : paths) {
            for (auto iteU = sPath.nPath.begin(), iteV = std::next(iteU);
                 iteV != sPath.nPath.end(); ++iteU, ++iteV) {
                linkUsage[inst->edgeToId(*iteU, *iteV)] += inst->demands[sPath.demand].d;
            }
        }
        return linkUsage;
    }

    Matrix<char> getFunctionPlacement() const {
        Matrix<char> funcPlacement(inst->NFVNodes.size(), inst->funcCharge.size(), 0);
        for (const auto& sPath : paths) {
            for (int j = 0; j < sPath.locations.size(); ++j) {
                funcPlacement(sPath.locations[j], inst->demands[sPath.demand].functions[j]) = 1;
            }
        }
        return funcPlacement;
    }

    void save(const std::string& _filename,
        const std::pair<double, double>& _time) {
        std::ofstream ofs(_filename);
        ofs << objValue << '\t' << lowerBound << '\n';
        ofs << additionalInformation << std::endl;
        ofs << _time.first << '\t' << _time.second << std::endl;

        const std::vector<double> linkUsage = getLinkUsage();
        const std::vector<int> nodeUsage = getNodeUsage();

        const auto fPlacement = getFunctionPlacement();
        for (function_descriptor f = 0; f < inst->funcCharge.size(); ++f) {
            int nbLicenses = 0;
            for (Graph::Node u = 0; u < inst->network.getOrder(); ++u) {
                nbLicenses += fPlacement(u, f);
            }
            assert([&]() {
                if (nbLicenses > inst->nbLicenses) {
                    std::cout << f << " is using too many licenses: [";
                    for (Graph::Node u = 0; u < inst->network.getOrder(); ++u) {
                        if (fPlacement(u, f)) {
                            std::cout << u << ", ";
                        }
                    }
                    std::cout << '\n';
                    return false;
                }
                return true;
            }());
        }

        ofs << inst->network.getOrder() << '\n';
        for (Graph::Node u = 0; u < inst->network.getOrder(); ++u) {
            ofs << u << '\t' << nodeUsage[u] << '\t' << inst->nodeCapa[u] << '\n';
            assert(nodeUsage[u] <= inst->nodeCapa[u]);
        }

        ofs << inst->network.size() << '\n';
        int e = 0;
        for (const auto& edge : inst->network.getEdges()) {
            ofs << std::fixed << edge.first << '\t' << edge.second << '\t'
                << linkUsage[e] << '\t' << inst->network.getEdgeWeight(edge) << '\n';
            assert(linkUsage[e] <= inst->network.getEdgeWeight(edge));
            ++e;
        }

        ofs << paths.size() << '\n';
        for (const auto & [ nPath, locations, demandID ] : paths) {
            ofs << nPath.size() << '\t';
            for (const auto& u : nPath) {
                ofs << u << '\t';
            }

            for (int j = 0; j < locations.size(); ++j) {
                ofs << locations[j] << '\t' << inst->demands[demandID].functions[j]
                    << '\t';
            }

            ofs << '\n';
        }
        std::cout << "Results saved to :" << _filename << '\n';
    }

  private:
    const Instance* inst;
    double objValue;
    double lowerBound;
    std::vector<ServicePath> paths;
    AdditionalInformation additionalInformation;
};

} // namespace OccLim

bool operator==(const SFC::Demand& _d1, const SFC::Demand& _d2);
std::vector<int> loadNodeCapa(const std::string& _filename,
    double _percent = 1.0);
std::vector<Graph::Node> loadNFV(const std::string& _filename, int _nbSDNs,
    int _type);
std::pair<std::vector<Demand>, std::vector<double>>
loadDemands(const std::string& _filename, double _percent);
DiGraph loadNetwork(const std::string& _filename);
std::vector<double> loadFunctions(const std::string& _filename);
std::ostream& operator<<(std::ostream& _out, const Demand& _demand);
std::ostream& operator<<(std::ostream& _out, const ServicePath& _sPath);
std::ostream& operator<<(std::ostream& _out, const Demand& _demand);
std::ostream& operator<<(std::ostream& _out, const ServicePath& _sPath);

class LayeredGraph {
  public:
    LayeredGraph(const Instance& _inst);

    LayeredGraph(const LayeredGraph&) = default;
    LayeredGraph(LayeredGraph&&) = default;
    LayeredGraph& operator=(const LayeredGraph&) = default;
    LayeredGraph& operator=(LayeredGraph&&) = default;
    ~LayeredGraph() = default;

    void removeCrossLayerLink(Graph::Node _u, int _j1);
    void removeInLayerEdge(Graph::Edge _edge, int _j1);

    double getCrossLayerWeight(Graph::Node _u, int _j1) const;
    double getInLayerWeight(Graph::Edge _edge, int _j1) const;

    void setCrossLayerWeight(Graph::Node _u, int _j1, double _weight);
    void setInLayerWeight(Graph::Edge _edge, int _j1, double _weight);

    int getOrder() const {
        return m_graph.getOrder();
    }

    std::vector<Graph::Node> getNeighbors(const Graph::Node _u) const {
        return m_graph.getNeighbors(_u);
    }

    struct Node {
        Graph::Node node;
        int layer;
    };

  private:
    int getRealNode(const LayeredGraph::Node _node) const {
        return m_inst->network.getOrder() * _node.layer + _node.node;
    }
    const Instance* m_inst;
    DiGraph m_graph;
};

} // namespace SFC

#endif
