#ifndef COLUMN_GENERATION_HPP
#define COLUMN_GENERATION_HPP

#include <cmath>
#include <iostream>
#include <map>
#include <optional>
#include <vector>

#include "cplex_utility.hpp"

#include "ThreadPool.hpp"
constexpr double getEpsGap() { return 1e-6; }

template <typename RMP, typename = void>
struct has_get_column : std::false_type {};

template <typename RMP>
struct has_get_column<RMP, std::void_t<std::enable_if_t<std::is_same<
                               int, decltype(std::declval<RMP>().getColumn(
                                        typename RMP::Column()))>::value>>>
    : std::true_type {};

template <typename RMP>
void addColumns(RMP& _rmp, std::vector<typename RMP::Column>& _columns) {
    if constexpr (has_get_column<RMP>::value) {
        IloNumColumnArray columns(_rmp.getEnv());
        columns.setSize(_columns.size());
        std::transform(_columns.begin(), _columns.end(), begin(columns),
            [&](auto&& _col) { return _rmp.getColumn(_col); });
        _rmp.addColumns(columns);
        columns.end();
    } else {
        _rmp.addColumns(_columns);
    }
}

template <typename Instance, typename RMP>
class ColumnGenerationModel {
  public:
    using Column = typename RMP::Column;
    using Solution = typename RMP::Solution;

    explicit ColumnGenerationModel(const Instance* _inst)
        : m_inst(_inst)
        , m_rmp(m_inst) {}

    ~ColumnGenerationModel() = default;
    ColumnGenerationModel(const ColumnGenerationModel&) = default;
    ColumnGenerationModel& operator=(const ColumnGenerationModel&) = default;
    ColumnGenerationModel(ColumnGenerationModel&&) = default;
    ColumnGenerationModel& operator=(ColumnGenerationModel&&) = default;

    Solution getSolution() const { return m_rmp.getSolution(); }

    void addInitConf(const std::vector<Column>& _initColumns) {
        m_rmp.addColumns(_initColumns);
    }

    template <typename PricingProblem, bool CLEAR>
    bool solve(const int _nbThreads) {
        bool reducedCost;
        using future_return_type = std::pair<std::optional<Column>, double>;
        std::vector<std::future<future_return_type>> futures;
        futures.reserve(m_rmp.getNbPricing());

        std::vector<Column> columns;
        columns.reserve(m_rmp.getNbPricing());

        ThreadPool m_pool(_nbThreads);
        const std::map<std::thread::id, int> m_threadID = [&]() {
            std::map<std::thread::id, int> retval;
            int i = 0;
            for (const auto& id : m_pool.getIds()) {
                retval.emplace(id, i++);
            }
            return retval;
        }();

        std::vector<PricingProblem> m_pps;
        m_pps.reserve(_nbThreads);
        for (int i = 0; i < _nbThreads; ++i) {
            m_pps.emplace_back(m_inst);
        }

        int nbIte = 0;
        do {
            ++nbIte;
            reducedCost = false;
            futures.clear();
            std::cout << "Solving RMP...";
            if (m_rmp.template solve<CLEAR>()) {
                std::cout << "Reduced Main Problem Objective Value = "
                          << m_rmp.getObjValue() << ", #cols:" << m_rmp.getNbColumns()
                          << ", #ite: " << nbIte << '\n';
                std::cout << "Searching for paths..." << std::flush;
                for (int i = 0; i < m_rmp.getNbPricing(); ++i) {
                    futures.emplace_back(
                        m_pool.enqueue([&, this, i]() -> future_return_type {
                            auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                            pp.setPricingID(i);
                            pp.updateDual(m_rmp);
                            if (!pp.solve()) {
                                throw std::runtime_error("Pricing could not be solved: " + std::to_string(i));
                            }
                            if (epsilon_less<double>()(pp.getObjValue(), 0.0)) {
                                return {pp.getColumn(), pp.getObjValue()};
                            }
                            return {std::nullopt, pp.getObjValue()};
                        }));
                }
                std::cout << "getting paths..." << std::flush;
                columns.clear();
                for (auto& fut : futures) {
                    auto col = fut.get();
                    if (col.first.has_value()) {
                        reducedCost = true;
                        columns.emplace_back(std::move(col.first.value()));
                    }
                }
                m_rmp.addColumns(columns);
                std::cout << "done!\n";
            } else {
                std::cout << "No solution found for RMP\n";
                return false;
            }
        } while (reducedCost);
        return m_rmp.solveInteger();
    }

    template <typename PricingProblem>
    bool solve(const int _nbThreads) {
        bool reducedCost;
        using future_return_type = std::pair<std::optional<Column>, double>;
        std::vector<std::future<future_return_type>> futures;
        futures.reserve(m_rmp.getNbPricing());

        std::vector<Column> columns;
        columns.reserve(m_rmp.getNbPricing());

        ThreadPool m_pool(_nbThreads);
        const std::map<std::thread::id, int> m_threadID = [&]() {
            std::map<std::thread::id, int> retval;
            int i = 0;
            for (const auto& id : m_pool.getIds()) {
                retval.emplace(id, i++);
            }
            return retval;
        }();

        std::vector<PricingProblem> m_pps;
        m_pps.reserve(_nbThreads);
        for (int i = 0; i < _nbThreads; ++i) {
            m_pps.emplace_back(m_inst);
        }

        int nbIte = 0;
        do {
            ++nbIte;
            reducedCost = false;
            futures.clear();
            std::cout << "Solving RMP...";
            std::cout << "Solving RMP...";
            if (m_rmp.solve()) {
                std::cout << "Reduced Main Problem Objective Value = "
                          << m_rmp.getObjValue() << ", #cols:" << m_rmp.getNbColumns()
                          << ", #ite: " << nbIte << '\n';
                std::cout << "Searching for paths..." << std::flush;
                for (int i = 0; i < m_rmp.getNbPricing(); ++i) {
                    futures.emplace_back(
                        m_pool.enqueue([&, this, i]() -> future_return_type {
                            auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                            pp.setPricingID(i);
                            pp.updateDual(m_rmp);
                            if (!pp.solve()) {
                                throw std::runtime_error("Pricing could not be solved: " + std::to_string(i));
                            }
                            if (epsilon_less<double>()(pp.getObjValue(), 0.0)) {
                                return {pp.getColumn(), pp.getObjValue()};
                            }
                            return {std::nullopt, pp.getObjValue()};
                        }));
                }
                std::cout << "getting paths..." << std::flush;
                columns.clear();
                for (auto& fut : futures) {
                    auto col = fut.get();
                    if (col.first.has_value()) {
                        reducedCost = true;
                        columns.emplace_back(std::move(col.first.value()));
                    }
                }
                m_rmp.addColumns(columns);
                std::cout << "done!\n";
            } else {
                std::cout << "No solution found for RMP\n";
                return false;
            }
        } while (reducedCost);
        return m_rmp.solveInteger();
    }

    template <typename PricingProblem>
    bool solveStabilized(const int _nbThreads, double _alpha = 0.1) {
        using future_return_type = std::pair<Column, double>;
        m_rmp.setAlpha(_alpha);
        std::vector<std::future<future_return_type>> futures;
        futures.reserve(m_rmp.getNbPricing());
        std::vector<Column> columns;
        columns.reserve(m_rmp.getNbPricing());

        std::vector<Column> allColumns;
        allColumns.reserve(m_rmp.getNbPricing());

        ThreadPool m_pool(_nbThreads);
        const std::map<std::thread::id, int> m_threadID = [&]() {
            std::map<std::thread::id, int> pps;
            int i = 0;
            for (const auto& id : m_pool.getIds()) {
                pps.emplace(id, i++);
            }
            return pps;
        }();

        std::vector<PricingProblem> m_pps;
        m_pps.reserve(_nbThreads);
        for (int i = 0; i < _nbThreads; ++i) {
            m_pps.emplace_back(m_inst);
        }

        double bestLowerBound = -std::numeric_limits<double>::max();
        double currentObjValue = std::numeric_limits<double>::max();
        int nbIte = 0;
        while (true) {
            ++nbIte;
            futures.clear();
            std::cout << "Solving RMP..." << std::flush;
            if (m_rmp.solve()) {
                currentObjValue = m_rmp.getObjValue();
                std::cout << "Reduced Main Problem Objective Value = "
                          << currentObjValue << ", #cols: " << m_rmp.getNbColumns()
                          << ", #ite: " << nbIte << std::endl;
                if (epsilon_equal<double>()(currentObjValue, bestLowerBound)) {
                    break;
                }
                std::cout << "Searching for paths..." << std::flush;
                for (int i = 0; i < m_rmp.getNbPricing(); ++i) {
                    futures.emplace_back(
                        m_pool.enqueue([&, this, i]() -> future_return_type {
                            auto& pp = m_pps[m_threadID.at(std::this_thread::get_id())];
                            pp.setPricingID(i);
                            pp.updateDual(m_rmp.getDualValues());
                            if (!pp.solve()) {
                                throw std::runtime_error("Pricing could not be solved: " + std::to_string(i));
                            }
                            return {pp.getColumn(), pp.getObjValue()};
                        }));
                }
                std::cout << "getting paths..." << std::flush;
                columns.clear();
                allColumns.clear();
                double sumStabRC = 0.0;
                double sumRealRC = 0.0;
                for (auto& fut : futures) {
                    auto pair = fut.get();
                    auto& sPath = pair.first;
                    const double realRC = m_rmp.getReducedCost(sPath);

                    if (epsilon_less<double>()(pair.second, 0.0)) {
                        sumStabRC += pair.second;
                        allColumns.emplace_back(sPath);
                    }

                    sumRealRC += realRC;

                    if (epsilon_less<double>()(realRC, 0.0)) {
                        columns.emplace_back(std::move(sPath));
                    }
                }
                std::cout << "\n sumRealRC: " << sumRealRC
                          << ", sumStabRC: " << sumStabRC << '\n';
                std::cout << "done!\n";
                bestLowerBound =
                    m_rmp.getBestLowerBound(bestLowerBound, sumStabRC, allColumns);
                std::cout << "\nCurrent best LB: ";
                if (bestLowerBound < -std::numeric_limits<double>::max()) {
                    std::cout << bestLowerBound;
                } else {
                    std::cout << "-inf";
                }
                std::cout << "...";
                addColumns(m_rmp, columns);

            } else {
                std::cout << "No solution found for RMP\n";
                return false;
            }
        }
        return m_rmp.solveInteger();
    }

  private:
    const Instance* m_inst;
    RMP m_rmp;
};

#endif
