# -*- coding: utf-8 -*-
import random
import sys
sys.path.append("../../MyPython")
import collections
import time
import csv
import itertools

name = sys.argv[1]
networkCharge = int(sys.argv[2])
n = int(sys.argv[3])

if len(sys.argv) <= 2:
	print "Not enough argument {name} {networkCharge}"


# Web Service
# NAT-FW-TM-WOC-IDPS {0, 1, 2, 3, 4}
# 100 kbit/s
# 500 ms

# VoIP
# NAT-FW-TM-FW-NAT {0, 1, 2, 1, 0}
# 64 kbit/s
# 100 ms

# Video Streaming
# NAT-FW-TM-VOC-IDPS {0, 1, 2, 5, 4}
# 4 Mbit/s
# 100 ms

# Online Gaming
# NAT-FW-VOC-WOC-IDPS {0, 1, 5, 3, 4}
# 50 kbit/s
# 60 ms

# 0 NAT
# 1 FW
# 2 TM
# 3 WOC
# 4 IDPS
# 5 VOC


# nbFunction = int(sys.argv[2])
# functions = range(0, nbFunction)

# graph, demands = sndlib.getNetwork(name)

# finalDemands = {}
# for (s, t), d in demands.items():
# 	n = random.randint(1, nbFunction)
# 	chain = random.sample(functions, n)
# 	finalDemands[s, t] = (d, chain)

# CISCO Report
# By Subsegment (PB per Month)
# Internet video	     21,624  27,466  36,456  49,068  66,179  89,319 	33%
# Web, email, and data	 5,853 	 7,694 	 9,476 	 11,707  14,002  16,092 	22%
# File sharing	         6,090 	 6,146 	 6,130 	 6,168 	 6,231 	 6,038		0%
# Online gaming	         27 	 33 	 48 	 78 	 109 	 143 		40%

total = float(36456 + 9476 + 78 + 6168)
percentage = [
	9476 / total, 
	6168 / total, 
	36456 / total,
	78 / total]
print "percentage: ", percentage, sum(percentage)

loads = [100.0/1000000, 64.0/1000000, 4*1024.0/1000000, 50.0/1000000]
bandwidth = [networkCharge * i for i in percentage]
nbChains = [int(charge / size) for charge, size in zip(bandwidth, loads)]
print "bandwidth: ", bandwidth, "nbChains: ", nbChains

pairs = [ i for i in itertools.permutations(range(n), 2) ]
placement = collections.defaultdict(int)
# For each chains
for i in range(len(nbChains)):
	nb = nbChains[i]
	# Choose a pair
	for j in range(nb):
		u, v = pairs[random.randint(0, len(pairs) - 1)]
		placement[u, v, i] += 1

chains = [[0, 1, 2, 3, 4], [0, 1, 2, 1, 0], [0, 1, 2, 5, 4], [0, 1, 5, 3, 4]]

filename = "{}_demand.txt".format(name, int(time.time()))
with open("./instances/" + filename, 'wb') as csvfile:
	demandsWriter = csv.writer(csvfile, delimiter='\t')
	for (u, v, i), n in placement.items():
		demandsWriter.writerow([u, v, n*loads[i]] + chains[i])

